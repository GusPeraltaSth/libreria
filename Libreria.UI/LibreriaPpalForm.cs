﻿using FontAwesome.Sharp;
using Libreria.UI.Forms.Autor;
using Libreria.UI.Forms.Categoria;
using Libreria.UI.Forms.Distribuidores;
using Libreria.UI.Forms.Editorial;
using Libreria.UI.Forms.Estado;
using Libreria.UI.Forms.MetodoDePagos;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Libreria.UI
{
    public partial class LibreriaPpalForm : Form
    {
        private IconButton currentBtn;
        private Panel leftBorderBtn;


        #region Constructor
        public LibreriaPpalForm()
        {
            InitializeComponent();
            InitializationMenuTop();
            CustomizeDesign();
            leftBorderBtn = new Panel();
            leftBorderBtn.Size = new Size(7, 44);
            panelSideMenu.Controls.Add(leftBorderBtn);
        }
        #endregion

        #region Structure
        private struct RGBColors
        {
            public static Color color1 = Color.FromArgb(2, 117, 216);
        }
        #endregion

        #region Events

        private void salirButton_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
        private void mantenedoresButton_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color1);
            ShowSubMenu(panelMantenedoresSubMenu);
        }
        private void autorButton_Click(object sender, EventArgs e)
        {
            OpenChildForm(new SearchAutorForm());
            HideSubMenu();
        }
        private void categoriaButton_Click(object sender, EventArgs e)
        {
            OpenChildForm(new SearchCategoriaForm());
            HideSubMenu();
        }
        private void distribuidoresButton_Click(object sender, EventArgs e)
        {
            OpenChildForm(new SearchDistribuidorForm());

            HideSubMenu();
        }
        private void editorialButton_Click(object sender, EventArgs e)
        {
            OpenChildForm(new SearchEditorialForm());

            HideSubMenu();
        }
        private void estadoButton_Click(object sender, EventArgs e)
        {
            OpenChildForm(new SearchEstadoForm());

            HideSubMenu();
        }
        private void idiomasButton_Click_1(object sender, EventArgs e)
        {
            OpenChildForm(new SearchIdiomaForm());

            HideSubMenu();
        }
        private void metodoPagosButton_Click(object sender, EventArgs e)
        {
            OpenChildForm(new SearchMetodoDePagosForm());

            HideSubMenu();
        }
        private void comprasButton_Click(object sender, EventArgs e)
        {
            //OpenChildForm(new SearchComprasForm());

            HideSubMenu();
        }
        private void facturasButton_Click(object sender, EventArgs e)
        {
            //OpenChildForm(new SearchFacturasForm());

            HideSubMenu();
        }
        private void librosButton_Click(object sender, EventArgs e)
        {
            //OpenChildForm(new SearchLibrosForm());

            HideSubMenu();
        }
        #endregion

        #region Methods
        private void ActivateButton(object senderBtn, Color color)
        {
            if (senderBtn != null)
            {
                DisableButton();
                //currentBtn Button
                currentBtn = (IconButton)senderBtn;
                currentBtn.BackColor = Color.FromArgb(37, 36, 81);
                currentBtn.ForeColor = Color.White;
                currentBtn.TextAlign = ContentAlignment.MiddleCenter;
                currentBtn.IconColor = color;
                currentBtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                currentBtn.ImageAlign = ContentAlignment.MiddleRight;

                //left border button
                leftBorderBtn.BackColor = color;
                leftBorderBtn.Location = new Point(0, currentBtn.Location.Y);
                leftBorderBtn.Visible = true;
                leftBorderBtn.BringToFront();
            }
        }
        private void DisableButton()
        {
            if (currentBtn != null)
            {
                //currentBtn Button
                currentBtn.BackColor = Color.FromArgb(31, 30, 68);
                currentBtn.ForeColor = Color.Gainsboro;
                currentBtn.TextAlign = ContentAlignment.MiddleCenter;
                currentBtn.IconColor = Color.Gainsboro;
                currentBtn.TextImageRelation = TextImageRelation.Overlay;
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }
        private void InitializationMenuTop()
        {
            panelTitle.Dock = DockStyle.Top;
        }
        private void CustomizeDesign()
        {
            panelMantenedoresSubMenu.Visible = false;
        }
        private void HideSubMenu()
        {
            if (panelMantenedoresSubMenu.Visible == true)
            {
                panelMantenedoresSubMenu.Visible = false;
            }
        }
        private void ShowSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                HideSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }
        private void OpenChildForm(Form childForm)
        {
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name != childForm.Name && Application.OpenForms[i].Name != "LibreriaPpalForm")
                {
                    Application.OpenForms[i].Close();
                }
            }
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Location = new Point((containerFormPanel.Size.Width / 2) - (childForm.Size.Width / 2), (containerFormPanel.Size.Height / 2) - (childForm.Size.Height / 2));
            containerFormPanel.Controls.Add(childForm);
            containerFormPanel.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();

        }
        #endregion
    }
}
