﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;

namespace Libreria.UI.Forms.Autor
{
    public partial class CreateAutorForm : MaterialForm
    {
        private List<PaisModel> availablePais;

        #region Constructor
        public CreateAutorForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            LoadInitializeForm();
        }
        #endregion

        #region Methods
        public void LoadInitializeForm()
        {
            availablePais = GlobalConfig.Connection.GetPaises_All();

            agregarPaisAutorComboBox.DataSource = null;
            agregarPaisAutorComboBox.DataSource = availablePais;

            agregarPaisAutorComboBox.DisplayMember = "dsc_pais";
            agregarPaisAutorComboBox.ValueMember = "cdg_pais";
            agregarPaisAutorComboBox.SelectedValue = "--";

            agregarNombreAutorTextBox.Enabled = true;
            agregarAPaternoAutorTextBox.Enabled = true;
            agregarAMaternoAutorTextBox.Enabled = true;
            agregarPaisAutorComboBox.Enabled = true;
            msgCreaAutorLabel.Text = "";

            agregarNombreAutorTextBox.Focus();
        }
        #endregion

        #region Events
        private void agregarAutorButton_Click(object sender, EventArgs e)
        {
            AutorModel model = new AutorModel();
            model.aut_nombre = agregarNombreAutorTextBox.Text;
            model.aut_apellido_paterno = agregarAPaternoAutorTextBox.Text;
            model.aut_apellido_materno = agregarAMaternoAutorTextBox.Text;
            model.cdg_pais = agregarPaisAutorComboBox.SelectedValue.ToString();

            GlobalConfig.Connection.InsertAutor(model);
            if (model.id_autores != 0)
            {
                msgCreaAutorLabel.Text = "Autor Agregado Exitosamente";
                msgCreaAutorLabel.ForeColor = System.Drawing.Color.Green;

                agregarNombreAutorTextBox.Text = "";
                agregarAPaternoAutorTextBox.Text = "";
                agregarAMaternoAutorTextBox.Text = "";
                agregarPaisAutorComboBox.SelectedValue = "--";
                agregarNombreAutorTextBox.Focus();
            }
            else
            {
                msgCreaAutorLabel.Text = "Error al agregar, Favor vuelva a intentarlo nuevamente ...";
                msgCreaAutorLabel.ForeColor = System.Drawing.Color.Red;

                agregarNombreAutorTextBox.Text = "";
                agregarAPaternoAutorTextBox.Text = "";
                agregarAMaternoAutorTextBox.Text = "";
                agregarPaisAutorComboBox.SelectedValue = "--";
                this.ActiveControl = agregarNombreAutorTextBox;
            }
        }

        private void cancelarAutorButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}