﻿namespace Libreria.UI.Forms.Autor
{
    partial class CreateAutorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.agregarPaisAutorComboBox = new System.Windows.Forms.ComboBox();
            this.agregarPaisAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregarAMaternoAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregarAMaternoAutorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.agregarAPaternoAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregarAPaternoAutorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.cancelarAutorButton = new FontAwesome.Sharp.IconButton();
            this.agregarAutorButton = new FontAwesome.Sharp.IconButton();
            this.agregarNombreAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregarNombreAutorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.msgCreaAutorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // agregarPaisAutorComboBox
            // 
            this.agregarPaisAutorComboBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarPaisAutorComboBox.FormattingEnabled = true;
            this.agregarPaisAutorComboBox.ItemHeight = 20;
            this.agregarPaisAutorComboBox.Location = new System.Drawing.Point(316, 296);
            this.agregarPaisAutorComboBox.Name = "agregarPaisAutorComboBox";
            this.agregarPaisAutorComboBox.Size = new System.Drawing.Size(303, 28);
            this.agregarPaisAutorComboBox.TabIndex = 29;
            this.agregarPaisAutorComboBox.Tag = "5";
            // 
            // agregarPaisAutorLabel
            // 
            this.agregarPaisAutorLabel.AutoSize = true;
            this.agregarPaisAutorLabel.BackColor = System.Drawing.Color.White;
            this.agregarPaisAutorLabel.Depth = 0;
            this.agregarPaisAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.agregarPaisAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.agregarPaisAutorLabel.Location = new System.Drawing.Point(165, 304);
            this.agregarPaisAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.agregarPaisAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarPaisAutorLabel.Name = "agregarPaisAutorLabel";
            this.agregarPaisAutorLabel.Size = new System.Drawing.Size(42, 19);
            this.agregarPaisAutorLabel.TabIndex = 28;
            this.agregarPaisAutorLabel.Text = "País:";
            // 
            // agregarAMaternoAutorLabel
            // 
            this.agregarAMaternoAutorLabel.AutoSize = true;
            this.agregarAMaternoAutorLabel.BackColor = System.Drawing.Color.White;
            this.agregarAMaternoAutorLabel.Depth = 0;
            this.agregarAMaternoAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.agregarAMaternoAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.agregarAMaternoAutorLabel.Location = new System.Drawing.Point(165, 243);
            this.agregarAMaternoAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.agregarAMaternoAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarAMaternoAutorLabel.Name = "agregarAMaternoAutorLabel";
            this.agregarAMaternoAutorLabel.Size = new System.Drawing.Size(128, 19);
            this.agregarAMaternoAutorLabel.TabIndex = 27;
            this.agregarAMaternoAutorLabel.Text = "Apellido Materno:";
            // 
            // agregarAMaternoAutorTextBox
            // 
            this.agregarAMaternoAutorTextBox.BackColor = System.Drawing.Color.White;
            this.agregarAMaternoAutorTextBox.Depth = 0;
            this.agregarAMaternoAutorTextBox.Hint = "";
            this.agregarAMaternoAutorTextBox.Location = new System.Drawing.Point(316, 240);
            this.agregarAMaternoAutorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.agregarAMaternoAutorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarAMaternoAutorTextBox.Name = "agregarAMaternoAutorTextBox";
            this.agregarAMaternoAutorTextBox.PasswordChar = '\0';
            this.agregarAMaternoAutorTextBox.SelectedText = "";
            this.agregarAMaternoAutorTextBox.SelectionLength = 0;
            this.agregarAMaternoAutorTextBox.SelectionStart = 0;
            this.agregarAMaternoAutorTextBox.Size = new System.Drawing.Size(303, 23);
            this.agregarAMaternoAutorTextBox.TabIndex = 4;
            this.agregarAMaternoAutorTextBox.UseSystemPasswordChar = false;
            // 
            // agregarAPaternoAutorLabel
            // 
            this.agregarAPaternoAutorLabel.AutoSize = true;
            this.agregarAPaternoAutorLabel.BackColor = System.Drawing.Color.White;
            this.agregarAPaternoAutorLabel.Depth = 0;
            this.agregarAPaternoAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.agregarAPaternoAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.agregarAPaternoAutorLabel.Location = new System.Drawing.Point(165, 172);
            this.agregarAPaternoAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.agregarAPaternoAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarAPaternoAutorLabel.Name = "agregarAPaternoAutorLabel";
            this.agregarAPaternoAutorLabel.Size = new System.Drawing.Size(124, 19);
            this.agregarAPaternoAutorLabel.TabIndex = 25;
            this.agregarAPaternoAutorLabel.Text = "Apellido Paterno:";
            // 
            // agregarAPaternoAutorTextBox
            // 
            this.agregarAPaternoAutorTextBox.BackColor = System.Drawing.Color.White;
            this.agregarAPaternoAutorTextBox.Depth = 0;
            this.agregarAPaternoAutorTextBox.Hint = "";
            this.agregarAPaternoAutorTextBox.Location = new System.Drawing.Point(316, 169);
            this.agregarAPaternoAutorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.agregarAPaternoAutorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarAPaternoAutorTextBox.Name = "agregarAPaternoAutorTextBox";
            this.agregarAPaternoAutorTextBox.PasswordChar = '\0';
            this.agregarAPaternoAutorTextBox.SelectedText = "";
            this.agregarAPaternoAutorTextBox.SelectionLength = 0;
            this.agregarAPaternoAutorTextBox.SelectionStart = 0;
            this.agregarAPaternoAutorTextBox.Size = new System.Drawing.Size(303, 23);
            this.agregarAPaternoAutorTextBox.TabIndex = 3;
            this.agregarAPaternoAutorTextBox.UseSystemPasswordChar = false;
            // 
            // cancelarAutorButton
            // 
            this.cancelarAutorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarAutorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarAutorButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarAutorButton.ForeColor = System.Drawing.Color.White;
            this.cancelarAutorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarAutorButton.IconColor = System.Drawing.Color.Black;
            this.cancelarAutorButton.IconSize = 16;
            this.cancelarAutorButton.Location = new System.Drawing.Point(445, 439);
            this.cancelarAutorButton.Name = "cancelarAutorButton";
            this.cancelarAutorButton.Rotation = 0D;
            this.cancelarAutorButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarAutorButton.TabIndex = 7;
            this.cancelarAutorButton.Text = "Cancelar";
            this.cancelarAutorButton.UseVisualStyleBackColor = false;
            this.cancelarAutorButton.Click += new System.EventHandler(this.cancelarAutorButton_Click);
            // 
            // agregarAutorButton
            // 
            this.agregarAutorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.agregarAutorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarAutorButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarAutorButton.ForeColor = System.Drawing.Color.White;
            this.agregarAutorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.agregarAutorButton.IconColor = System.Drawing.Color.Black;
            this.agregarAutorButton.IconSize = 16;
            this.agregarAutorButton.Location = new System.Drawing.Point(169, 439);
            this.agregarAutorButton.Name = "agregarAutorButton";
            this.agregarAutorButton.Rotation = 0D;
            this.agregarAutorButton.Size = new System.Drawing.Size(174, 36);
            this.agregarAutorButton.TabIndex = 6;
            this.agregarAutorButton.Text = "Guardar";
            this.agregarAutorButton.UseVisualStyleBackColor = false;
            this.agregarAutorButton.Click += new System.EventHandler(this.agregarAutorButton_Click);
            // 
            // agregarNombreAutorLabel
            // 
            this.agregarNombreAutorLabel.AutoSize = true;
            this.agregarNombreAutorLabel.BackColor = System.Drawing.Color.White;
            this.agregarNombreAutorLabel.Depth = 0;
            this.agregarNombreAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.agregarNombreAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.agregarNombreAutorLabel.Location = new System.Drawing.Point(165, 92);
            this.agregarNombreAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.agregarNombreAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarNombreAutorLabel.Name = "agregarNombreAutorLabel";
            this.agregarNombreAutorLabel.Size = new System.Drawing.Size(67, 19);
            this.agregarNombreAutorLabel.TabIndex = 21;
            this.agregarNombreAutorLabel.Text = "Nombre:";
            // 
            // agregarNombreAutorTextBox
            // 
            this.agregarNombreAutorTextBox.BackColor = System.Drawing.Color.White;
            this.agregarNombreAutorTextBox.Depth = 0;
            this.agregarNombreAutorTextBox.Hint = "";
            this.agregarNombreAutorTextBox.Location = new System.Drawing.Point(316, 89);
            this.agregarNombreAutorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.agregarNombreAutorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarNombreAutorTextBox.Name = "agregarNombreAutorTextBox";
            this.agregarNombreAutorTextBox.PasswordChar = '\0';
            this.agregarNombreAutorTextBox.SelectedText = "";
            this.agregarNombreAutorTextBox.SelectionLength = 0;
            this.agregarNombreAutorTextBox.SelectionStart = 0;
            this.agregarNombreAutorTextBox.Size = new System.Drawing.Size(303, 23);
            this.agregarNombreAutorTextBox.TabIndex = 2;
            this.agregarNombreAutorTextBox.UseSystemPasswordChar = false;
            // 
            // msgCreaAutorLabel
            // 
            this.msgCreaAutorLabel.AutoSize = true;
            this.msgCreaAutorLabel.BackColor = System.Drawing.Color.White;
            this.msgCreaAutorLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgCreaAutorLabel.Location = new System.Drawing.Point(165, 366);
            this.msgCreaAutorLabel.Name = "msgCreaAutorLabel";
            this.msgCreaAutorLabel.Size = new System.Drawing.Size(57, 21);
            this.msgCreaAutorLabel.TabIndex = 30;
            this.msgCreaAutorLabel.Text = "label1";
            // 
            // CreateAutorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 512);
            this.ControlBox = false;
            this.Controls.Add(this.msgCreaAutorLabel);
            this.Controls.Add(this.agregarPaisAutorComboBox);
            this.Controls.Add(this.agregarPaisAutorLabel);
            this.Controls.Add(this.agregarAMaternoAutorLabel);
            this.Controls.Add(this.agregarAMaternoAutorTextBox);
            this.Controls.Add(this.agregarAPaternoAutorLabel);
            this.Controls.Add(this.agregarAPaternoAutorTextBox);
            this.Controls.Add(this.cancelarAutorButton);
            this.Controls.Add(this.agregarAutorButton);
            this.Controls.Add(this.agregarNombreAutorLabel);
            this.Controls.Add(this.agregarNombreAutorTextBox);
            this.Name = "CreateAutorForm";
            this.Text = "Agregar Autor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox agregarPaisAutorComboBox;
        private MaterialSkin.Controls.MaterialLabel agregarPaisAutorLabel;
        private MaterialSkin.Controls.MaterialLabel agregarAMaternoAutorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField agregarAMaternoAutorTextBox;
        private MaterialSkin.Controls.MaterialLabel agregarAPaternoAutorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField agregarAPaternoAutorTextBox;
        private FontAwesome.Sharp.IconButton cancelarAutorButton;
        private FontAwesome.Sharp.IconButton agregarAutorButton;
        private MaterialSkin.Controls.MaterialLabel agregarNombreAutorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField agregarNombreAutorTextBox;
        private System.Windows.Forms.Label msgCreaAutorLabel;
    }
}