﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Autor
{
    public partial class EditAutorForm : MaterialForm
    {
        private List<AutorModel> availableAutor;
        private List<PaisModel> availablePais;
        private int idAutor;

        #region Constructor

        public EditAutorForm(int id = 0)
        {
            InitializeComponent();
            idAutor = id;

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            guardarAutorButton.Enabled = false;

            LoadInitializeForm(idAutor);
        }

        #endregion Constructor

        #region Methods

        public void LoadInitializeForm(int id)
        {
            availableAutor = GlobalConfig.Connection.SearchAutor(id, null, null, null, null);
            availablePais = GlobalConfig.Connection.GetPaises_All();

            editarPaisAutorComboBox.DataSource = availablePais;

            editarPaisAutorComboBox.DisplayMember = "dsc_pais";
            editarPaisAutorComboBox.ValueMember = "cdg_pais";

            foreach (AutorModel item in availableAutor)
            {
                editarNombreAutorTextBox.Text = item.aut_nombre;
                editarAPaternoAutorTextBox.Text = item.aut_apellido_paterno;
                editarAMaternoAutorTextBox.Text = item.aut_apellido_materno;
                editarPaisAutorComboBox.SelectedValue = item.cdg_pais;
            }

            guardarAutorButton.Enabled = true;
        }

        #endregion Methods

        #region Events

        private void cancelarAutorButton_Click(object sender, EventArgs e)
        {
            this.Close();

            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name != "LibreriaPpalForm" && Application.OpenForms[i].Name != "SearchAutorForm")
                {
                    Application.OpenForms[i].Close();
                }
            }
        }

        private void guardarAutorButton_Click(object sender, EventArgs e)
        {
            AutorModel model = new AutorModel();
            model.id_autores = idAutor;
            model.aut_nombre = editarNombreAutorTextBox.Text;
            model.aut_apellido_paterno = editarAPaternoAutorTextBox.Text;
            model.aut_apellido_materno = editarAMaternoAutorTextBox.Text;
            model.cdg_pais = editarPaisAutorComboBox.SelectedValue.ToString();

            try
            {
                GlobalConfig.Connection.EditAutor(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("El autor se ha guardado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }

        #endregion Events
    }
}