﻿namespace Libreria.UI.Forms.Autor
{
    partial class EditAutorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarAutorButton = new FontAwesome.Sharp.IconButton();
            this.guardarAutorButton = new FontAwesome.Sharp.IconButton();
            this.editarNombreAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarNombreAutorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.editarAPaternoAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarAPaternoAutorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.editarAMtaernoAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarAMaternoAutorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.editarPaisAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarPaisAutorComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cancelarAutorButton
            // 
            this.cancelarAutorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarAutorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarAutorButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarAutorButton.ForeColor = System.Drawing.Color.White;
            this.cancelarAutorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarAutorButton.IconColor = System.Drawing.Color.Black;
            this.cancelarAutorButton.IconSize = 16;
            this.cancelarAutorButton.Location = new System.Drawing.Point(459, 393);
            this.cancelarAutorButton.Name = "cancelarAutorButton";
            this.cancelarAutorButton.Rotation = 0D;
            this.cancelarAutorButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarAutorButton.TabIndex = 12;
            this.cancelarAutorButton.Text = "Cancelar";
            this.cancelarAutorButton.UseVisualStyleBackColor = false;
            this.cancelarAutorButton.Click += new System.EventHandler(this.cancelarAutorButton_Click);
            // 
            // guardarAutorButton
            // 
            this.guardarAutorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.guardarAutorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.guardarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.guardarAutorButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guardarAutorButton.ForeColor = System.Drawing.Color.White;
            this.guardarAutorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.guardarAutorButton.IconColor = System.Drawing.Color.Black;
            this.guardarAutorButton.IconSize = 16;
            this.guardarAutorButton.Location = new System.Drawing.Point(183, 393);
            this.guardarAutorButton.Name = "guardarAutorButton";
            this.guardarAutorButton.Rotation = 0D;
            this.guardarAutorButton.Size = new System.Drawing.Size(174, 36);
            this.guardarAutorButton.TabIndex = 11;
            this.guardarAutorButton.Text = "Guardar";
            this.guardarAutorButton.UseVisualStyleBackColor = false;
            this.guardarAutorButton.Click += new System.EventHandler(this.guardarAutorButton_Click);
            // 
            // editarNombreAutorLabel
            // 
            this.editarNombreAutorLabel.AutoSize = true;
            this.editarNombreAutorLabel.BackColor = System.Drawing.Color.White;
            this.editarNombreAutorLabel.Depth = 0;
            this.editarNombreAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarNombreAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarNombreAutorLabel.Location = new System.Drawing.Point(179, 108);
            this.editarNombreAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarNombreAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarNombreAutorLabel.Name = "editarNombreAutorLabel";
            this.editarNombreAutorLabel.Size = new System.Drawing.Size(67, 19);
            this.editarNombreAutorLabel.TabIndex = 10;
            this.editarNombreAutorLabel.Text = "Nombre:";
            // 
            // editarNombreAutorTextBox
            // 
            this.editarNombreAutorTextBox.BackColor = System.Drawing.Color.White;
            this.editarNombreAutorTextBox.Depth = 0;
            this.editarNombreAutorTextBox.Hint = "";
            this.editarNombreAutorTextBox.Location = new System.Drawing.Point(330, 105);
            this.editarNombreAutorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarNombreAutorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarNombreAutorTextBox.Name = "editarNombreAutorTextBox";
            this.editarNombreAutorTextBox.PasswordChar = '\0';
            this.editarNombreAutorTextBox.SelectedText = "";
            this.editarNombreAutorTextBox.SelectionLength = 0;
            this.editarNombreAutorTextBox.SelectionStart = 0;
            this.editarNombreAutorTextBox.Size = new System.Drawing.Size(303, 23);
            this.editarNombreAutorTextBox.TabIndex = 9;
            this.editarNombreAutorTextBox.UseSystemPasswordChar = false;
            // 
            // editarAPaternoAutorLabel
            // 
            this.editarAPaternoAutorLabel.AutoSize = true;
            this.editarAPaternoAutorLabel.BackColor = System.Drawing.Color.White;
            this.editarAPaternoAutorLabel.Depth = 0;
            this.editarAPaternoAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarAPaternoAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarAPaternoAutorLabel.Location = new System.Drawing.Point(179, 188);
            this.editarAPaternoAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarAPaternoAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarAPaternoAutorLabel.Name = "editarAPaternoAutorLabel";
            this.editarAPaternoAutorLabel.Size = new System.Drawing.Size(124, 19);
            this.editarAPaternoAutorLabel.TabIndex = 14;
            this.editarAPaternoAutorLabel.Text = "Apellido Paterno:";
            // 
            // editarAPaternoAutorTextBox
            // 
            this.editarAPaternoAutorTextBox.BackColor = System.Drawing.Color.White;
            this.editarAPaternoAutorTextBox.Depth = 0;
            this.editarAPaternoAutorTextBox.Hint = "";
            this.editarAPaternoAutorTextBox.Location = new System.Drawing.Point(330, 185);
            this.editarAPaternoAutorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarAPaternoAutorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarAPaternoAutorTextBox.Name = "editarAPaternoAutorTextBox";
            this.editarAPaternoAutorTextBox.PasswordChar = '\0';
            this.editarAPaternoAutorTextBox.SelectedText = "";
            this.editarAPaternoAutorTextBox.SelectionLength = 0;
            this.editarAPaternoAutorTextBox.SelectionStart = 0;
            this.editarAPaternoAutorTextBox.Size = new System.Drawing.Size(303, 23);
            this.editarAPaternoAutorTextBox.TabIndex = 13;
            this.editarAPaternoAutorTextBox.UseSystemPasswordChar = false;
            // 
            // editarAMtaernoAutorLabel
            // 
            this.editarAMtaernoAutorLabel.AutoSize = true;
            this.editarAMtaernoAutorLabel.BackColor = System.Drawing.Color.White;
            this.editarAMtaernoAutorLabel.Depth = 0;
            this.editarAMtaernoAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarAMtaernoAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarAMtaernoAutorLabel.Location = new System.Drawing.Point(179, 259);
            this.editarAMtaernoAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarAMtaernoAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarAMtaernoAutorLabel.Name = "editarAMtaernoAutorLabel";
            this.editarAMtaernoAutorLabel.Size = new System.Drawing.Size(128, 19);
            this.editarAMtaernoAutorLabel.TabIndex = 16;
            this.editarAMtaernoAutorLabel.Text = "Apellido Materno:";
            // 
            // editarAMaternoAutorTextBox
            // 
            this.editarAMaternoAutorTextBox.BackColor = System.Drawing.Color.White;
            this.editarAMaternoAutorTextBox.Depth = 0;
            this.editarAMaternoAutorTextBox.Hint = "";
            this.editarAMaternoAutorTextBox.Location = new System.Drawing.Point(330, 256);
            this.editarAMaternoAutorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarAMaternoAutorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarAMaternoAutorTextBox.Name = "editarAMaternoAutorTextBox";
            this.editarAMaternoAutorTextBox.PasswordChar = '\0';
            this.editarAMaternoAutorTextBox.SelectedText = "";
            this.editarAMaternoAutorTextBox.SelectionLength = 0;
            this.editarAMaternoAutorTextBox.SelectionStart = 0;
            this.editarAMaternoAutorTextBox.Size = new System.Drawing.Size(303, 23);
            this.editarAMaternoAutorTextBox.TabIndex = 15;
            this.editarAMaternoAutorTextBox.UseSystemPasswordChar = false;
            // 
            // editarPaisAutorLabel
            // 
            this.editarPaisAutorLabel.AutoSize = true;
            this.editarPaisAutorLabel.BackColor = System.Drawing.Color.White;
            this.editarPaisAutorLabel.Depth = 0;
            this.editarPaisAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarPaisAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarPaisAutorLabel.Location = new System.Drawing.Point(179, 320);
            this.editarPaisAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarPaisAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarPaisAutorLabel.Name = "editarPaisAutorLabel";
            this.editarPaisAutorLabel.Size = new System.Drawing.Size(42, 19);
            this.editarPaisAutorLabel.TabIndex = 18;
            this.editarPaisAutorLabel.Text = "País:";
            // 
            // editarPaisAutorComboBox
            // 
            this.editarPaisAutorComboBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editarPaisAutorComboBox.FormattingEnabled = true;
            this.editarPaisAutorComboBox.Location = new System.Drawing.Point(330, 312);
            this.editarPaisAutorComboBox.Name = "editarPaisAutorComboBox";
            this.editarPaisAutorComboBox.Size = new System.Drawing.Size(303, 28);
            this.editarPaisAutorComboBox.TabIndex = 19;
            // 
            // EditAutorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(794, 450);
            this.Controls.Add(this.editarPaisAutorComboBox);
            this.Controls.Add(this.editarPaisAutorLabel);
            this.Controls.Add(this.editarAMtaernoAutorLabel);
            this.Controls.Add(this.editarAMaternoAutorTextBox);
            this.Controls.Add(this.editarAPaternoAutorLabel);
            this.Controls.Add(this.editarAPaternoAutorTextBox);
            this.Controls.Add(this.cancelarAutorButton);
            this.Controls.Add(this.guardarAutorButton);
            this.Controls.Add(this.editarNombreAutorLabel);
            this.Controls.Add(this.editarNombreAutorTextBox);
            this.Name = "EditAutorForm";
            this.Text = "Editar Autor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarAutorButton;
        private FontAwesome.Sharp.IconButton guardarAutorButton;
        private MaterialSkin.Controls.MaterialLabel editarNombreAutorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField editarNombreAutorTextBox;
        private MaterialSkin.Controls.MaterialLabel editarAPaternoAutorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField editarAPaternoAutorTextBox;
        private MaterialSkin.Controls.MaterialLabel editarAMtaernoAutorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField editarAMaternoAutorTextBox;
        private MaterialSkin.Controls.MaterialLabel editarPaisAutorLabel;
        private System.Windows.Forms.ComboBox editarPaisAutorComboBox;
    }
}