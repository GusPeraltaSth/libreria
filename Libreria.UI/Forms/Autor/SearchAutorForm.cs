﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Autor
{
    public partial class SearchAutorForm : MaterialForm
    {
        private List<AutorModel> availableAutor;
        private List<PaisModel> availablePais;
        private string dscAutor;
        private string idTmp;
        private int id;

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public SearchAutorForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
            LoadInitializeForm();
        }

        #endregion Constructor

        #region Methods

        public void LoadInitializeForm()
        {
            availableAutor = GlobalConfig.Connection.GetAutores_All();
            availablePais = GlobalConfig.Connection.GetPaises_All();

            filtroBuscarGroupBox.Enabled = false;

            buscarAutorListBox.DataSource = null;
            paisesBuscarComboBox.DataSource = null;

            buscarAutorListBox.DataSource = availableAutor;
            buscarAutorListBox.DisplayMember = "fullName";
            buscarAutorListBox.ValueMember = "id_autores";

            paisesBuscarComboBox.DataSource = availablePais;

            paisesBuscarComboBox.DisplayMember = "dsc_pais";
            paisesBuscarComboBox.ValueMember = "cdg_pais";

            paisesBuscarComboBox.SelectedValue = "--";

            editarAutorButton.Enabled = false;
            eliminarAutorButton.Enabled = false;

            nombreBuscarTextBox.Enabled = false;
            aPaternoBuscarTextBox.Enabled = false;
            aMaternoBuscarTextBox.Enabled = false;
            paisesBuscarComboBox.Enabled = false;
        }

        private void buscarAutor()
        {
            try
            {
                //TODO - Revisar problema por Nullable Exception en ListBox y ComboBox

                List<AutorModel> searchAutor;
                searchAutor = GlobalConfig.Connection.SearchAutor(0, nombreBuscarTextBox.Text.ToUpper(), aPaternoBuscarTextBox.Text.ToUpper(), aMaternoBuscarTextBox.Text.ToUpper(), paisesBuscarComboBox.SelectedValue.ToString().ToUpper());

                buscarAutorListBox.DataSource = searchAutor;
                buscarAutorListBox.DisplayMember = "fullName";
                buscarAutorListBox.ValueMember = "id_autores";
            }
            catch (NullReferenceException)
            {
                buscarAutorListBox.DisplayMember = "fullName";
                buscarAutorListBox.ValueMember = "id_autores";
            }
        }

        #endregion Methods

        #region Events
        private void nombreBuscarRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (nombreBuscarRadioButton.Checked)
            {
                nombreBuscarTextBox.Enabled = true;
                aPaternoBuscarTextBox.Clear();
                aMaternoBuscarTextBox.Clear();
                paisesBuscarComboBox.SelectedValue = "--";
            }
            else
            {
                nombreBuscarTextBox.Enabled = false;
            }
        }

        private void aPaternoBuscarRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (aPaternoBuscarRadioButton.Checked)
            {
                aPaternoBuscarTextBox.Enabled = true;
                nombreBuscarTextBox.Clear();
                aMaternoBuscarTextBox.Clear();
                paisesBuscarComboBox.SelectedValue = "--";
            }
            else
            {
                aPaternoBuscarTextBox.Enabled = false;
            }
        }

        private void aMaternoBuscarRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (aMaternoBuscarRadioButton.Checked)
            {
                aMaternoBuscarTextBox.Enabled = true;
                aPaternoBuscarTextBox.Clear();
                paisesBuscarComboBox.SelectedValue = "--";
            }
            else
            {
                aMaternoBuscarTextBox.Enabled = false;
            }
        }

        private void paisesBuscarRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (paisesBuscarRadioButton.Checked)
            {
                paisesBuscarComboBox.Enabled = true;
                nombreBuscarTextBox.Clear();
                aPaternoBuscarTextBox.Clear();
                aMaternoBuscarTextBox.Clear();
            }
            else
            {
                paisesBuscarComboBox.Enabled = false;
            }
        }

        private void buscarAutorListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            editarAutorButton.Enabled = true;
            eliminarAutorButton.Enabled = true;

            if (buscarAutorListBox != null & buscarAutorListBox.Text != "")
            {
                dscAutor = buscarAutorListBox.GetItemText(buscarAutorListBox.SelectedItem);
                idTmp = buscarAutorListBox.SelectedValue.ToString();
            }
        }

        private void aPaternoBuscarTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            buscarAutor();
        }

        private void aMaternoBuscarTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            buscarAutor();
        }

        private void paisesBuscarComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (paisesBuscarComboBox.SelectedIndex != 0)
            {
                buscarAutor();
            }
        }

        private void nombreBuscarTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            buscarAutor();
        }

        private void buscarAutorListBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            editarAutorButton.Enabled = true;
            eliminarAutorButton.Enabled = true;

            if (buscarAutorListBox != null & buscarAutorListBox.Text != "")
            {
                dscAutor = buscarAutorListBox.GetItemText(buscarAutorListBox.SelectedItem);
                idTmp = buscarAutorListBox.SelectedValue.ToString();

                int.TryParse(idTmp, out id);
            }
        }

        private void editarAutorButton_Click(object sender, EventArgs e)
        {
            EditAutorForm frmEditAutor = new EditAutorForm(id);
            frmEditAutor.StartPosition = FormStartPosition.CenterParent;
            frmEditAutor.Tag = this;
            frmEditAutor.ShowDialog(this);

            paisesBuscarComboBox.SelectedIndex = 0;

            LoadInitializeForm();
        }

        private void eliminarAutorButton_Click(object sender, EventArgs e)
        {
            DeleteAutorForm frmDeleteAutor = new DeleteAutorForm(dscAutor, id);
            frmDeleteAutor.StartPosition = FormStartPosition.CenterParent;
            frmDeleteAutor.Tag = this;
            frmDeleteAutor.ShowDialog(this);

            LoadInitializeForm();
        }

        private void cancelarAutorButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void agregarAutorButton_Click(object sender, EventArgs e)
        {
            CreateAutorForm createAutorForm = new CreateAutorForm();
            createAutorForm.StartPosition = FormStartPosition.CenterParent;
            createAutorForm.Tag = this;
            createAutorForm.ShowDialog(this);
            LoadInitializeForm();
        }

        #endregion Events
    }
}