﻿namespace Libreria.UI.Forms.Autor
{
    partial class SearchAutorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarAutorButton = new FontAwesome.Sharp.IconButton();
            this.eliminarAutorButton = new FontAwesome.Sharp.IconButton();
            this.editarAutorButton = new FontAwesome.Sharp.IconButton();
            this.buscarAutorListBox = new System.Windows.Forms.ListBox();
            this.aPaternoBuscarTextBox = new System.Windows.Forms.TextBox();
            this.paisesBuscarComboBox = new System.Windows.Forms.ComboBox();
            this.nombreBuscarTextBox = new System.Windows.Forms.TextBox();
            this.nombreBuscarRadioButton = new MaterialSkin.Controls.MaterialRadioButton();
            this.aPaternoBuscarRadioButton = new MaterialSkin.Controls.MaterialRadioButton();
            this.paisesBuscarRadioButton = new MaterialSkin.Controls.MaterialRadioButton();
            this.aMaternoBuscarRadioButton = new MaterialSkin.Controls.MaterialRadioButton();
            this.aMaternoBuscarTextBox = new System.Windows.Forms.TextBox();
            this.filtroBuscarGroupBox = new System.Windows.Forms.GroupBox();
            this.agregarAutorButton = new FontAwesome.Sharp.IconButton();
            this.filtroBuscarGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // cancelarAutorButton
            // 
            this.cancelarAutorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarAutorButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarAutorButton.ForeColor = System.Drawing.Color.White;
            this.cancelarAutorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarAutorButton.IconColor = System.Drawing.Color.Black;
            this.cancelarAutorButton.IconSize = 16;
            this.cancelarAutorButton.Location = new System.Drawing.Point(580, 353);
            this.cancelarAutorButton.Name = "cancelarAutorButton";
            this.cancelarAutorButton.Rotation = 0D;
            this.cancelarAutorButton.Size = new System.Drawing.Size(100, 36);
            this.cancelarAutorButton.TabIndex = 15;
            this.cancelarAutorButton.Text = "Cancelar";
            this.cancelarAutorButton.UseVisualStyleBackColor = false;
            this.cancelarAutorButton.Click += new System.EventHandler(this.cancelarAutorButton_Click);
            // 
            // eliminarAutorButton
            // 
            this.eliminarAutorButton.BackColor = System.Drawing.Color.White;
            this.eliminarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eliminarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eliminarAutorButton.ForeColor = System.Drawing.Color.White;
            this.eliminarAutorButton.IconChar = FontAwesome.Sharp.IconChar.Trash;
            this.eliminarAutorButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.eliminarAutorButton.IconSize = 22;
            this.eliminarAutorButton.Location = new System.Drawing.Point(569, 163);
            this.eliminarAutorButton.Name = "eliminarAutorButton";
            this.eliminarAutorButton.Rotation = 0D;
            this.eliminarAutorButton.Size = new System.Drawing.Size(36, 36);
            this.eliminarAutorButton.TabIndex = 14;
            this.eliminarAutorButton.UseVisualStyleBackColor = false;
            this.eliminarAutorButton.Click += new System.EventHandler(this.eliminarAutorButton_Click);
            // 
            // editarAutorButton
            // 
            this.editarAutorButton.BackColor = System.Drawing.Color.White;
            this.editarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.editarAutorButton.ForeColor = System.Drawing.Color.White;
            this.editarAutorButton.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.editarAutorButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.editarAutorButton.IconSize = 22;
            this.editarAutorButton.Location = new System.Drawing.Point(569, 121);
            this.editarAutorButton.Name = "editarAutorButton";
            this.editarAutorButton.Rotation = 0D;
            this.editarAutorButton.Size = new System.Drawing.Size(36, 36);
            this.editarAutorButton.TabIndex = 13;
            this.editarAutorButton.UseVisualStyleBackColor = false;
            this.editarAutorButton.Click += new System.EventHandler(this.editarAutorButton_Click);
            // 
            // buscarAutorListBox
            // 
            this.buscarAutorListBox.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buscarAutorListBox.FormattingEnabled = true;
            this.buscarAutorListBox.ItemHeight = 17;
            this.buscarAutorListBox.Location = new System.Drawing.Point(280, 79);
            this.buscarAutorListBox.MultiColumn = true;
            this.buscarAutorListBox.Name = "buscarAutorListBox";
            this.buscarAutorListBox.Size = new System.Drawing.Size(283, 310);
            this.buscarAutorListBox.TabIndex = 12;
            this.buscarAutorListBox.SelectedIndexChanged += new System.EventHandler(this.buscarAutorListBox_SelectedIndexChanged_1);
            // 
            // aPaternoBuscarTextBox
            // 
            this.aPaternoBuscarTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aPaternoBuscarTextBox.Location = new System.Drawing.Point(6, 120);
            this.aPaternoBuscarTextBox.Name = "aPaternoBuscarTextBox";
            this.aPaternoBuscarTextBox.Size = new System.Drawing.Size(195, 26);
            this.aPaternoBuscarTextBox.TabIndex = 10;
            this.aPaternoBuscarTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.aPaternoBuscarTextBox_KeyPress);
            // 
            // paisesBuscarComboBox
            // 
            this.paisesBuscarComboBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paisesBuscarComboBox.FormattingEnabled = true;
            this.paisesBuscarComboBox.Location = new System.Drawing.Point(6, 265);
            this.paisesBuscarComboBox.Name = "paisesBuscarComboBox";
            this.paisesBuscarComboBox.Size = new System.Drawing.Size(195, 28);
            this.paisesBuscarComboBox.TabIndex = 16;
            this.paisesBuscarComboBox.Text = "-- SELECCIONE --";
            this.paisesBuscarComboBox.SelectedIndexChanged += new System.EventHandler(this.paisesBuscarComboBox_SelectedIndexChanged);
            // 
            // nombreBuscarTextBox
            // 
            this.nombreBuscarTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreBuscarTextBox.Location = new System.Drawing.Point(6, 49);
            this.nombreBuscarTextBox.Name = "nombreBuscarTextBox";
            this.nombreBuscarTextBox.Size = new System.Drawing.Size(195, 26);
            this.nombreBuscarTextBox.TabIndex = 17;
            this.nombreBuscarTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nombreBuscarTextBox_KeyPress);
            // 
            // nombreBuscarRadioButton
            // 
            this.nombreBuscarRadioButton.AutoSize = true;
            this.nombreBuscarRadioButton.BackColor = System.Drawing.Color.White;
            this.nombreBuscarRadioButton.Depth = 0;
            this.nombreBuscarRadioButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.nombreBuscarRadioButton.Location = new System.Drawing.Point(6, 16);
            this.nombreBuscarRadioButton.Margin = new System.Windows.Forms.Padding(0);
            this.nombreBuscarRadioButton.MouseLocation = new System.Drawing.Point(-1, -1);
            this.nombreBuscarRadioButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.nombreBuscarRadioButton.Name = "nombreBuscarRadioButton";
            this.nombreBuscarRadioButton.Ripple = true;
            this.nombreBuscarRadioButton.Size = new System.Drawing.Size(78, 30);
            this.nombreBuscarRadioButton.TabIndex = 18;
            this.nombreBuscarRadioButton.TabStop = true;
            this.nombreBuscarRadioButton.Text = "Nombre";
            this.nombreBuscarRadioButton.UseVisualStyleBackColor = false;
            this.nombreBuscarRadioButton.CheckedChanged += new System.EventHandler(this.nombreBuscarRadioButton_CheckedChanged);
            // 
            // aPaternoBuscarRadioButton
            // 
            this.aPaternoBuscarRadioButton.AutoSize = true;
            this.aPaternoBuscarRadioButton.BackColor = System.Drawing.Color.White;
            this.aPaternoBuscarRadioButton.Depth = 0;
            this.aPaternoBuscarRadioButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.aPaternoBuscarRadioButton.Location = new System.Drawing.Point(6, 87);
            this.aPaternoBuscarRadioButton.Margin = new System.Windows.Forms.Padding(0);
            this.aPaternoBuscarRadioButton.MouseLocation = new System.Drawing.Point(-1, -1);
            this.aPaternoBuscarRadioButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.aPaternoBuscarRadioButton.Name = "aPaternoBuscarRadioButton";
            this.aPaternoBuscarRadioButton.Ripple = true;
            this.aPaternoBuscarRadioButton.Size = new System.Drawing.Size(131, 30);
            this.aPaternoBuscarRadioButton.TabIndex = 19;
            this.aPaternoBuscarRadioButton.TabStop = true;
            this.aPaternoBuscarRadioButton.Text = "Apellido Paterno";
            this.aPaternoBuscarRadioButton.UseVisualStyleBackColor = false;
            this.aPaternoBuscarRadioButton.CheckedChanged += new System.EventHandler(this.aPaternoBuscarRadioButton_CheckedChanged);
            // 
            // paisesBuscarRadioButton
            // 
            this.paisesBuscarRadioButton.AutoSize = true;
            this.paisesBuscarRadioButton.BackColor = System.Drawing.Color.White;
            this.paisesBuscarRadioButton.Depth = 0;
            this.paisesBuscarRadioButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.paisesBuscarRadioButton.Location = new System.Drawing.Point(6, 232);
            this.paisesBuscarRadioButton.Margin = new System.Windows.Forms.Padding(0);
            this.paisesBuscarRadioButton.MouseLocation = new System.Drawing.Point(-1, -1);
            this.paisesBuscarRadioButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.paisesBuscarRadioButton.Name = "paisesBuscarRadioButton";
            this.paisesBuscarRadioButton.Ripple = true;
            this.paisesBuscarRadioButton.Size = new System.Drawing.Size(56, 30);
            this.paisesBuscarRadioButton.TabIndex = 20;
            this.paisesBuscarRadioButton.TabStop = true;
            this.paisesBuscarRadioButton.Text = "País";
            this.paisesBuscarRadioButton.UseVisualStyleBackColor = false;
            this.paisesBuscarRadioButton.CheckedChanged += new System.EventHandler(this.paisesBuscarRadioButton_CheckedChanged);
            // 
            // aMaternoBuscarRadioButton
            // 
            this.aMaternoBuscarRadioButton.AutoSize = true;
            this.aMaternoBuscarRadioButton.BackColor = System.Drawing.Color.White;
            this.aMaternoBuscarRadioButton.Depth = 0;
            this.aMaternoBuscarRadioButton.Font = new System.Drawing.Font("Roboto", 10F);
            this.aMaternoBuscarRadioButton.Location = new System.Drawing.Point(6, 158);
            this.aMaternoBuscarRadioButton.Margin = new System.Windows.Forms.Padding(0);
            this.aMaternoBuscarRadioButton.MouseLocation = new System.Drawing.Point(-1, -1);
            this.aMaternoBuscarRadioButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.aMaternoBuscarRadioButton.Name = "aMaternoBuscarRadioButton";
            this.aMaternoBuscarRadioButton.Ripple = true;
            this.aMaternoBuscarRadioButton.Size = new System.Drawing.Size(134, 30);
            this.aMaternoBuscarRadioButton.TabIndex = 22;
            this.aMaternoBuscarRadioButton.TabStop = true;
            this.aMaternoBuscarRadioButton.Text = "Apellido Materno";
            this.aMaternoBuscarRadioButton.UseVisualStyleBackColor = false;
            this.aMaternoBuscarRadioButton.CheckedChanged += new System.EventHandler(this.aMaternoBuscarRadioButton_CheckedChanged);
            // 
            // aMaternoBuscarTextBox
            // 
            this.aMaternoBuscarTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aMaternoBuscarTextBox.Location = new System.Drawing.Point(6, 191);
            this.aMaternoBuscarTextBox.Name = "aMaternoBuscarTextBox";
            this.aMaternoBuscarTextBox.Size = new System.Drawing.Size(195, 26);
            this.aMaternoBuscarTextBox.TabIndex = 21;
            this.aMaternoBuscarTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.aMaternoBuscarTextBox_KeyPress);
            // 
            // filtroBuscarGroupBox
            // 
            this.filtroBuscarGroupBox.BackColor = System.Drawing.Color.White;
            this.filtroBuscarGroupBox.Controls.Add(this.nombreBuscarTextBox);
            this.filtroBuscarGroupBox.Controls.Add(this.paisesBuscarRadioButton);
            this.filtroBuscarGroupBox.Controls.Add(this.paisesBuscarComboBox);
            this.filtroBuscarGroupBox.Controls.Add(this.aMaternoBuscarRadioButton);
            this.filtroBuscarGroupBox.Controls.Add(this.aMaternoBuscarTextBox);
            this.filtroBuscarGroupBox.Controls.Add(this.nombreBuscarRadioButton);
            this.filtroBuscarGroupBox.Controls.Add(this.aPaternoBuscarRadioButton);
            this.filtroBuscarGroupBox.Controls.Add(this.aPaternoBuscarTextBox);
            this.filtroBuscarGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.filtroBuscarGroupBox.Location = new System.Drawing.Point(28, 79);
            this.filtroBuscarGroupBox.Name = "filtroBuscarGroupBox";
            this.filtroBuscarGroupBox.Size = new System.Drawing.Size(210, 310);
            this.filtroBuscarGroupBox.TabIndex = 23;
            this.filtroBuscarGroupBox.TabStop = false;
            this.filtroBuscarGroupBox.Text = "Filtros";
            // 
            // agregarAutorButton
            // 
            this.agregarAutorButton.BackColor = System.Drawing.Color.White;
            this.agregarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarAutorButton.ForeColor = System.Drawing.Color.White;
            this.agregarAutorButton.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.agregarAutorButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarAutorButton.IconSize = 22;
            this.agregarAutorButton.Location = new System.Drawing.Point(569, 79);
            this.agregarAutorButton.Name = "agregarAutorButton";
            this.agregarAutorButton.Rotation = 0D;
            this.agregarAutorButton.Size = new System.Drawing.Size(36, 36);
            this.agregarAutorButton.TabIndex = 24;
            this.agregarAutorButton.UseVisualStyleBackColor = false;
            this.agregarAutorButton.Click += new System.EventHandler(this.agregarAutorButton_Click);
            // 
            // SearchAutorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 420);
            this.ControlBox = false;
            this.Controls.Add(this.agregarAutorButton);
            this.Controls.Add(this.filtroBuscarGroupBox);
            this.Controls.Add(this.cancelarAutorButton);
            this.Controls.Add(this.eliminarAutorButton);
            this.Controls.Add(this.editarAutorButton);
            this.Controls.Add(this.buscarAutorListBox);
            this.Name = "SearchAutorForm";
            this.Text = "Autor";
            this.filtroBuscarGroupBox.ResumeLayout(false);
            this.filtroBuscarGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarAutorButton;
        private FontAwesome.Sharp.IconButton eliminarAutorButton;
        private FontAwesome.Sharp.IconButton editarAutorButton;
        private System.Windows.Forms.ListBox buscarAutorListBox;
        private System.Windows.Forms.TextBox aPaternoBuscarTextBox;
        private System.Windows.Forms.ComboBox paisesBuscarComboBox;
        private System.Windows.Forms.TextBox nombreBuscarTextBox;
        private MaterialSkin.Controls.MaterialRadioButton nombreBuscarRadioButton;
        private MaterialSkin.Controls.MaterialRadioButton aPaternoBuscarRadioButton;
        private MaterialSkin.Controls.MaterialRadioButton paisesBuscarRadioButton;
        private MaterialSkin.Controls.MaterialRadioButton aMaternoBuscarRadioButton;
        private System.Windows.Forms.TextBox aMaternoBuscarTextBox;
        private System.Windows.Forms.GroupBox filtroBuscarGroupBox;
        private FontAwesome.Sharp.IconButton agregarAutorButton;
    }
}