﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Autor
{
    public partial class DeleteAutorForm : MaterialForm
    {
        private string fullNameLocal;
        private int idAutorLocal;

        #region Constructor
        public DeleteAutorForm(string fullName = null, int idAutor = 0)
        {
            InitializeComponent();

            fullNameLocal = fullName;
            idAutorLocal = idAutor;

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            InitializeDeleteForm(fullNameLocal);
        }

        #endregion

        #region Methods
        public void InitializeDeleteForm(string fullName)
        {
            eliminarAutorLabel.Text = eliminarAutorLabel.Text + " " + fullName + "?";
        }

        #endregion Methods

        #region Events

        private void cancelarAutorButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void eliminarAutorButton_Click(object sender, EventArgs e)
        {
            AutorModel model = new AutorModel();
            model.id_autores = idAutorLocal;

            try
            {
                GlobalConfig.Connection.DeleteAutor(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("El autor se ha eliminado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }

        #endregion Events
    }
}