﻿namespace Libreria.UI.Forms.Autor
{
    partial class DeleteAutorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarAutorButton = new FontAwesome.Sharp.IconButton();
            this.eliminarAutorButton = new FontAwesome.Sharp.IconButton();
            this.eliminarAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // cancelarAutorButton
            // 
            this.cancelarAutorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarAutorButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarAutorButton.ForeColor = System.Drawing.Color.White;
            this.cancelarAutorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarAutorButton.IconColor = System.Drawing.Color.Black;
            this.cancelarAutorButton.IconSize = 16;
            this.cancelarAutorButton.Location = new System.Drawing.Point(275, 241);
            this.cancelarAutorButton.Name = "cancelarAutorButton";
            this.cancelarAutorButton.Rotation = 0D;
            this.cancelarAutorButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarAutorButton.TabIndex = 15;
            this.cancelarAutorButton.Text = "Cancelar";
            this.cancelarAutorButton.UseVisualStyleBackColor = false;
            this.cancelarAutorButton.Click += new System.EventHandler(this.cancelarAutorButton_Click);
            // 
            // eliminarAutorButton
            // 
            this.eliminarAutorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.eliminarAutorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eliminarAutorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eliminarAutorButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eliminarAutorButton.ForeColor = System.Drawing.Color.White;
            this.eliminarAutorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.eliminarAutorButton.IconColor = System.Drawing.Color.Black;
            this.eliminarAutorButton.IconSize = 16;
            this.eliminarAutorButton.Location = new System.Drawing.Point(72, 241);
            this.eliminarAutorButton.Name = "eliminarAutorButton";
            this.eliminarAutorButton.Rotation = 0D;
            this.eliminarAutorButton.Size = new System.Drawing.Size(174, 36);
            this.eliminarAutorButton.TabIndex = 14;
            this.eliminarAutorButton.Text = "Eliminar";
            this.eliminarAutorButton.UseVisualStyleBackColor = false;
            this.eliminarAutorButton.Click += new System.EventHandler(this.eliminarAutorButton_Click);
            // 
            // eliminarAutorLabel
            // 
            this.eliminarAutorLabel.AutoSize = true;
            this.eliminarAutorLabel.Depth = 0;
            this.eliminarAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.eliminarAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.eliminarAutorLabel.Location = new System.Drawing.Point(69, 142);
            this.eliminarAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.eliminarAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.eliminarAutorLabel.Name = "eliminarAutorLabel";
            this.eliminarAutorLabel.Size = new System.Drawing.Size(229, 19);
            this.eliminarAutorLabel.TabIndex = 13;
            this.eliminarAutorLabel.Text = "¿Está seguro que quiere eliminar";
            // 
            // DeleteAutorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 343);
            this.Controls.Add(this.cancelarAutorButton);
            this.Controls.Add(this.eliminarAutorButton);
            this.Controls.Add(this.eliminarAutorLabel);
            this.Name = "DeleteAutorForm";
            this.Text = "Eliminar Autor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarAutorButton;
        private FontAwesome.Sharp.IconButton eliminarAutorButton;
        private MaterialSkin.Controls.MaterialLabel eliminarAutorLabel;
    }
}