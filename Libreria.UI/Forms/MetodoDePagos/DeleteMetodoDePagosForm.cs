﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.MetodoDePagos
{
    public partial class DeleteMetodoDePagosForm : MaterialForm
    {
        private string dscMetodoPago;
        private int id;

        #region Constructor
        public DeleteMetodoDePagosForm(MetodoPagoModel model)
        {
            InitializeComponent();
            id = model.id_metodoPago;
            dscMetodoPago = model.dsc_metodoPago;

            InitializeDeleteForm(model.dsc_metodoPago);

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
        }
        #endregion

        #region Methods
        public void InitializeDeleteForm(string dscMetodoPago)
        {
            eliminarLabel.Text = eliminarLabel.Text + " " + dscMetodoPago + "?";
        }
        #endregion

        #region Events

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name == "SearchMetodoDePagosForm")
                {
                    Application.OpenForms[i].Show();
                }
            }
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            MetodoPagoModel model = new MetodoPagoModel();
            model.dsc_metodoPago = dscMetodoPago;
            model.id_metodoPago = id;

            try
            {
                GlobalConfig.Connection.DeleteMetodoPago(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("El método de pago se ha eliminado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}
