﻿namespace Libreria.UI.Forms.MetodoDePagos
{
    partial class SearchMetodoDePagosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.buscarListBox = new System.Windows.Forms.ListBox();
            this.agregarButton = new FontAwesome.Sharp.IconButton();
            this.eliminarButton = new FontAwesome.Sharp.IconButton();
            this.editarButton = new FontAwesome.Sharp.IconButton();
            this.SuspendLayout();
            // 
            // cancelarButton
            // 
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(498, 328);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(100, 36);
            this.cancelarButton.TabIndex = 14;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // buscarListBox
            // 
            this.buscarListBox.DisplayMember = "dsc_idioma";
            this.buscarListBox.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buscarListBox.FormattingEnabled = true;
            this.buscarListBox.ItemHeight = 17;
            this.buscarListBox.Location = new System.Drawing.Point(209, 105);
            this.buscarListBox.Name = "buscarListBox";
            this.buscarListBox.Size = new System.Drawing.Size(283, 259);
            this.buscarListBox.TabIndex = 11;
            this.buscarListBox.ValueMember = "id_idioma";
            this.buscarListBox.SelectedIndexChanged += new System.EventHandler(this.buscarListBox_SelectedIndexChanged);
            // 
            // agregarButton
            // 
            this.agregarButton.BackColor = System.Drawing.Color.White;
            this.agregarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarButton.ForeColor = System.Drawing.Color.White;
            this.agregarButton.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.agregarButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarButton.IconSize = 22;
            this.agregarButton.Location = new System.Drawing.Point(498, 105);
            this.agregarButton.Name = "agregarButton";
            this.agregarButton.Rotation = 0D;
            this.agregarButton.Size = new System.Drawing.Size(36, 36);
            this.agregarButton.TabIndex = 20;
            this.agregarButton.UseVisualStyleBackColor = false;
            this.agregarButton.Click += new System.EventHandler(this.agregarButton_Click);
            // 
            // eliminarButton
            // 
            this.eliminarButton.BackColor = System.Drawing.Color.White;
            this.eliminarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eliminarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eliminarButton.ForeColor = System.Drawing.Color.White;
            this.eliminarButton.IconChar = FontAwesome.Sharp.IconChar.Trash;
            this.eliminarButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.eliminarButton.IconSize = 22;
            this.eliminarButton.Location = new System.Drawing.Point(498, 189);
            this.eliminarButton.Name = "eliminarButton";
            this.eliminarButton.Rotation = 0D;
            this.eliminarButton.Size = new System.Drawing.Size(36, 36);
            this.eliminarButton.TabIndex = 19;
            this.eliminarButton.UseVisualStyleBackColor = false;
            this.eliminarButton.Click += new System.EventHandler(this.eliminarButton_Click);
            // 
            // editarButton
            // 
            this.editarButton.BackColor = System.Drawing.Color.White;
            this.editarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.editarButton.ForeColor = System.Drawing.Color.White;
            this.editarButton.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.editarButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.editarButton.IconSize = 22;
            this.editarButton.Location = new System.Drawing.Point(498, 147);
            this.editarButton.Name = "editarButton";
            this.editarButton.Rotation = 0D;
            this.editarButton.Size = new System.Drawing.Size(36, 36);
            this.editarButton.TabIndex = 18;
            this.editarButton.UseVisualStyleBackColor = false;
            this.editarButton.Click += new System.EventHandler(this.editarButton_Click);
            // 
            // SearchMetodoDePagosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 450);
            this.ControlBox = false;
            this.Controls.Add(this.agregarButton);
            this.Controls.Add(this.eliminarButton);
            this.Controls.Add(this.editarButton);
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.buscarListBox);
            this.Name = "SearchMetodoDePagosForm";
            this.Text = "Métodos de Pago";
            this.ResumeLayout(false);

        }

        #endregion
        private FontAwesome.Sharp.IconButton cancelarButton;
        private System.Windows.Forms.ListBox buscarListBox;
        private FontAwesome.Sharp.IconButton agregarButton;
        private FontAwesome.Sharp.IconButton eliminarButton;
        private FontAwesome.Sharp.IconButton editarButton;
    }
}