﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;

namespace Libreria.UI.Forms.MetodoDePagos
{
    public partial class CreateMetodoDePagosForm : MaterialForm
    {
        #region Constructor
        public CreateMetodoDePagosForm()
        {
            InitializeComponent();
            msgCreaLabel.Text = "";

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
        }
        #endregion

        #region Events
        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void agregarButton_Click(object sender, EventArgs e)
        {
            MetodoPagoModel model = new MetodoPagoModel();
            model.dsc_metodoPago = agregaTextBox.Text;

            GlobalConfig.Connection.InsertMetodoPago(model);
            if (model.id_metodoPago != 0)
            {
                msgCreaLabel.Text = "Método de Pago Agregado Exitosamente";
                msgCreaLabel.ForeColor = System.Drawing.Color.Green;

                agregaTextBox.Text = "";
                agregaTextBox.Focus();
            }
            else
            {
                msgCreaLabel.Text = "Error al Crear, Favor vuelva a intentarlo nuevamente ...";
                msgCreaLabel.ForeColor = System.Drawing.Color.Red;

                agregaTextBox.Text = "";
                this.ActiveControl = agregaTextBox;
            }
        }
        #endregion
    }
}
