﻿namespace Libreria.UI.Forms.MetodoDePagos
{
    partial class EditMetodoDePagosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.guardarButton = new FontAwesome.Sharp.IconButton();
            this.editarLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.SuspendLayout();
            // 
            // cancelarButton
            // 
            this.cancelarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(367, 190);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarButton.TabIndex = 12;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // guardarButton
            // 
            this.guardarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.guardarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.guardarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.guardarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guardarButton.ForeColor = System.Drawing.Color.White;
            this.guardarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.guardarButton.IconColor = System.Drawing.Color.Black;
            this.guardarButton.IconSize = 16;
            this.guardarButton.Location = new System.Drawing.Point(114, 190);
            this.guardarButton.Name = "guardarButton";
            this.guardarButton.Rotation = 0D;
            this.guardarButton.Size = new System.Drawing.Size(174, 36);
            this.guardarButton.TabIndex = 11;
            this.guardarButton.Text = "Guardar";
            this.guardarButton.UseVisualStyleBackColor = false;
            this.guardarButton.Click += new System.EventHandler(this.guardarButton_Click);
            // 
            // editarLabel
            // 
            this.editarLabel.AutoSize = true;
            this.editarLabel.BackColor = System.Drawing.Color.White;
            this.editarLabel.Depth = 0;
            this.editarLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editarLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarLabel.Location = new System.Drawing.Point(110, 107);
            this.editarLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarLabel.Name = "editarLabel";
            this.editarLabel.Size = new System.Drawing.Size(140, 20);
            this.editarLabel.TabIndex = 10;
            this.editarLabel.Text = "Método de Pago:";
            // 
            // editarTextBox
            // 
            this.editarTextBox.BackColor = System.Drawing.Color.White;
            this.editarTextBox.Depth = 0;
            this.editarTextBox.Hint = "";
            this.editarTextBox.Location = new System.Drawing.Point(260, 104);
            this.editarTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarTextBox.Name = "editarTextBox";
            this.editarTextBox.PasswordChar = '\0';
            this.editarTextBox.SelectedText = "";
            this.editarTextBox.SelectionLength = 0;
            this.editarTextBox.SelectionStart = 0;
            this.editarTextBox.Size = new System.Drawing.Size(281, 23);
            this.editarTextBox.TabIndex = 9;
            this.editarTextBox.UseSystemPasswordChar = false;
            // 
            // EditMetodoDePagosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 286);
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.guardarButton);
            this.Controls.Add(this.editarLabel);
            this.Controls.Add(this.editarTextBox);
            this.Name = "EditMetodoDePagosForm";
            this.Text = "Editar Método de Pago";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton guardarButton;
        private MaterialSkin.Controls.MaterialLabel editarLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField editarTextBox;
    }
}