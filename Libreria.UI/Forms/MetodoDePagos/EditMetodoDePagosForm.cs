﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.MetodoDePagos
{
    public partial class EditMetodoDePagosForm : MaterialForm
    {
        private int id;

        #region Constructor
        public EditMetodoDePagosForm(MetodoPagoModel model)
        {
            InitializeComponent();

            editarTextBox.Text = model.dsc_metodoPago;
            id = model.id_metodoPago;

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            editarTextBox.Enabled = true;
        }
        #endregion

        #region Events
        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name == "SearchMetodoDePagosForm")
                {
                    Application.OpenForms[i].Show();
                }
            }
        }

        private void guardarButton_Click(object sender, EventArgs e)
        {
            MetodoPagoModel model = new MetodoPagoModel();
            model.dsc_metodoPago = editarTextBox.Text;
            model.id_metodoPago = id;

            try
            {
                GlobalConfig.Connection.EditMetodoPago(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("El método de pago se ha guardado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}
