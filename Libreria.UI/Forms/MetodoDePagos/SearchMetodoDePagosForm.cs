﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Libreria.UI.Forms.MetodoDePagos
{
    public partial class SearchMetodoDePagosForm : MaterialForm
    {
        List<MetodoPagoModel> metodoPagos;
        private string dscMetodoPago;
        private int id;
        private string idTmp;

        #region Constructor
        public SearchMetodoDePagosForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            LoadDataForm();
        }
        #endregion

        #region Methods
        public void LoadDataForm()
        {
            metodoPagos = GlobalConfig.Connection.GetMetodoPago_All();

            buscarListBox.DataSource = null;

            buscarListBox.DataSource = metodoPagos;
            buscarListBox.DisplayMember = "dsc_metodoPago";
            buscarListBox.ValueMember = "id_metodoPago";

            editarButton.Enabled = false;
            eliminarButton.Enabled = false;
            agregarButton.Enabled = true;
        }
        #endregion

        #region Events
        private void buscarListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            editarButton.Enabled = true;
            eliminarButton.Enabled = true;

            if (buscarListBox != null & buscarListBox.Text != "")
            {
                dscMetodoPago = buscarListBox.GetItemText(buscarListBox.SelectedItem);
                idTmp = buscarListBox.SelectedValue.ToString();

                int.TryParse(idTmp, out id);
            }
        }

        private void editarButton_Click(object sender, EventArgs e)
        {
            MetodoPagoModel model = new MetodoPagoModel();
            model.dsc_metodoPago = dscMetodoPago;
            model.id_metodoPago = id;

            EditMetodoDePagosForm frmEditMetodoPago = new EditMetodoDePagosForm(model);
            frmEditMetodoPago.StartPosition = FormStartPosition.CenterParent;
            frmEditMetodoPago.Tag = this;
            frmEditMetodoPago.ShowDialog(this);

            LoadDataForm();
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            MetodoPagoModel model = new MetodoPagoModel();
            model.dsc_metodoPago = dscMetodoPago;
            model.id_metodoPago = id;



            DeleteMetodoDePagosForm frmDeleteMetodoPago = new DeleteMetodoDePagosForm(model);
            frmDeleteMetodoPago.StartPosition = FormStartPosition.CenterParent;
            frmDeleteMetodoPago.Tag = this;
            frmDeleteMetodoPago.ShowDialog(this);

            LoadDataForm();
        }

        private void agregarButton_Click(object sender, EventArgs e)
        {
            CreateMetodoDePagosForm frmCreateMetodoPago = new CreateMetodoDePagosForm();
            frmCreateMetodoPago.StartPosition = FormStartPosition.CenterParent;
            frmCreateMetodoPago.Tag = this;
            frmCreateMetodoPago.ShowDialog(this);

            LoadDataForm();
        }

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
