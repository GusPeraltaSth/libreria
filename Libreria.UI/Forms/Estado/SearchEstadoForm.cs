﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Estado
{
    public partial class SearchEstadoForm : MaterialForm
    {
        private List<EstadoModel> estados;

        #region Constructor
        public SearchEstadoForm()
        {

            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            LoadDataForm();
        }
        #endregion

        #region Methods
        public void LoadDataForm()
        {
            estados = GlobalConfig.Connection.GetEstado_All();

            buscarEstadoListBox.DataSource = null;

            buscarEstadoListBox.DisplayMember = "dsc_estado";
            buscarEstadoListBox.ValueMember = "id_estado";
            buscarEstadoListBox.DataSource = estados;

            editarButton.Enabled = false;
            eliminarButton.Enabled = false;
            agregarButton.Enabled = true;
        }
        #endregion

        #region Events
        private void agregarButton_Click(object sender, EventArgs e)
        {
            CreateEstadoForm frmEstadoCreate = new CreateEstadoForm();
            frmEstadoCreate.StartPosition = FormStartPosition.CenterParent;
            frmEstadoCreate.Tag = this;
            frmEstadoCreate.ShowDialog(this);

            LoadDataForm();
        }

        private void editarButton_Click(object sender, EventArgs e)
        {
            EstadoModel model = new EstadoModel();

            model.id_estado = int.Parse(buscarEstadoListBox.SelectedValue.ToString());
            model.dsc_estado = buscarEstadoListBox.GetItemText(buscarEstadoListBox.SelectedItem);

            EditEstadoForm frmEditEstado = new EditEstadoForm(model);
            frmEditEstado.StartPosition = FormStartPosition.CenterParent;
            frmEditEstado.Tag = this;
            frmEditEstado.ShowDialog(this);

            LoadDataForm();
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            EstadoModel model = new EstadoModel();
            model.id_estado = int.Parse(buscarEstadoListBox.SelectedValue.ToString());
            model.dsc_estado = buscarEstadoListBox.GetItemText(buscarEstadoListBox.SelectedItem);

            DeleteEstadoForm frmDeleteEstado = new DeleteEstadoForm(model);
            frmDeleteEstado.StartPosition = FormStartPosition.CenterParent;
            frmDeleteEstado.Tag = this;
            frmDeleteEstado.ShowDialog(this);

            LoadDataForm();
        }


        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buscarEstadoListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editarButton.Enabled == false)
            {
                editarButton.Enabled = true;
            }
            if (eliminarButton.Enabled == false)
            {
                eliminarButton.Enabled = true;
            }
        }
        #endregion
    }
}
