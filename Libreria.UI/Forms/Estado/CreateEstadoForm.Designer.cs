﻿namespace Libreria.UI.Forms.Estado
{
    partial class CreateEstadoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msgCreaLabel = new System.Windows.Forms.Label();
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.agregarButton = new FontAwesome.Sharp.IconButton();
            this.agregaLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregaTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.SuspendLayout();
            // 
            // msgCreaLabel
            // 
            this.msgCreaLabel.AutoSize = true;
            this.msgCreaLabel.BackColor = System.Drawing.Color.White;
            this.msgCreaLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgCreaLabel.Location = new System.Drawing.Point(190, 206);
            this.msgCreaLabel.Name = "msgCreaLabel";
            this.msgCreaLabel.Size = new System.Drawing.Size(53, 20);
            this.msgCreaLabel.TabIndex = 17;
            this.msgCreaLabel.Text = "label1";
            // 
            // cancelarButton
            // 
            this.cancelarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(440, 301);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarButton.TabIndex = 21;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // agregarButton
            // 
            this.agregarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.agregarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarButton.ForeColor = System.Drawing.Color.White;
            this.agregarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.agregarButton.IconColor = System.Drawing.Color.Black;
            this.agregarButton.IconSize = 16;
            this.agregarButton.Location = new System.Drawing.Point(190, 301);
            this.agregarButton.Name = "agregarButton";
            this.agregarButton.Rotation = 0D;
            this.agregarButton.Size = new System.Drawing.Size(174, 36);
            this.agregarButton.TabIndex = 20;
            this.agregarButton.Text = "Agregar";
            this.agregarButton.UseVisualStyleBackColor = false;
            this.agregarButton.Click += new System.EventHandler(this.agregarButton_Click);
            // 
            // agregaLabel
            // 
            this.agregaLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.agregaLabel.AutoSize = true;
            this.agregaLabel.BackColor = System.Drawing.Color.White;
            this.agregaLabel.Depth = 0;
            this.agregaLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.agregaLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.agregaLabel.Location = new System.Drawing.Point(186, 116);
            this.agregaLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregaLabel.Name = "agregaLabel";
            this.agregaLabel.Size = new System.Drawing.Size(60, 19);
            this.agregaLabel.TabIndex = 19;
            this.agregaLabel.Text = "Estado:";
            // 
            // agregaTextBox
            // 
            this.agregaTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.agregaTextBox.BackColor = System.Drawing.Color.White;
            this.agregaTextBox.Depth = 0;
            this.agregaTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregaTextBox.Hint = "";
            this.agregaTextBox.Location = new System.Drawing.Point(332, 113);
            this.agregaTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregaTextBox.Name = "agregaTextBox";
            this.agregaTextBox.PasswordChar = '\0';
            this.agregaTextBox.SelectedText = "";
            this.agregaTextBox.SelectionLength = 0;
            this.agregaTextBox.SelectionStart = 0;
            this.agregaTextBox.Size = new System.Drawing.Size(282, 23);
            this.agregaTextBox.TabIndex = 18;
            this.agregaTextBox.UseSystemPasswordChar = false;
            // 
            // CreateEstadoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.msgCreaLabel);
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.agregarButton);
            this.Controls.Add(this.agregaLabel);
            this.Controls.Add(this.agregaTextBox);
            this.Name = "CreateEstadoForm";
            this.Text = "Crea Estado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label msgCreaLabel;
        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton agregarButton;
        public MaterialSkin.Controls.MaterialLabel agregaLabel;
        public MaterialSkin.Controls.MaterialSingleLineTextField agregaTextBox;
    }
}