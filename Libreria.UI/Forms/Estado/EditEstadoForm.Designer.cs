﻿namespace Libreria.UI.Forms.Estado
{
    partial class EditEstadoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.guardarButton = new FontAwesome.Sharp.IconButton();
            this.editarEstadoLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarEstadoTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.SuspendLayout();
            // 
            // cancelarButton
            // 
            this.cancelarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(380, 189);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarButton.TabIndex = 12;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // guardarButton
            // 
            this.guardarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.guardarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.guardarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.guardarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guardarButton.ForeColor = System.Drawing.Color.White;
            this.guardarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.guardarButton.IconColor = System.Drawing.Color.Black;
            this.guardarButton.IconSize = 16;
            this.guardarButton.Location = new System.Drawing.Point(127, 189);
            this.guardarButton.Name = "guardarButton";
            this.guardarButton.Rotation = 0D;
            this.guardarButton.Size = new System.Drawing.Size(174, 36);
            this.guardarButton.TabIndex = 11;
            this.guardarButton.Text = "Guardar";
            this.guardarButton.UseVisualStyleBackColor = false;
            this.guardarButton.Click += new System.EventHandler(this.guardarButton_Click);
            // 
            // editarEstadoLabel
            // 
            this.editarEstadoLabel.AutoSize = true;
            this.editarEstadoLabel.BackColor = System.Drawing.Color.White;
            this.editarEstadoLabel.Depth = 0;
            this.editarEstadoLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarEstadoLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarEstadoLabel.Location = new System.Drawing.Point(123, 117);
            this.editarEstadoLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarEstadoLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarEstadoLabel.Name = "editarEstadoLabel";
            this.editarEstadoLabel.Size = new System.Drawing.Size(60, 19);
            this.editarEstadoLabel.TabIndex = 10;
            this.editarEstadoLabel.Text = "Estado:";
            // 
            // editarEstadoTextBox
            // 
            this.editarEstadoTextBox.BackColor = System.Drawing.Color.White;
            this.editarEstadoTextBox.Depth = 0;
            this.editarEstadoTextBox.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editarEstadoTextBox.Hint = "";
            this.editarEstadoTextBox.Location = new System.Drawing.Point(251, 117);
            this.editarEstadoTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarEstadoTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarEstadoTextBox.Name = "editarEstadoTextBox";
            this.editarEstadoTextBox.PasswordChar = '\0';
            this.editarEstadoTextBox.SelectedText = "";
            this.editarEstadoTextBox.SelectionLength = 0;
            this.editarEstadoTextBox.SelectionStart = 0;
            this.editarEstadoTextBox.Size = new System.Drawing.Size(303, 23);
            this.editarEstadoTextBox.TabIndex = 9;
            this.editarEstadoTextBox.UseSystemPasswordChar = false;
            // 
            // EditEstadoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 286);
            this.ControlBox = false;
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.guardarButton);
            this.Controls.Add(this.editarEstadoLabel);
            this.Controls.Add(this.editarEstadoTextBox);
            this.Name = "EditEstadoForm";
            this.Text = "Editar Estado";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton guardarButton;
        private MaterialSkin.Controls.MaterialLabel editarEstadoLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField editarEstadoTextBox;
    }
}