﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Estado
{
    public partial class EditEstadoForm : MaterialForm
    {
        private int id;

        #region Constructor
        public EditEstadoForm(EstadoModel model)
        {
            InitializeComponent();

            editarEstadoTextBox.Text = model.dsc_estado;
            id = model.id_estado;


            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
        }
        #endregion

        #region Events
        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name == "SearchEstadoForm")
                {
                    Application.OpenForms[i].Show();
                }
            }
        }

        private void guardarButton_Click(object sender, EventArgs e)
        {
            EstadoModel model = new EstadoModel();
            model.dsc_estado = editarEstadoTextBox.Text;
            model.id_estado = id;

            try
            {
                GlobalConfig.Connection.EditEstado(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("El estado se ha guardado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}

