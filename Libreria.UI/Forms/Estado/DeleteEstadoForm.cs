﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Estado
{
    public partial class DeleteEstadoForm : MaterialForm
    {
        private string dscEstado;
        private int id;

        #region Constructor
        public DeleteEstadoForm(EstadoModel model)
        {
            InitializeComponent();

            id = model.id_estado;
            dscEstado = model.dsc_estado;

            InitializeDeleteForm(model.dsc_estado);
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);


        }
        #endregion

        #region Methods
        public void InitializeDeleteForm(string dscEstado)
        {
            eliminarEstadoLabel.Text = eliminarEstadoLabel.Text + " " + dscEstado + "?";
        }
        #endregion

        #region Events
        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name == "SearchEstadoForm")
                {
                    Application.OpenForms[i].Show();
                }
            }
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            EstadoModel model = new EstadoModel();
            model.dsc_estado = dscEstado;
            model.id_estado = id;

            try
            {
                GlobalConfig.Connection.DeleteEstado(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("El estado se ha eliminado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}
