﻿namespace Libreria.UI
{
    partial class CreateIdiomaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.agregaIdiomaTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.dscIdiomaLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.cancelarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.msgCreaIdiomaLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // agregaIdiomaTextBox
            // 
            this.agregaIdiomaTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.agregaIdiomaTextBox.Depth = 0;
            this.agregaIdiomaTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregaIdiomaTextBox.Hint = "";
            this.agregaIdiomaTextBox.Location = new System.Drawing.Point(272, 130);
            this.agregaIdiomaTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregaIdiomaTextBox.Name = "agregaIdiomaTextBox";
            this.agregaIdiomaTextBox.PasswordChar = '\0';
            this.agregaIdiomaTextBox.SelectedText = "";
            this.agregaIdiomaTextBox.SelectionLength = 0;
            this.agregaIdiomaTextBox.SelectionStart = 0;
            this.agregaIdiomaTextBox.Size = new System.Drawing.Size(286, 23);
            this.agregaIdiomaTextBox.TabIndex = 0;
            this.agregaIdiomaTextBox.UseSystemPasswordChar = false;
            this.agregaIdiomaTextBox.Click += new System.EventHandler(this.agregaIdiomaTextBox_Click);
            // 
            // dscIdiomaLabel
            // 
            this.dscIdiomaLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dscIdiomaLabel.AutoSize = true;
            this.dscIdiomaLabel.Depth = 0;
            this.dscIdiomaLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.dscIdiomaLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dscIdiomaLabel.Location = new System.Drawing.Point(130, 132);
            this.dscIdiomaLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.dscIdiomaLabel.Name = "dscIdiomaLabel";
            this.dscIdiomaLabel.Size = new System.Drawing.Size(117, 19);
            this.dscIdiomaLabel.TabIndex = 1;
            this.dscIdiomaLabel.Text = "Nombre Idioma:";
            // 
            // agregarIdiomaButton
            // 
            this.agregarIdiomaButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.agregarIdiomaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarIdiomaButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarIdiomaButton.ForeColor = System.Drawing.Color.White;
            this.agregarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.agregarIdiomaButton.IconColor = System.Drawing.Color.Black;
            this.agregarIdiomaButton.IconSize = 16;
            this.agregarIdiomaButton.Location = new System.Drawing.Point(134, 317);
            this.agregarIdiomaButton.Name = "agregarIdiomaButton";
            this.agregarIdiomaButton.Rotation = 0D;
            this.agregarIdiomaButton.Size = new System.Drawing.Size(174, 36);
            this.agregarIdiomaButton.TabIndex = 5;
            this.agregarIdiomaButton.Text = "Agregar";
            this.agregarIdiomaButton.UseVisualStyleBackColor = false;
            this.agregarIdiomaButton.Click += new System.EventHandler(this.agregarIdiomaButton_Click);
            // 
            // cancelarIdiomaButton
            // 
            this.cancelarIdiomaButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarIdiomaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarIdiomaButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarIdiomaButton.ForeColor = System.Drawing.Color.White;
            this.cancelarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarIdiomaButton.IconColor = System.Drawing.Color.Black;
            this.cancelarIdiomaButton.IconSize = 16;
            this.cancelarIdiomaButton.Location = new System.Drawing.Point(384, 317);
            this.cancelarIdiomaButton.Name = "cancelarIdiomaButton";
            this.cancelarIdiomaButton.Rotation = 0D;
            this.cancelarIdiomaButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarIdiomaButton.TabIndex = 6;
            this.cancelarIdiomaButton.Text = "Cancelar";
            this.cancelarIdiomaButton.UseVisualStyleBackColor = false;
            this.cancelarIdiomaButton.Click += new System.EventHandler(this.cancelarIdiomaButton_Click);
            // 
            // msgCreaIdiomaLabel
            // 
            this.msgCreaIdiomaLabel.AutoSize = true;
            this.msgCreaIdiomaLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgCreaIdiomaLabel.Location = new System.Drawing.Point(134, 222);
            this.msgCreaIdiomaLabel.Name = "msgCreaIdiomaLabel";
            this.msgCreaIdiomaLabel.Size = new System.Drawing.Size(57, 21);
            this.msgCreaIdiomaLabel.TabIndex = 0;
            this.msgCreaIdiomaLabel.Text = "label1";
            // 
            // CreateIdiomaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(700, 446);
            this.ControlBox = false;
            this.Controls.Add(this.msgCreaIdiomaLabel);
            this.Controls.Add(this.cancelarIdiomaButton);
            this.Controls.Add(this.agregarIdiomaButton);
            this.Controls.Add(this.dscIdiomaLabel);
            this.Controls.Add(this.agregaIdiomaTextBox);
            this.Name = "CreateIdiomaForm";
            this.Text = "Agregar Idioma";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public MaterialSkin.Controls.MaterialSingleLineTextField agregaIdiomaTextBox;
        public MaterialSkin.Controls.MaterialLabel dscIdiomaLabel;
        private FontAwesome.Sharp.IconButton agregarIdiomaButton;
        private FontAwesome.Sharp.IconButton cancelarIdiomaButton;
        private System.Windows.Forms.Label msgCreaIdiomaLabel;
    }
}