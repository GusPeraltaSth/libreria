﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI
{
    public partial class EditIdiomaForm : MaterialForm
    {
        private int id;

        #region Constructor
        public EditIdiomaForm(string idiomaLocal = "", int idLocal = 0)
        {
            InitializeComponent();

            editarIdiomaTextBox.Text = idiomaLocal;
            id = idLocal;

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            editarIdiomaTextBox.Enabled = true;
        }
        #endregion

        #region Methods
        #endregion

        #region Events
        private void cancelarIdiomaButton_Click(object sender, EventArgs e)
        {
            this.Close();
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name == "SearchIdiomaForm")
                {
                    Application.OpenForms[i].Show();
                }
            }
        }

        private void guardarIdiomaButton_Click(object sender, EventArgs e)
        {
            IdiomaModel model = new IdiomaModel();
            model.dsc_idioma = editarIdiomaTextBox.Text;
            model.id_idioma = id;

            try
            {
                GlobalConfig.Connection.EditIdioma(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("El idioma se ha guardado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}
