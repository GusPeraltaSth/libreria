﻿namespace Libreria.UI
{
    partial class SearchIdiomaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buscarIdiomaTextBox = new System.Windows.Forms.TextBox();
            this.buscarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.buscarIdiomaListBox = new System.Windows.Forms.ListBox();
            this.idiomaModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.editarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.eliminarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.cancelarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.agregarIdiomaButton = new FontAwesome.Sharp.IconButton();
            ((System.ComponentModel.ISupportInitialize)(this.idiomaModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // buscarIdiomaTextBox
            // 
            this.buscarIdiomaTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buscarIdiomaTextBox.Location = new System.Drawing.Point(212, 107);
            this.buscarIdiomaTextBox.Name = "buscarIdiomaTextBox";
            this.buscarIdiomaTextBox.Size = new System.Drawing.Size(359, 26);
            this.buscarIdiomaTextBox.TabIndex = 3;
            this.buscarIdiomaTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.buscarIdiomaTextBox_KeyPress);
            // 
            // buscarIdiomaButton
            // 
            this.buscarIdiomaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(192)))), ((int)(((byte)(222)))));
            this.buscarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buscarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.buscarIdiomaButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buscarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.buscarIdiomaButton.IconColor = System.Drawing.Color.Black;
            this.buscarIdiomaButton.IconSize = 16;
            this.buscarIdiomaButton.Location = new System.Drawing.Point(577, 101);
            this.buscarIdiomaButton.Name = "buscarIdiomaButton";
            this.buscarIdiomaButton.Rotation = 0D;
            this.buscarIdiomaButton.Size = new System.Drawing.Size(100, 36);
            this.buscarIdiomaButton.TabIndex = 5;
            this.buscarIdiomaButton.Text = "Buscar";
            this.buscarIdiomaButton.UseVisualStyleBackColor = false;
            // 
            // buscarIdiomaListBox
            // 
            this.buscarIdiomaListBox.DataSource = this.idiomaModelBindingSource;
            this.buscarIdiomaListBox.DisplayMember = "dsc_idioma";
            this.buscarIdiomaListBox.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buscarIdiomaListBox.FormattingEnabled = true;
            this.buscarIdiomaListBox.ItemHeight = 17;
            this.buscarIdiomaListBox.Location = new System.Drawing.Point(250, 139);
            this.buscarIdiomaListBox.Name = "buscarIdiomaListBox";
            this.buscarIdiomaListBox.Size = new System.Drawing.Size(283, 259);
            this.buscarIdiomaListBox.TabIndex = 6;
            this.buscarIdiomaListBox.ValueMember = "id_idioma";
            this.buscarIdiomaListBox.SelectedIndexChanged += new System.EventHandler(this.buscarIdiomaListBox_SelectedIndexChanged);
            // 
            // idiomaModelBindingSource
            // 
            this.idiomaModelBindingSource.DataSource = typeof(Libreria.DLL.Models.IdiomaModel);
            // 
            // editarIdiomaButton
            // 
            this.editarIdiomaButton.BackColor = System.Drawing.Color.White;
            this.editarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.editarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.editarIdiomaButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.editarIdiomaButton.IconSize = 22;
            this.editarIdiomaButton.Location = new System.Drawing.Point(539, 181);
            this.editarIdiomaButton.Name = "editarIdiomaButton";
            this.editarIdiomaButton.Rotation = 0D;
            this.editarIdiomaButton.Size = new System.Drawing.Size(36, 36);
            this.editarIdiomaButton.TabIndex = 7;
            this.editarIdiomaButton.UseVisualStyleBackColor = false;
            this.editarIdiomaButton.Click += new System.EventHandler(this.editarIdiomaButton_Click);
            // 
            // eliminarIdiomaButton
            // 
            this.eliminarIdiomaButton.BackColor = System.Drawing.Color.White;
            this.eliminarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eliminarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eliminarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.Trash;
            this.eliminarIdiomaButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.eliminarIdiomaButton.IconSize = 22;
            this.eliminarIdiomaButton.Location = new System.Drawing.Point(539, 223);
            this.eliminarIdiomaButton.Name = "eliminarIdiomaButton";
            this.eliminarIdiomaButton.Rotation = 0D;
            this.eliminarIdiomaButton.Size = new System.Drawing.Size(36, 36);
            this.eliminarIdiomaButton.TabIndex = 8;
            this.eliminarIdiomaButton.UseVisualStyleBackColor = false;
            this.eliminarIdiomaButton.Click += new System.EventHandler(this.eliminarIdiomaButton_Click);
            // 
            // cancelarIdiomaButton
            // 
            this.cancelarIdiomaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarIdiomaButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarIdiomaButton.IconColor = System.Drawing.Color.Black;
            this.cancelarIdiomaButton.IconSize = 16;
            this.cancelarIdiomaButton.Location = new System.Drawing.Point(577, 362);
            this.cancelarIdiomaButton.Name = "cancelarIdiomaButton";
            this.cancelarIdiomaButton.Rotation = 0D;
            this.cancelarIdiomaButton.Size = new System.Drawing.Size(100, 36);
            this.cancelarIdiomaButton.TabIndex = 9;
            this.cancelarIdiomaButton.Text = "Cancelar";
            this.cancelarIdiomaButton.UseVisualStyleBackColor = false;
            this.cancelarIdiomaButton.Click += new System.EventHandler(this.cancelarIdiomaButton_Click);
            // 
            // agregarIdiomaButton
            // 
            this.agregarIdiomaButton.BackColor = System.Drawing.Color.White;
            this.agregarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.agregarIdiomaButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarIdiomaButton.IconSize = 22;
            this.agregarIdiomaButton.Location = new System.Drawing.Point(539, 139);
            this.agregarIdiomaButton.Name = "agregarIdiomaButton";
            this.agregarIdiomaButton.Rotation = 0D;
            this.agregarIdiomaButton.Size = new System.Drawing.Size(36, 36);
            this.agregarIdiomaButton.TabIndex = 10;
            this.agregarIdiomaButton.UseVisualStyleBackColor = false;
            this.agregarIdiomaButton.Click += new System.EventHandler(this.agregarIdiomaButton_Click);
            // 
            // SearchIdiomaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(91)))), ((int)(((byte)(192)))), ((int)(((byte)(222)))));
            this.ClientSize = new System.Drawing.Size(784, 450);
            this.ControlBox = false;
            this.Controls.Add(this.agregarIdiomaButton);
            this.Controls.Add(this.cancelarIdiomaButton);
            this.Controls.Add(this.eliminarIdiomaButton);
            this.Controls.Add(this.editarIdiomaButton);
            this.Controls.Add(this.buscarIdiomaListBox);
            this.Controls.Add(this.buscarIdiomaButton);
            this.Controls.Add(this.buscarIdiomaTextBox);
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "SearchIdiomaForm";
            this.Text = "Idioma";
            ((System.ComponentModel.ISupportInitialize)(this.idiomaModelBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox buscarIdiomaTextBox;
        private FontAwesome.Sharp.IconButton buscarIdiomaButton;
        private System.Windows.Forms.ListBox buscarIdiomaListBox;
        private FontAwesome.Sharp.IconButton editarIdiomaButton;
        private FontAwesome.Sharp.IconButton eliminarIdiomaButton;
        private FontAwesome.Sharp.IconButton cancelarIdiomaButton;
        private System.Windows.Forms.BindingSource idiomaModelBindingSource;
        private FontAwesome.Sharp.IconButton agregarIdiomaButton;
    }
}