﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI
{
    public partial class DeleteIdiomaForm : MaterialForm
    {
        private string dscIdioma;
        private int id;

        #region Constructor
        public DeleteIdiomaForm(string dsc_idioma = "", int id_idioma = 0)
        {
            InitializeComponent();

            id = id_idioma;
            dscIdioma = dsc_idioma;
            InitializeDeleteForm(dscIdioma);

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
        }
        #endregion

        #region Methods
        public void InitializeDeleteForm(string dscIdioma)
        {
            eliminarIdiomaLabel.Text = eliminarIdiomaLabel.Text + " " + dscIdioma + "?";
        }
        #endregion

        #region Events
        private void cancelarIdiomaButton_Click(object sender, EventArgs e)
        {
            this.Close();
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name == "SearchIdiomaForm")
                {
                    Application.OpenForms[i].Show();
                }
            }
        }
        private void eliminarIdiomaButton_Click(object sender, EventArgs e)
        {
            IdiomaModel model = new IdiomaModel();
            model.dsc_idioma = dscIdioma;
            model.id_idioma = id;

            try
            {
                GlobalConfig.Connection.DeleteIdioma(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("El idioma se ha eliminado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}
