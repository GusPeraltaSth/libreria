﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Libreria.UI
{
    public partial class SearchIdiomaForm : MaterialForm
    {
        private List<IdiomaModel> availableIdioma;
        private string descIdioma;
        private string idTmp;
        private int id;

        #region Constructor
        /// <summary>
        /// Método Constructor Formulario Buscar Idioma
        /// </summary>
        public SearchIdiomaForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            LoadDataForm();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Método de Inicialización de los componentes del formulario
        /// </summary>
        public void LoadDataForm()
        {
            buscarIdiomaButton.Enabled = false;
            buscarIdiomaButton.Visible = false;

            buscarIdiomaTextBox.Enabled = false;
            buscarIdiomaTextBox.Visible = false;

            availableIdioma = GlobalConfig.Connection.GetIdioma_All();

            buscarIdiomaListBox.DataSource = null;

            buscarIdiomaListBox.DataSource = availableIdioma;
            buscarIdiomaListBox.DisplayMember = "dsc_idioma";
            buscarIdiomaListBox.ValueMember = "id_idioma";

            editarIdiomaButton.Enabled = false;
            eliminarIdiomaButton.Enabled = false;
            agregarIdiomaButton.Enabled = true;
        }
        #endregion

        #region Events
        /// <summary>
        ///Evento al cambiar la seleccion de la lista
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buscarIdiomaListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            editarIdiomaButton.Enabled = true;
            eliminarIdiomaButton.Enabled = true;

            if (buscarIdiomaListBox != null & buscarIdiomaListBox.Text != "")
            {
                descIdioma = buscarIdiomaListBox.GetItemText(buscarIdiomaListBox.SelectedItem);
                idTmp = buscarIdiomaListBox.SelectedValue.ToString();

                int.TryParse(idTmp, out id);
            }
        }

        /// <summary>
        /// Evento que se dispara al presionar el botón editar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editarIdiomaButton_Click(object sender, EventArgs e)
        {
            EditIdiomaForm frmEditIdioma = new EditIdiomaForm(descIdioma, id);
            frmEditIdioma.StartPosition = FormStartPosition.CenterParent;
            frmEditIdioma.Tag = this;
            frmEditIdioma.ShowDialog(this);

            LoadDataForm();
        }

        /// <summary>
        /// Evento que se dispara al presionar el botón eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void eliminarIdiomaButton_Click(object sender, EventArgs e)
        {

            DeleteIdiomaForm frmDeleteIdioma = new DeleteIdiomaForm(descIdioma, id);
            frmDeleteIdioma.StartPosition = FormStartPosition.CenterParent;
            frmDeleteIdioma.Tag = this;
            frmDeleteIdioma.ShowDialog(this);

            LoadDataForm();
        }

        /// <summary>
        /// Evento que agrega un nuevo idioma
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void agregarIdiomaButton_Click(object sender, EventArgs e)
        {
            CreateIdiomaForm createIdiomaForm = new CreateIdiomaForm();
            createIdiomaForm.StartPosition = FormStartPosition.CenterParent;
            createIdiomaForm.Tag = this;
            createIdiomaForm.ShowDialog(this);

            LoadDataForm();
        }

        /// <summary>
        /// Evento que se dispara al presionar el botón cancelar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cancelarIdiomaButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Evento que se dispara al escribir en la caja de texto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buscarIdiomaTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            List<IdiomaModel> searchIdioma;
            searchIdioma = GlobalConfig.Connection.SearchIdioma(buscarIdiomaTextBox.Text);

            buscarIdiomaListBox.DataSource = searchIdioma;
            buscarIdiomaListBox.DisplayMember = "dsc_idioma";
            buscarIdiomaListBox.ValueMember = "id_idioma";
        }
        #endregion
    }
}
