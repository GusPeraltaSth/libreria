﻿namespace Libreria.UI
{
    partial class EditIdiomaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.editarIdiomaTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.editarIdiomaLabel = new MaterialSkin.Controls.MaterialLabel();
            this.cancelarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.guardarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.SuspendLayout();
            // 
            // editarIdiomaTextBox
            // 
            this.editarIdiomaTextBox.Depth = 0;
            this.editarIdiomaTextBox.Hint = "";
            this.editarIdiomaTextBox.Location = new System.Drawing.Point(244, 119);
            this.editarIdiomaTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarIdiomaTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarIdiomaTextBox.Name = "editarIdiomaTextBox";
            this.editarIdiomaTextBox.PasswordChar = '\0';
            this.editarIdiomaTextBox.SelectedText = "";
            this.editarIdiomaTextBox.SelectionLength = 0;
            this.editarIdiomaTextBox.SelectionStart = 0;
            this.editarIdiomaTextBox.Size = new System.Drawing.Size(303, 23);
            this.editarIdiomaTextBox.TabIndex = 0;
            this.editarIdiomaTextBox.UseSystemPasswordChar = false;
            // 
            // editarIdiomaLabel
            // 
            this.editarIdiomaLabel.AutoSize = true;
            this.editarIdiomaLabel.Depth = 0;
            this.editarIdiomaLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarIdiomaLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarIdiomaLabel.Location = new System.Drawing.Point(116, 119);
            this.editarIdiomaLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarIdiomaLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarIdiomaLabel.Name = "editarIdiomaLabel";
            this.editarIdiomaLabel.Size = new System.Drawing.Size(59, 19);
            this.editarIdiomaLabel.TabIndex = 1;
            this.editarIdiomaLabel.Text = "Idioma:";
            // 
            // cancelarIdiomaButton
            // 
            this.cancelarIdiomaButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarIdiomaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarIdiomaButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarIdiomaButton.ForeColor = System.Drawing.Color.White;
            this.cancelarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarIdiomaButton.IconColor = System.Drawing.Color.Black;
            this.cancelarIdiomaButton.IconSize = 16;
            this.cancelarIdiomaButton.Location = new System.Drawing.Point(373, 202);
            this.cancelarIdiomaButton.Name = "cancelarIdiomaButton";
            this.cancelarIdiomaButton.Rotation = 0D;
            this.cancelarIdiomaButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarIdiomaButton.TabIndex = 8;
            this.cancelarIdiomaButton.Text = "Cancelar";
            this.cancelarIdiomaButton.UseVisualStyleBackColor = false;
            this.cancelarIdiomaButton.Click += new System.EventHandler(this.cancelarIdiomaButton_Click);
            // 
            // guardarIdiomaButton
            // 
            this.guardarIdiomaButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.guardarIdiomaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.guardarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.guardarIdiomaButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guardarIdiomaButton.ForeColor = System.Drawing.Color.White;
            this.guardarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.guardarIdiomaButton.IconColor = System.Drawing.Color.Black;
            this.guardarIdiomaButton.IconSize = 16;
            this.guardarIdiomaButton.Location = new System.Drawing.Point(120, 202);
            this.guardarIdiomaButton.Name = "guardarIdiomaButton";
            this.guardarIdiomaButton.Rotation = 0D;
            this.guardarIdiomaButton.Size = new System.Drawing.Size(174, 36);
            this.guardarIdiomaButton.TabIndex = 7;
            this.guardarIdiomaButton.Text = "Guardar";
            this.guardarIdiomaButton.UseVisualStyleBackColor = false;
            this.guardarIdiomaButton.Click += new System.EventHandler(this.guardarIdiomaButton_Click);
            // 
            // EditIdiomaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 286);
            this.ControlBox = false;
            this.Controls.Add(this.cancelarIdiomaButton);
            this.Controls.Add(this.guardarIdiomaButton);
            this.Controls.Add(this.editarIdiomaLabel);
            this.Controls.Add(this.editarIdiomaTextBox);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "EditIdiomaForm";
            this.Sizable = false;
            this.Text = "Editar Idioma";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField editarIdiomaTextBox;
        private MaterialSkin.Controls.MaterialLabel editarIdiomaLabel;
        private FontAwesome.Sharp.IconButton cancelarIdiomaButton;
        private FontAwesome.Sharp.IconButton guardarIdiomaButton;
    }
}