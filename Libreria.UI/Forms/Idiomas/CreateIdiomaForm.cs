﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;

namespace Libreria.UI
{
    public partial class CreateIdiomaForm : MaterialForm
    {
        #region Constructor
        public CreateIdiomaForm()
        {
            InitializeComponent();
            msgCreaIdiomaLabel.Text = "";

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
        }
        #endregion

        #region Methods
        #endregion

        #region Events
        private void cancelarIdiomaButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void agregarIdiomaButton_Click(object sender, EventArgs e)
        {
            IdiomaModel model = new IdiomaModel();
            model.dsc_idioma = agregaIdiomaTextBox.Text;

            GlobalConfig.Connection.InsertIdioma(model);
            if (model.id_idioma != 0)
            {
                msgCreaIdiomaLabel.Text = "Idioma Agregado Exitosamente";
                msgCreaIdiomaLabel.ForeColor = System.Drawing.Color.Green;

                agregaIdiomaTextBox.Text = "";
                agregaIdiomaTextBox.Focus();
            }
            else
            {
                msgCreaIdiomaLabel.Text = "Error al Crear, Favor vuelva a intentarlo nuevamente ...";
                msgCreaIdiomaLabel.ForeColor = System.Drawing.Color.Red;

                agregaIdiomaTextBox.Text = "";
                this.ActiveControl = agregaIdiomaTextBox;
            }
        }

        private void agregaIdiomaTextBox_Click(object sender, EventArgs e)
        {
            msgCreaIdiomaLabel.Text = "";
        }
        #endregion
    }
}
