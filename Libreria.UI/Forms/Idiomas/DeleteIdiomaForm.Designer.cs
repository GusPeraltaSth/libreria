﻿namespace Libreria.UI
{
    partial class DeleteIdiomaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.eliminarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.eliminarIdiomaLabel = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // cancelarIdiomaButton
            // 
            this.cancelarIdiomaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarIdiomaButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarIdiomaButton.ForeColor = System.Drawing.Color.White;
            this.cancelarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarIdiomaButton.IconColor = System.Drawing.Color.Black;
            this.cancelarIdiomaButton.IconSize = 16;
            this.cancelarIdiomaButton.Location = new System.Drawing.Point(352, 229);
            this.cancelarIdiomaButton.Name = "cancelarIdiomaButton";
            this.cancelarIdiomaButton.Rotation = 0D;
            this.cancelarIdiomaButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarIdiomaButton.TabIndex = 12;
            this.cancelarIdiomaButton.Text = "Cancelar";
            this.cancelarIdiomaButton.UseVisualStyleBackColor = false;
            this.cancelarIdiomaButton.Click += new System.EventHandler(this.cancelarIdiomaButton_Click);
            // 
            // eliminarIdiomaButton
            // 
            this.eliminarIdiomaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.eliminarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eliminarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eliminarIdiomaButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eliminarIdiomaButton.ForeColor = System.Drawing.Color.White;
            this.eliminarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.eliminarIdiomaButton.IconColor = System.Drawing.Color.Black;
            this.eliminarIdiomaButton.IconSize = 16;
            this.eliminarIdiomaButton.Location = new System.Drawing.Point(149, 229);
            this.eliminarIdiomaButton.Name = "eliminarIdiomaButton";
            this.eliminarIdiomaButton.Rotation = 0D;
            this.eliminarIdiomaButton.Size = new System.Drawing.Size(174, 36);
            this.eliminarIdiomaButton.TabIndex = 11;
            this.eliminarIdiomaButton.Text = "Eliminar";
            this.eliminarIdiomaButton.UseVisualStyleBackColor = false;
            this.eliminarIdiomaButton.Click += new System.EventHandler(this.eliminarIdiomaButton_Click);
            // 
            // eliminarIdiomaLabel
            // 
            this.eliminarIdiomaLabel.AutoSize = true;
            this.eliminarIdiomaLabel.BackColor = System.Drawing.Color.White;
            this.eliminarIdiomaLabel.Depth = 0;
            this.eliminarIdiomaLabel.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eliminarIdiomaLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.eliminarIdiomaLabel.Location = new System.Drawing.Point(146, 130);
            this.eliminarIdiomaLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.eliminarIdiomaLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.eliminarIdiomaLabel.Name = "eliminarIdiomaLabel";
            this.eliminarIdiomaLabel.Size = new System.Drawing.Size(245, 20);
            this.eliminarIdiomaLabel.TabIndex = 10;
            this.eliminarIdiomaLabel.Text = "¿Está seguro que quiere eliminar";
            // 
            // DeleteIdiomaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 332);
            this.ControlBox = false;
            this.Controls.Add(this.cancelarIdiomaButton);
            this.Controls.Add(this.eliminarIdiomaButton);
            this.Controls.Add(this.eliminarIdiomaLabel);
            this.Name = "DeleteIdiomaForm";
            this.Text = "Eliminar Idioma";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarIdiomaButton;
        private FontAwesome.Sharp.IconButton eliminarIdiomaButton;
        private MaterialSkin.Controls.MaterialLabel eliminarIdiomaLabel;
    }
}