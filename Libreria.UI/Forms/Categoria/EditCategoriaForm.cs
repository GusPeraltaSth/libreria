﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Categoria
{
    public partial class EditCategoriaForm : MaterialForm
    {
        private int id;

        #region Constructor
        public EditCategoriaForm(string dscCategoria, int idCategoria)
        {
            InitializeComponent();
            editarCategoriaTextBox.Text = dscCategoria;
            id = idCategoria;

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            editarCategoriaTextBox.Enabled = true;
        }
        #endregion

        #region Events
        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name == "SearchCategoriaForm")
                {
                    Application.OpenForms[i].Show();
                }
            }
        }

        private void guardarButton_Click(object sender, EventArgs e)
        {
            CategoriaModel model = new CategoriaModel();
            model.dsc_categoria = editarCategoriaTextBox.Text;
            model.id_categoria = id;

            try
            {
                GlobalConfig.Connection.EditCategoria(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("La categoria se ha guardado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}
