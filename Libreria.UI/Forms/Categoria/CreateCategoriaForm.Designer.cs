﻿namespace Libreria.UI.Forms.Categoria
{
    partial class CreateCategoriaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msgCreaCategoriaLabel = new System.Windows.Forms.Label();
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.agregarButton = new FontAwesome.Sharp.IconButton();
            this.dscCategoriaLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregaCategoriaTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.SuspendLayout();
            // 
            // msgCreaCategoriaLabel
            // 
            this.msgCreaCategoriaLabel.AutoSize = true;
            this.msgCreaCategoriaLabel.BackColor = System.Drawing.Color.White;
            this.msgCreaCategoriaLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgCreaCategoriaLabel.Location = new System.Drawing.Point(86, 235);
            this.msgCreaCategoriaLabel.Name = "msgCreaCategoriaLabel";
            this.msgCreaCategoriaLabel.Size = new System.Drawing.Size(57, 21);
            this.msgCreaCategoriaLabel.TabIndex = 7;
            this.msgCreaCategoriaLabel.Text = "label1";
            // 
            // cancelarButton
            // 
            this.cancelarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(340, 317);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarButton.TabIndex = 11;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // agregarButton
            // 
            this.agregarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.agregarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarButton.ForeColor = System.Drawing.Color.White;
            this.agregarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.agregarButton.IconColor = System.Drawing.Color.Black;
            this.agregarButton.IconSize = 16;
            this.agregarButton.Location = new System.Drawing.Point(90, 317);
            this.agregarButton.Name = "agregarButton";
            this.agregarButton.Rotation = 0D;
            this.agregarButton.Size = new System.Drawing.Size(174, 36);
            this.agregarButton.TabIndex = 10;
            this.agregarButton.Text = "Agregar";
            this.agregarButton.UseVisualStyleBackColor = false;
            this.agregarButton.Click += new System.EventHandler(this.agregarButton_Click);
            // 
            // dscCategoriaLabel
            // 
            this.dscCategoriaLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dscCategoriaLabel.AutoSize = true;
            this.dscCategoriaLabel.BackColor = System.Drawing.Color.White;
            this.dscCategoriaLabel.Depth = 0;
            this.dscCategoriaLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.dscCategoriaLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.dscCategoriaLabel.Location = new System.Drawing.Point(86, 159);
            this.dscCategoriaLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.dscCategoriaLabel.Name = "dscCategoriaLabel";
            this.dscCategoriaLabel.Size = new System.Drawing.Size(136, 19);
            this.dscCategoriaLabel.TabIndex = 9;
            this.dscCategoriaLabel.Text = "Nombre Categoría:";
            // 
            // agregaCategoriaTextBox
            // 
            this.agregaCategoriaTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.agregaCategoriaTextBox.BackColor = System.Drawing.Color.White;
            this.agregaCategoriaTextBox.Depth = 0;
            this.agregaCategoriaTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregaCategoriaTextBox.Hint = "";
            this.agregaCategoriaTextBox.Location = new System.Drawing.Point(243, 156);
            this.agregaCategoriaTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregaCategoriaTextBox.Name = "agregaCategoriaTextBox";
            this.agregaCategoriaTextBox.PasswordChar = '\0';
            this.agregaCategoriaTextBox.SelectedText = "";
            this.agregaCategoriaTextBox.SelectionLength = 0;
            this.agregaCategoriaTextBox.SelectionStart = 0;
            this.agregaCategoriaTextBox.Size = new System.Drawing.Size(271, 23);
            this.agregaCategoriaTextBox.TabIndex = 8;
            this.agregaCategoriaTextBox.UseSystemPasswordChar = false;
            // 
            // CreateCategoriaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 466);
            this.ControlBox = false;
            this.Controls.Add(this.msgCreaCategoriaLabel);
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.agregarButton);
            this.Controls.Add(this.dscCategoriaLabel);
            this.Controls.Add(this.agregaCategoriaTextBox);
            this.Name = "CreateCategoriaForm";
            this.Text = "Crear Categoría";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label msgCreaCategoriaLabel;
        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton agregarButton;
        public MaterialSkin.Controls.MaterialLabel dscCategoriaLabel;
        public MaterialSkin.Controls.MaterialSingleLineTextField agregaCategoriaTextBox;
    }
}