﻿namespace Libreria.UI.Forms.Categoria
{
    partial class EditCategoriaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.guardarButton = new FontAwesome.Sharp.IconButton();
            this.editarCategoriaLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarCategoriaTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.SuspendLayout();
            // 
            // cancelarButton
            // 
            this.cancelarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(370, 206);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarButton.TabIndex = 12;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // guardarButton
            // 
            this.guardarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.guardarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.guardarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.guardarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guardarButton.ForeColor = System.Drawing.Color.White;
            this.guardarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.guardarButton.IconColor = System.Drawing.Color.Black;
            this.guardarButton.IconSize = 16;
            this.guardarButton.Location = new System.Drawing.Point(117, 206);
            this.guardarButton.Name = "guardarButton";
            this.guardarButton.Rotation = 0D;
            this.guardarButton.Size = new System.Drawing.Size(174, 36);
            this.guardarButton.TabIndex = 11;
            this.guardarButton.Text = "Guardar";
            this.guardarButton.UseVisualStyleBackColor = false;
            this.guardarButton.Click += new System.EventHandler(this.guardarButton_Click);
            // 
            // editarCategoriaLabel
            // 
            this.editarCategoriaLabel.AutoSize = true;
            this.editarCategoriaLabel.BackColor = System.Drawing.Color.White;
            this.editarCategoriaLabel.Depth = 0;
            this.editarCategoriaLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarCategoriaLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarCategoriaLabel.Location = new System.Drawing.Point(113, 122);
            this.editarCategoriaLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarCategoriaLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarCategoriaLabel.Name = "editarCategoriaLabel";
            this.editarCategoriaLabel.Size = new System.Drawing.Size(78, 19);
            this.editarCategoriaLabel.TabIndex = 10;
            this.editarCategoriaLabel.Text = "Categoría:";
            // 
            // editarCategoriaTextBox
            // 
            this.editarCategoriaTextBox.BackColor = System.Drawing.Color.White;
            this.editarCategoriaTextBox.Depth = 0;
            this.editarCategoriaTextBox.Hint = "";
            this.editarCategoriaTextBox.Location = new System.Drawing.Point(241, 122);
            this.editarCategoriaTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarCategoriaTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarCategoriaTextBox.Name = "editarCategoriaTextBox";
            this.editarCategoriaTextBox.PasswordChar = '\0';
            this.editarCategoriaTextBox.SelectedText = "";
            this.editarCategoriaTextBox.SelectionLength = 0;
            this.editarCategoriaTextBox.SelectionStart = 0;
            this.editarCategoriaTextBox.Size = new System.Drawing.Size(303, 23);
            this.editarCategoriaTextBox.TabIndex = 9;
            this.editarCategoriaTextBox.UseSystemPasswordChar = false;
            // 
            // EditCategoriaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 286);
            this.ControlBox = false;
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.guardarButton);
            this.Controls.Add(this.editarCategoriaLabel);
            this.Controls.Add(this.editarCategoriaTextBox);
            this.Name = "EditCategoriaForm";
            this.Text = "Editar Categoría";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton guardarButton;
        private MaterialSkin.Controls.MaterialLabel editarCategoriaLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField editarCategoriaTextBox;
    }
}