﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;

namespace Libreria.UI.Forms.Categoria
{
    public partial class CreateCategoriaForm : MaterialForm
    {
        #region Constructor
        public CreateCategoriaForm()
        {
            InitializeComponent();
            msgCreaCategoriaLabel.Text = "";

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
        }
        #endregion

        #region Methods
        #endregion

        #region Events
        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void agregarButton_Click(object sender, EventArgs e)
        {
            CategoriaModel model = new CategoriaModel();

            if (agregaCategoriaTextBox.Text != "")
            {
                model.dsc_categoria = agregaCategoriaTextBox.Text;

                GlobalConfig.Connection.InsertCategoria(model);
                if (model.id_categoria != 0)
                {
                    msgCreaCategoriaLabel.Text = "Idioma Agregado Exitosamente";
                    msgCreaCategoriaLabel.ForeColor = System.Drawing.Color.Green;

                    agregaCategoriaTextBox.Text = "";
                    agregaCategoriaTextBox.Focus();
                }
                else
                {
                    msgCreaCategoriaLabel.Text = "Error al Crear, Favor vuelva a intentarlo nuevamente ...";
                    msgCreaCategoriaLabel.ForeColor = System.Drawing.Color.Red;

                    agregaCategoriaTextBox.Text = "";
                    this.ActiveControl = agregaCategoriaTextBox;
                }
            }
            else
            {
                agregaCategoriaTextBox.Select();
                agregaCategoriaTextBox.Focus();
            }

        }
        #endregion
    }
}
