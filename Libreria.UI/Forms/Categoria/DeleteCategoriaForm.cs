﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Categoria
{
    public partial class DeleteCategoriaForm : MaterialForm
    {
        private int id;
        private string dscCategoria;

        #region Constructor
        public DeleteCategoriaForm(string dsc_Categoria, int id_categoria)
        {
            InitializeComponent();
            id = id_categoria;
            dscCategoria = dsc_Categoria;
            LoadDataForm(dscCategoria);


            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
        }
        #endregion

        #region Methods
        public void LoadDataForm(string dsc_Categoria)
        {
            eliminarCategoriaLabel.Text = eliminarCategoriaLabel.Text + " " + dsc_Categoria + "?";
        }

        #endregion

        #region Events
        private void eliminarButton_Click(object sender, EventArgs e)
        {
            CategoriaModel model = new CategoriaModel();
            model.dsc_categoria = dscCategoria;
            model.id_categoria = id;

            try
            {
                GlobalConfig.Connection.DeleteCategoria(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("La categoría se ha eliminado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
