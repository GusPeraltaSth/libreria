﻿namespace Libreria.UI.Forms.Categoria
{
    partial class SearchCategoriaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.agregarButton = new FontAwesome.Sharp.IconButton();
            this.cancelarIdiomaButton = new FontAwesome.Sharp.IconButton();
            this.eliminarButton = new FontAwesome.Sharp.IconButton();
            this.editarButton = new FontAwesome.Sharp.IconButton();
            this.buscarCategoriaListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // agregarButton
            // 
            this.agregarButton.BackColor = System.Drawing.Color.White;
            this.agregarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarButton.ForeColor = System.Drawing.Color.White;
            this.agregarButton.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.agregarButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarButton.IconSize = 22;
            this.agregarButton.Location = new System.Drawing.Point(489, 103);
            this.agregarButton.Name = "agregarButton";
            this.agregarButton.Rotation = 0D;
            this.agregarButton.Size = new System.Drawing.Size(36, 36);
            this.agregarButton.TabIndex = 15;
            this.agregarButton.UseVisualStyleBackColor = false;
            this.agregarButton.Click += new System.EventHandler(this.agregarButton_Click);
            // 
            // cancelarIdiomaButton
            // 
            this.cancelarIdiomaButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarIdiomaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarIdiomaButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarIdiomaButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarIdiomaButton.ForeColor = System.Drawing.Color.White;
            this.cancelarIdiomaButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarIdiomaButton.IconColor = System.Drawing.Color.Black;
            this.cancelarIdiomaButton.IconSize = 16;
            this.cancelarIdiomaButton.Location = new System.Drawing.Point(489, 326);
            this.cancelarIdiomaButton.Name = "cancelarIdiomaButton";
            this.cancelarIdiomaButton.Rotation = 0D;
            this.cancelarIdiomaButton.Size = new System.Drawing.Size(100, 36);
            this.cancelarIdiomaButton.TabIndex = 14;
            this.cancelarIdiomaButton.Text = "Cancelar";
            this.cancelarIdiomaButton.UseVisualStyleBackColor = false;
            this.cancelarIdiomaButton.Click += new System.EventHandler(this.cancelarIdiomaButton_Click);
            // 
            // eliminarButton
            // 
            this.eliminarButton.BackColor = System.Drawing.Color.White;
            this.eliminarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eliminarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eliminarButton.ForeColor = System.Drawing.Color.White;
            this.eliminarButton.IconChar = FontAwesome.Sharp.IconChar.Trash;
            this.eliminarButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.eliminarButton.IconSize = 22;
            this.eliminarButton.Location = new System.Drawing.Point(489, 187);
            this.eliminarButton.Name = "eliminarButton";
            this.eliminarButton.Rotation = 0D;
            this.eliminarButton.Size = new System.Drawing.Size(36, 36);
            this.eliminarButton.TabIndex = 13;
            this.eliminarButton.UseVisualStyleBackColor = false;
            this.eliminarButton.Click += new System.EventHandler(this.eliminarButton_Click);
            // 
            // editarButton
            // 
            this.editarButton.BackColor = System.Drawing.Color.White;
            this.editarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.editarButton.ForeColor = System.Drawing.Color.White;
            this.editarButton.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.editarButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.editarButton.IconSize = 22;
            this.editarButton.Location = new System.Drawing.Point(489, 145);
            this.editarButton.Name = "editarButton";
            this.editarButton.Rotation = 0D;
            this.editarButton.Size = new System.Drawing.Size(36, 36);
            this.editarButton.TabIndex = 12;
            this.editarButton.UseVisualStyleBackColor = false;
            this.editarButton.Click += new System.EventHandler(this.editarButton_Click);
            // 
            // buscarCategoriaListBox
            // 
            this.buscarCategoriaListBox.DisplayMember = "dsc_idioma";
            this.buscarCategoriaListBox.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buscarCategoriaListBox.FormattingEnabled = true;
            this.buscarCategoriaListBox.ItemHeight = 17;
            this.buscarCategoriaListBox.Location = new System.Drawing.Point(200, 103);
            this.buscarCategoriaListBox.Name = "buscarCategoriaListBox";
            this.buscarCategoriaListBox.Size = new System.Drawing.Size(283, 259);
            this.buscarCategoriaListBox.TabIndex = 11;
            this.buscarCategoriaListBox.ValueMember = "id_idioma";
            this.buscarCategoriaListBox.SelectedIndexChanged += new System.EventHandler(this.buscarCategoriaListBox_SelectedIndexChanged);
            // 
            // SearchCategoriaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 450);
            this.ControlBox = false;
            this.Controls.Add(this.agregarButton);
            this.Controls.Add(this.cancelarIdiomaButton);
            this.Controls.Add(this.eliminarButton);
            this.Controls.Add(this.editarButton);
            this.Controls.Add(this.buscarCategoriaListBox);
            this.Name = "SearchCategoriaForm";
            this.Text = "Categoría";
            this.ResumeLayout(false);

        }

        #endregion

        private FontAwesome.Sharp.IconButton agregarButton;
        private FontAwesome.Sharp.IconButton cancelarIdiomaButton;
        private FontAwesome.Sharp.IconButton eliminarButton;
        private FontAwesome.Sharp.IconButton editarButton;
        private System.Windows.Forms.ListBox buscarCategoriaListBox;
    }
}