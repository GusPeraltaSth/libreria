﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Categoria
{
    public partial class SearchCategoriaForm : MaterialForm
    {
        private List<CategoriaModel> categorias;
        private string dscCategoria;
        private string idTmp;
        private int id;

        #region Constructor
        public SearchCategoriaForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            LoadDataForm();
        }
        #endregion

        #region Methods

        public void LoadDataForm()
        {
            categorias = GlobalConfig.Connection.GetCategoria_All();

            buscarCategoriaListBox.DataSource = null;

            buscarCategoriaListBox.DisplayMember = "dsc_categoria";
            buscarCategoriaListBox.ValueMember = "id_categoria";
            buscarCategoriaListBox.DataSource = categorias;

            editarButton.Enabled = false;
            eliminarButton.Enabled = false;
            agregarButton.Enabled = true;
        }
        #endregion

        #region Events
        private void buscarCategoriaListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            editarButton.Enabled = true;
            eliminarButton.Enabled = true;

            if (buscarCategoriaListBox != null & buscarCategoriaListBox.Text != "")
            {
                dscCategoria = buscarCategoriaListBox.GetItemText(buscarCategoriaListBox.SelectedItem);
                idTmp = buscarCategoriaListBox.SelectedValue.ToString();

                int.TryParse(idTmp, out id);
            }
        }
        private void editarButton_Click(object sender, EventArgs e)
        {
            EditCategoriaForm frmEditCategoria = new EditCategoriaForm(dscCategoria, id);
            frmEditCategoria.StartPosition = FormStartPosition.CenterParent;
            frmEditCategoria.Tag = this;
            frmEditCategoria.ShowDialog(this);

            LoadDataForm();
        }
        private void eliminarButton_Click(object sender, EventArgs e)
        {
            DeleteCategoriaForm frmDeleteCategoria = new DeleteCategoriaForm(dscCategoria, id);
            frmDeleteCategoria.StartPosition = FormStartPosition.CenterParent;
            frmDeleteCategoria.Tag = this;
            frmDeleteCategoria.ShowDialog(this);

            LoadDataForm();
        }
        private void agregarButton_Click(object sender, EventArgs e)
        {
            CreateCategoriaForm frmCategoriaCreate = new CreateCategoriaForm();
            frmCategoriaCreate.StartPosition = FormStartPosition.CenterParent;
            frmCategoriaCreate.Tag = this;
            frmCategoriaCreate.ShowDialog(this);

            LoadDataForm();
        }

        private void cancelarIdiomaButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
