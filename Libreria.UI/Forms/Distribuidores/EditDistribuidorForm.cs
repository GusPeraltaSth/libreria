﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Distribuidores
{
    public partial class EditDistribuidorForm : MaterialForm
    {

        #region Constructor
        public EditDistribuidorForm(DistribuidorModel model)
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
            msgEditarLabel.Visible = false;

            LoadDataForm(model);

        }
        #endregion

        #region Methods
        private void LoadDataForm(DistribuidorModel model)
        {
            editarRutDistribuidorTextBox.Text = model.dis_rut;
            editarRutDistribuidorTextBox.Enabled = false;
            editarNombreDistribuidorTextBox.Text = model.dis_nombre_empresa;
            editarFechaIngresoDistribuidorDateTimePicker.Value = (model.dis_fecha_ingreso.Date);

            editarDireccionDistribuidorTextBox.Enabled = false;
        }
        #endregion

        #region Events
        private void cancelarEditarDistribuidorButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void editarGuardarDistribuidorButton_Click(object sender, System.EventArgs e)
        {
            DistribuidorModel model = new DistribuidorModel();
            model.dis_rut = editarRutDistribuidorTextBox.Text;
            model.dis_nombre_empresa = editarNombreDistribuidorTextBox.Text;
            model.dis_fecha_ingreso = editarFechaIngresoDistribuidorDateTimePicker.Value;

            try
            {
                GlobalConfig.Connection.EditDistribuidor(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("La categoria se ha guardado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}
