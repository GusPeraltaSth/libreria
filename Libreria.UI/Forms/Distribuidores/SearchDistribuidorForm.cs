﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Distribuidores
{
    public partial class SearchDistribuidorForm : MaterialForm
    {
        #region Constructor
        public SearchDistribuidorForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            LoadDataForm();
        }
        #endregion

        #region Methods
        public void LoadDataForm()
        {

            editarDistribuidorButton.Enabled = false;
            eliminarDistribuidorButton.Enabled = false;
            searchDistribuidorListView.FullRowSelect = true;

            List<DistribuidorModel> listDistribuidor = GlobalConfig.Connection.GetDistribuidor_All();

            foreach (DistribuidorModel d in listDistribuidor)
            {

                ListViewItem item = new ListViewItem(d.dis_rut.ToString());
                item.SubItems.Add(d.dis_nombre_empresa);
                item.SubItems.Add(d.dis_fecha_ingreso.ToString("dd/MM/yyyy"));

                searchDistribuidorListView.Items.Add(item);
            }
        }
        #endregion

        #region Events
        private void searchDistribuidorListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            eliminarDistribuidorButton.Enabled = true;
            editarDistribuidorButton.Enabled = true;
        }

        private void agregarDistribuidorButton_Click(object sender, EventArgs e)
        {
            string rutDis;



            CreateDistribuidorForm createDistribuidorForm = new CreateDistribuidorForm();
            createDistribuidorForm.StartPosition = FormStartPosition.CenterScreen;
            createDistribuidorForm.ShowDialog();

            rutDis = createDistribuidorForm.rutDis;
            List<DistribuidorModel> listDistribuidor = GlobalConfig.Connection.GetDistribuidor_All();

            foreach (DistribuidorModel d in listDistribuidor)
            {
                if (d.dis_rut == rutDis)
                {
                    ListViewItem item = new ListViewItem(d.dis_rut.ToString());
                    item.SubItems.Add(d.dis_nombre_empresa);
                    item.SubItems.Add(d.dis_fecha_ingreso.ToString("dd/MM/yyyy"));

                    searchDistribuidorListView.Items.Add(item);
                }
            }



        }

        private void eliminarDistribuidorButton_Click(object sender, EventArgs e)
        {
            DistribuidorModel model = new DistribuidorModel();
            ListViewItem item = searchDistribuidorListView.SelectedItems[0];
            model.dis_rut = item.Text;
            model.dis_nombre_empresa = item.SubItems[1].Text;

            DeleteDistribuidorForm deleteDistribuidorForm = new DeleteDistribuidorForm(model);
            deleteDistribuidorForm.StartPosition = FormStartPosition.CenterScreen;
            deleteDistribuidorForm.ShowDialog();

            if (deleteDistribuidorForm.flag != false)
            {
                foreach (ListViewItem i in searchDistribuidorListView.SelectedItems)
                {
                    i.Remove();
                }
            }
        }

        private void editarDistribuidorButton_Click(object sender, EventArgs e)
        {
            DistribuidorModel model = new DistribuidorModel();
            ListViewItem item = searchDistribuidorListView.SelectedItems[0];
            model.dis_rut = item.Text;
            model.dis_nombre_empresa = item.SubItems[1].Text;
            model.dis_fecha_ingreso = DateTime.Parse(item.SubItems[2].Text);

            EditDistribuidorForm editDistribuidorForm = new EditDistribuidorForm(model);
            editDistribuidorForm.StartPosition = FormStartPosition.CenterScreen;
            editDistribuidorForm.ShowDialog();

            searchDistribuidorListView.Refresh();
        }

        private void cancelarDistribuidorButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
