﻿namespace Libreria.UI.Forms
{
    partial class CreateDistribuidorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.agregarButton = new FontAwesome.Sharp.IconButton();
            this.msgCreaDistribuidorLabel = new System.Windows.Forms.Label();
            this.agregarPaisAutorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregarFechaIngresoDistribuidorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregarNombreDistribuidorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregarNombreDistribuidorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.agregarRutDistribuidorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.agregarRutDistribuidorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.agregarFechaIngresoDistribuidorDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.agregarDireccionDistribuidorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.agregarDireccionButton = new FontAwesome.Sharp.IconButton();
            this.searchButton = new FontAwesome.Sharp.IconButton();
            this.SuspendLayout();
            // 
            // cancelarButton
            // 
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(480, 419);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarButton.TabIndex = 28;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarDistribuidorButton_Click);
            // 
            // agregarButton
            // 
            this.agregarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarButton.ForeColor = System.Drawing.Color.White;
            this.agregarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.agregarButton.IconColor = System.Drawing.Color.Black;
            this.agregarButton.IconSize = 16;
            this.agregarButton.Location = new System.Drawing.Point(162, 419);
            this.agregarButton.Name = "agregarButton";
            this.agregarButton.Rotation = 0D;
            this.agregarButton.Size = new System.Drawing.Size(174, 36);
            this.agregarButton.TabIndex = 29;
            this.agregarButton.Text = "Agregar";
            this.agregarButton.UseVisualStyleBackColor = false;
            this.agregarButton.Click += new System.EventHandler(this.agregarButton_Click);
            // 
            // msgCreaDistribuidorLabel
            // 
            this.msgCreaDistribuidorLabel.AutoSize = true;
            this.msgCreaDistribuidorLabel.BackColor = System.Drawing.Color.White;
            this.msgCreaDistribuidorLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgCreaDistribuidorLabel.Location = new System.Drawing.Point(158, 358);
            this.msgCreaDistribuidorLabel.Name = "msgCreaDistribuidorLabel";
            this.msgCreaDistribuidorLabel.Size = new System.Drawing.Size(57, 21);
            this.msgCreaDistribuidorLabel.TabIndex = 31;
            this.msgCreaDistribuidorLabel.Text = "label1";
            // 
            // agregarPaisAutorLabel
            // 
            this.agregarPaisAutorLabel.AutoSize = true;
            this.agregarPaisAutorLabel.BackColor = System.Drawing.Color.White;
            this.agregarPaisAutorLabel.Depth = 0;
            this.agregarPaisAutorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.agregarPaisAutorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.agregarPaisAutorLabel.Location = new System.Drawing.Point(158, 304);
            this.agregarPaisAutorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.agregarPaisAutorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarPaisAutorLabel.Name = "agregarPaisAutorLabel";
            this.agregarPaisAutorLabel.Size = new System.Drawing.Size(77, 19);
            this.agregarPaisAutorLabel.TabIndex = 38;
            this.agregarPaisAutorLabel.Text = "Dirección:";
            // 
            // agregarFechaIngresoDistribuidorLabel
            // 
            this.agregarFechaIngresoDistribuidorLabel.AutoSize = true;
            this.agregarFechaIngresoDistribuidorLabel.BackColor = System.Drawing.Color.White;
            this.agregarFechaIngresoDistribuidorLabel.Depth = 0;
            this.agregarFechaIngresoDistribuidorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.agregarFechaIngresoDistribuidorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.agregarFechaIngresoDistribuidorLabel.Location = new System.Drawing.Point(158, 243);
            this.agregarFechaIngresoDistribuidorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.agregarFechaIngresoDistribuidorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarFechaIngresoDistribuidorLabel.Name = "agregarFechaIngresoDistribuidorLabel";
            this.agregarFechaIngresoDistribuidorLabel.Size = new System.Drawing.Size(107, 19);
            this.agregarFechaIngresoDistribuidorLabel.TabIndex = 37;
            this.agregarFechaIngresoDistribuidorLabel.Text = "Fecha Ingreso:";
            // 
            // agregarNombreDistribuidorLabel
            // 
            this.agregarNombreDistribuidorLabel.AutoSize = true;
            this.agregarNombreDistribuidorLabel.BackColor = System.Drawing.Color.White;
            this.agregarNombreDistribuidorLabel.Depth = 0;
            this.agregarNombreDistribuidorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.agregarNombreDistribuidorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.agregarNombreDistribuidorLabel.Location = new System.Drawing.Point(158, 172);
            this.agregarNombreDistribuidorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.agregarNombreDistribuidorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarNombreDistribuidorLabel.Name = "agregarNombreDistribuidorLabel";
            this.agregarNombreDistribuidorLabel.Size = new System.Drawing.Size(67, 19);
            this.agregarNombreDistribuidorLabel.TabIndex = 35;
            this.agregarNombreDistribuidorLabel.Text = "Nombre:";
            // 
            // agregarNombreDistribuidorTextBox
            // 
            this.agregarNombreDistribuidorTextBox.BackColor = System.Drawing.Color.White;
            this.agregarNombreDistribuidorTextBox.Depth = 0;
            this.agregarNombreDistribuidorTextBox.Hint = "";
            this.agregarNombreDistribuidorTextBox.Location = new System.Drawing.Point(309, 169);
            this.agregarNombreDistribuidorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.agregarNombreDistribuidorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarNombreDistribuidorTextBox.Name = "agregarNombreDistribuidorTextBox";
            this.agregarNombreDistribuidorTextBox.PasswordChar = '\0';
            this.agregarNombreDistribuidorTextBox.SelectedText = "";
            this.agregarNombreDistribuidorTextBox.SelectionLength = 0;
            this.agregarNombreDistribuidorTextBox.SelectionStart = 0;
            this.agregarNombreDistribuidorTextBox.Size = new System.Drawing.Size(345, 23);
            this.agregarNombreDistribuidorTextBox.TabIndex = 34;
            this.agregarNombreDistribuidorTextBox.UseSystemPasswordChar = false;
            // 
            // agregarRutDistribuidorLabel
            // 
            this.agregarRutDistribuidorLabel.AutoSize = true;
            this.agregarRutDistribuidorLabel.BackColor = System.Drawing.Color.White;
            this.agregarRutDistribuidorLabel.Depth = 0;
            this.agregarRutDistribuidorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.agregarRutDistribuidorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.agregarRutDistribuidorLabel.Location = new System.Drawing.Point(158, 92);
            this.agregarRutDistribuidorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.agregarRutDistribuidorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarRutDistribuidorLabel.Name = "agregarRutDistribuidorLabel";
            this.agregarRutDistribuidorLabel.Size = new System.Drawing.Size(41, 19);
            this.agregarRutDistribuidorLabel.TabIndex = 33;
            this.agregarRutDistribuidorLabel.Text = "RUT:";
            // 
            // agregarRutDistribuidorTextBox
            // 
            this.agregarRutDistribuidorTextBox.BackColor = System.Drawing.Color.White;
            this.agregarRutDistribuidorTextBox.Depth = 0;
            this.agregarRutDistribuidorTextBox.Hint = "";
            this.agregarRutDistribuidorTextBox.Location = new System.Drawing.Point(309, 89);
            this.agregarRutDistribuidorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.agregarRutDistribuidorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarRutDistribuidorTextBox.Name = "agregarRutDistribuidorTextBox";
            this.agregarRutDistribuidorTextBox.PasswordChar = '\0';
            this.agregarRutDistribuidorTextBox.SelectedText = "";
            this.agregarRutDistribuidorTextBox.SelectionLength = 0;
            this.agregarRutDistribuidorTextBox.SelectionStart = 0;
            this.agregarRutDistribuidorTextBox.Size = new System.Drawing.Size(345, 23);
            this.agregarRutDistribuidorTextBox.TabIndex = 32;
            this.agregarRutDistribuidorTextBox.UseSystemPasswordChar = false;
            // 
            // agregarFechaIngresoDistribuidorDateTimePicker
            // 
            this.agregarFechaIngresoDistribuidorDateTimePicker.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarFechaIngresoDistribuidorDateTimePicker.Location = new System.Drawing.Point(309, 241);
            this.agregarFechaIngresoDistribuidorDateTimePicker.Name = "agregarFechaIngresoDistribuidorDateTimePicker";
            this.agregarFechaIngresoDistribuidorDateTimePicker.Size = new System.Drawing.Size(345, 26);
            this.agregarFechaIngresoDistribuidorDateTimePicker.TabIndex = 40;
            // 
            // agregarDireccionDistribuidorTextBox
            // 
            this.agregarDireccionDistribuidorTextBox.BackColor = System.Drawing.Color.White;
            this.agregarDireccionDistribuidorTextBox.Depth = 0;
            this.agregarDireccionDistribuidorTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarDireccionDistribuidorTextBox.Hint = "";
            this.agregarDireccionDistribuidorTextBox.Location = new System.Drawing.Point(309, 299);
            this.agregarDireccionDistribuidorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.agregarDireccionDistribuidorTextBox.Name = "agregarDireccionDistribuidorTextBox";
            this.agregarDireccionDistribuidorTextBox.PasswordChar = '\0';
            this.agregarDireccionDistribuidorTextBox.SelectedText = "";
            this.agregarDireccionDistribuidorTextBox.SelectionLength = 0;
            this.agregarDireccionDistribuidorTextBox.SelectionStart = 0;
            this.agregarDireccionDistribuidorTextBox.Size = new System.Drawing.Size(261, 23);
            this.agregarDireccionDistribuidorTextBox.TabIndex = 41;
            this.agregarDireccionDistribuidorTextBox.UseSystemPasswordChar = false;
            // 
            // agregarDireccionButton
            // 
            this.agregarDireccionButton.BackColor = System.Drawing.Color.White;
            this.agregarDireccionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarDireccionButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarDireccionButton.ForeColor = System.Drawing.Color.White;
            this.agregarDireccionButton.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.agregarDireccionButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarDireccionButton.IconSize = 22;
            this.agregarDireccionButton.Location = new System.Drawing.Point(576, 296);
            this.agregarDireccionButton.Name = "agregarDireccionButton";
            this.agregarDireccionButton.Rotation = 0D;
            this.agregarDireccionButton.Size = new System.Drawing.Size(36, 36);
            this.agregarDireccionButton.TabIndex = 42;
            this.agregarDireccionButton.UseVisualStyleBackColor = false;
            this.agregarDireccionButton.Click += new System.EventHandler(this.agregarDireccionButton_Click);
            // 
            // searchButton
            // 
            this.searchButton.BackColor = System.Drawing.Color.White;
            this.searchButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.searchButton.ForeColor = System.Drawing.Color.White;
            this.searchButton.IconChar = FontAwesome.Sharp.IconChar.Binoculars;
            this.searchButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.searchButton.IconSize = 22;
            this.searchButton.Location = new System.Drawing.Point(618, 296);
            this.searchButton.Name = "searchButton";
            this.searchButton.Rotation = 0D;
            this.searchButton.Size = new System.Drawing.Size(36, 36);
            this.searchButton.TabIndex = 43;
            this.searchButton.UseVisualStyleBackColor = false;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // CreateDistribuidorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 498);
            this.ControlBox = false;
            this.Controls.Add(this.searchButton);
            this.Controls.Add(this.agregarDireccionButton);
            this.Controls.Add(this.agregarDireccionDistribuidorTextBox);
            this.Controls.Add(this.agregarFechaIngresoDistribuidorDateTimePicker);
            this.Controls.Add(this.agregarPaisAutorLabel);
            this.Controls.Add(this.agregarFechaIngresoDistribuidorLabel);
            this.Controls.Add(this.agregarNombreDistribuidorLabel);
            this.Controls.Add(this.agregarNombreDistribuidorTextBox);
            this.Controls.Add(this.agregarRutDistribuidorLabel);
            this.Controls.Add(this.agregarRutDistribuidorTextBox);
            this.Controls.Add(this.msgCreaDistribuidorLabel);
            this.Controls.Add(this.agregarButton);
            this.Controls.Add(this.cancelarButton);
            this.Name = "CreateDistribuidorForm";
            this.Text = "Agregar Distribuidor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton agregarButton;
        private System.Windows.Forms.Label msgCreaDistribuidorLabel;
        private MaterialSkin.Controls.MaterialLabel agregarPaisAutorLabel;
        private MaterialSkin.Controls.MaterialLabel agregarFechaIngresoDistribuidorLabel;
        private MaterialSkin.Controls.MaterialLabel agregarNombreDistribuidorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField agregarNombreDistribuidorTextBox;
        private MaterialSkin.Controls.MaterialLabel agregarRutDistribuidorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField agregarRutDistribuidorTextBox;
        private System.Windows.Forms.DateTimePicker agregarFechaIngresoDistribuidorDateTimePicker;
        private MaterialSkin.Controls.MaterialSingleLineTextField agregarDireccionDistribuidorTextBox;
        private FontAwesome.Sharp.IconButton agregarDireccionButton;
        private FontAwesome.Sharp.IconButton searchButton;
    }
}