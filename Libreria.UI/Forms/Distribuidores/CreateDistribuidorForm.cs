﻿using Libreria.DLL;
using Libreria.DLL.Models;
using Libreria.UI.Forms.Direccion;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Libreria.UI.Forms
{
    public partial class CreateDistribuidorForm : MaterialForm
    {
        public int idDir;
        public string rutDis;

        #region Constructor
        public CreateDistribuidorForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
            msgCreaDistribuidorLabel.Visible = false;
            agregarButton.Enabled = false;
        }
        #endregion

        #region Methods
        public Button createEditButton()
        {
            Button editButton = new Button();
            editButton.Text = "Editar";
            editButton.Size = new Size(50, 100);

            return editButton;
        }
        #endregion

        #region Events
        private void agregarDireccionButton_Click(object sender, EventArgs e)
        {
            List<DireccionModel> list = new List<DireccionModel>();
            string dir;
            using (var form = new CreateDireccionForm())
            {
                form.StartPosition = FormStartPosition.CenterParent;
                form.ShowDialog();
                idDir = form.idDir;

                if (idDir != 0)
                {
                    list = GlobalConfig.Connection.GetDirecciones_All_Params(idDir);
                }

                if (list != null)
                {
                    foreach (var l in list)
                    {
                        dir = l.calle + " " + l.no_calle + " " + l.urbanizacion + " " + l.no_urbanizacion + ", " + l.dsc_comuna;
                        agregarDireccionDistribuidorTextBox.Text = dir;
                    }
                }

                if (!string.IsNullOrEmpty(agregarDireccionDistribuidorTextBox.Text))
                {
                    if (agregarButton.Enabled == false)
                    {
                        agregarButton.Enabled = true;
                    }
                }
            }
        }

        private void cancelarDistribuidorButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void agregarButton_Click(object sender, EventArgs e)
        {
            DistribuidorModel model = new DistribuidorModel();
            DireccionModel modelDir = new DireccionModel();

            if (!string.IsNullOrEmpty(agregarRutDistribuidorTextBox.Text) && !string.IsNullOrEmpty(agregarNombreDistribuidorTextBox.Text) && !string.IsNullOrEmpty(agregarDireccionDistribuidorTextBox.Text))
            {
                model.dis_rut = agregarRutDistribuidorTextBox.Text;
                model.dis_nombre_empresa = agregarNombreDistribuidorTextBox.Text;
                model.dis_fecha_ingreso = agregarFechaIngresoDistribuidorDateTimePicker.Value;
                modelDir.id_direccion = idDir;

                GlobalConfig.Connection.InsertDistribuidor(model, modelDir);

                if (model.dis_rut != "")
                {
                    msgCreaDistribuidorLabel.Text = "Distribuidor Agregado Exitosamente";
                    msgCreaDistribuidorLabel.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    msgCreaDistribuidorLabel.Text = "Error al agregar, Favor vuelva a intentarlo nuevamente ...";
                    msgCreaDistribuidorLabel.ForeColor = System.Drawing.Color.Red;
                }

                rutDis = model.dis_rut;
                this.Close();
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            List<DireccionModel> list = new List<DireccionModel>();
            string dir;
            using (var form = new SearchDireccionForm())
            {
                form.StartPosition = FormStartPosition.CenterParent;
                form.ShowDialog();
                idDir = form.idDir;

                if (idDir != 0)
                {
                    list = GlobalConfig.Connection.GetDirecciones_All_Params(idDir);
                }

                if (list != null)
                {
                    foreach (var l in list)
                    {
                        dir = l.calle + " " + l.no_calle + " " + l.urbanizacion + " " + l.no_urbanizacion + ", " + l.dsc_comuna;
                        agregarDireccionDistribuidorTextBox.Text = dir;
                    }
                }

                if (!string.IsNullOrEmpty(agregarDireccionDistribuidorTextBox.Text))
                {
                    if (agregarButton.Enabled == false)
                    {
                        agregarButton.Enabled = true;
                    }
                }
            }
        }
        #endregion
    }
}
