﻿namespace Libreria.UI.Forms.Distribuidores
{
    partial class EditDistribuidorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.editarDireccionDistribuidorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.editarFechaIngresoDistribuidorDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.editarDireccionDistribuidorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarFechaIngresoDistribuidorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarNombreDistribuidorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarNombreDistribuidorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.editarRutDistribuidorLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarRutDistribuidorTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.msgEditarLabel = new System.Windows.Forms.Label();
            this.editarGuardarDistribuidorButton = new FontAwesome.Sharp.IconButton();
            this.cancelarEditarDistribuidorButton = new FontAwesome.Sharp.IconButton();
            this.SuspendLayout();
            // 
            // editarDireccionDistribuidorTextBox
            // 
            this.editarDireccionDistribuidorTextBox.BackColor = System.Drawing.Color.White;
            this.editarDireccionDistribuidorTextBox.Depth = 0;
            this.editarDireccionDistribuidorTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editarDireccionDistribuidorTextBox.Hint = "";
            this.editarDireccionDistribuidorTextBox.Location = new System.Drawing.Point(229, 314);
            this.editarDireccionDistribuidorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarDireccionDistribuidorTextBox.Name = "editarDireccionDistribuidorTextBox";
            this.editarDireccionDistribuidorTextBox.PasswordChar = '\0';
            this.editarDireccionDistribuidorTextBox.SelectedText = "";
            this.editarDireccionDistribuidorTextBox.SelectionLength = 0;
            this.editarDireccionDistribuidorTextBox.SelectionStart = 0;
            this.editarDireccionDistribuidorTextBox.Size = new System.Drawing.Size(345, 23);
            this.editarDireccionDistribuidorTextBox.TabIndex = 53;
            this.editarDireccionDistribuidorTextBox.UseSystemPasswordChar = false;
            // 
            // editarFechaIngresoDistribuidorDateTimePicker
            // 
            this.editarFechaIngresoDistribuidorDateTimePicker.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editarFechaIngresoDistribuidorDateTimePicker.Location = new System.Drawing.Point(229, 256);
            this.editarFechaIngresoDistribuidorDateTimePicker.Name = "editarFechaIngresoDistribuidorDateTimePicker";
            this.editarFechaIngresoDistribuidorDateTimePicker.Size = new System.Drawing.Size(345, 26);
            this.editarFechaIngresoDistribuidorDateTimePicker.TabIndex = 52;
            // 
            // editarDireccionDistribuidorLabel
            // 
            this.editarDireccionDistribuidorLabel.AutoSize = true;
            this.editarDireccionDistribuidorLabel.BackColor = System.Drawing.Color.White;
            this.editarDireccionDistribuidorLabel.Depth = 0;
            this.editarDireccionDistribuidorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarDireccionDistribuidorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarDireccionDistribuidorLabel.Location = new System.Drawing.Point(78, 319);
            this.editarDireccionDistribuidorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarDireccionDistribuidorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarDireccionDistribuidorLabel.Name = "editarDireccionDistribuidorLabel";
            this.editarDireccionDistribuidorLabel.Size = new System.Drawing.Size(77, 19);
            this.editarDireccionDistribuidorLabel.TabIndex = 51;
            this.editarDireccionDistribuidorLabel.Text = "Dirección:";
            // 
            // editarFechaIngresoDistribuidorLabel
            // 
            this.editarFechaIngresoDistribuidorLabel.AutoSize = true;
            this.editarFechaIngresoDistribuidorLabel.BackColor = System.Drawing.Color.White;
            this.editarFechaIngresoDistribuidorLabel.Depth = 0;
            this.editarFechaIngresoDistribuidorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarFechaIngresoDistribuidorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarFechaIngresoDistribuidorLabel.Location = new System.Drawing.Point(78, 258);
            this.editarFechaIngresoDistribuidorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarFechaIngresoDistribuidorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarFechaIngresoDistribuidorLabel.Name = "editarFechaIngresoDistribuidorLabel";
            this.editarFechaIngresoDistribuidorLabel.Size = new System.Drawing.Size(107, 19);
            this.editarFechaIngresoDistribuidorLabel.TabIndex = 50;
            this.editarFechaIngresoDistribuidorLabel.Text = "Fecha Ingreso:";
            // 
            // editarNombreDistribuidorLabel
            // 
            this.editarNombreDistribuidorLabel.AutoSize = true;
            this.editarNombreDistribuidorLabel.BackColor = System.Drawing.Color.White;
            this.editarNombreDistribuidorLabel.Depth = 0;
            this.editarNombreDistribuidorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarNombreDistribuidorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarNombreDistribuidorLabel.Location = new System.Drawing.Point(78, 187);
            this.editarNombreDistribuidorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarNombreDistribuidorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarNombreDistribuidorLabel.Name = "editarNombreDistribuidorLabel";
            this.editarNombreDistribuidorLabel.Size = new System.Drawing.Size(67, 19);
            this.editarNombreDistribuidorLabel.TabIndex = 49;
            this.editarNombreDistribuidorLabel.Text = "Nombre:";
            // 
            // editarNombreDistribuidorTextBox
            // 
            this.editarNombreDistribuidorTextBox.BackColor = System.Drawing.Color.White;
            this.editarNombreDistribuidorTextBox.Depth = 0;
            this.editarNombreDistribuidorTextBox.Hint = "";
            this.editarNombreDistribuidorTextBox.Location = new System.Drawing.Point(229, 184);
            this.editarNombreDistribuidorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarNombreDistribuidorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarNombreDistribuidorTextBox.Name = "editarNombreDistribuidorTextBox";
            this.editarNombreDistribuidorTextBox.PasswordChar = '\0';
            this.editarNombreDistribuidorTextBox.SelectedText = "";
            this.editarNombreDistribuidorTextBox.SelectionLength = 0;
            this.editarNombreDistribuidorTextBox.SelectionStart = 0;
            this.editarNombreDistribuidorTextBox.Size = new System.Drawing.Size(345, 23);
            this.editarNombreDistribuidorTextBox.TabIndex = 48;
            this.editarNombreDistribuidorTextBox.UseSystemPasswordChar = false;
            // 
            // editarRutDistribuidorLabel
            // 
            this.editarRutDistribuidorLabel.AutoSize = true;
            this.editarRutDistribuidorLabel.BackColor = System.Drawing.Color.White;
            this.editarRutDistribuidorLabel.Depth = 0;
            this.editarRutDistribuidorLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarRutDistribuidorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarRutDistribuidorLabel.Location = new System.Drawing.Point(78, 107);
            this.editarRutDistribuidorLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarRutDistribuidorLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarRutDistribuidorLabel.Name = "editarRutDistribuidorLabel";
            this.editarRutDistribuidorLabel.Size = new System.Drawing.Size(41, 19);
            this.editarRutDistribuidorLabel.TabIndex = 47;
            this.editarRutDistribuidorLabel.Text = "RUT:";
            // 
            // editarRutDistribuidorTextBox
            // 
            this.editarRutDistribuidorTextBox.BackColor = System.Drawing.Color.White;
            this.editarRutDistribuidorTextBox.Depth = 0;
            this.editarRutDistribuidorTextBox.Hint = "";
            this.editarRutDistribuidorTextBox.Location = new System.Drawing.Point(229, 104);
            this.editarRutDistribuidorTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarRutDistribuidorTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarRutDistribuidorTextBox.Name = "editarRutDistribuidorTextBox";
            this.editarRutDistribuidorTextBox.PasswordChar = '\0';
            this.editarRutDistribuidorTextBox.SelectedText = "";
            this.editarRutDistribuidorTextBox.SelectionLength = 0;
            this.editarRutDistribuidorTextBox.SelectionStart = 0;
            this.editarRutDistribuidorTextBox.Size = new System.Drawing.Size(345, 23);
            this.editarRutDistribuidorTextBox.TabIndex = 46;
            this.editarRutDistribuidorTextBox.UseSystemPasswordChar = false;
            // 
            // msgEditarLabel
            // 
            this.msgEditarLabel.AutoSize = true;
            this.msgEditarLabel.BackColor = System.Drawing.Color.White;
            this.msgEditarLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgEditarLabel.Location = new System.Drawing.Point(78, 373);
            this.msgEditarLabel.Name = "msgEditarLabel";
            this.msgEditarLabel.Size = new System.Drawing.Size(57, 21);
            this.msgEditarLabel.TabIndex = 45;
            this.msgEditarLabel.Text = "label1";
            // 
            // editarGuardarDistribuidorButton
            // 
            this.editarGuardarDistribuidorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.editarGuardarDistribuidorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editarGuardarDistribuidorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.editarGuardarDistribuidorButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editarGuardarDistribuidorButton.ForeColor = System.Drawing.Color.White;
            this.editarGuardarDistribuidorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.editarGuardarDistribuidorButton.IconColor = System.Drawing.Color.Black;
            this.editarGuardarDistribuidorButton.IconSize = 16;
            this.editarGuardarDistribuidorButton.Location = new System.Drawing.Point(82, 434);
            this.editarGuardarDistribuidorButton.Name = "editarGuardarDistribuidorButton";
            this.editarGuardarDistribuidorButton.Rotation = 0D;
            this.editarGuardarDistribuidorButton.Size = new System.Drawing.Size(174, 36);
            this.editarGuardarDistribuidorButton.TabIndex = 44;
            this.editarGuardarDistribuidorButton.Text = "Guardar";
            this.editarGuardarDistribuidorButton.UseVisualStyleBackColor = false;
            this.editarGuardarDistribuidorButton.Click += new System.EventHandler(this.editarGuardarDistribuidorButton_Click);
            // 
            // cancelarEditarDistribuidorButton
            // 
            this.cancelarEditarDistribuidorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarEditarDistribuidorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarEditarDistribuidorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarEditarDistribuidorButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarEditarDistribuidorButton.ForeColor = System.Drawing.Color.White;
            this.cancelarEditarDistribuidorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarEditarDistribuidorButton.IconColor = System.Drawing.Color.Black;
            this.cancelarEditarDistribuidorButton.IconSize = 16;
            this.cancelarEditarDistribuidorButton.Location = new System.Drawing.Point(400, 434);
            this.cancelarEditarDistribuidorButton.Name = "cancelarEditarDistribuidorButton";
            this.cancelarEditarDistribuidorButton.Rotation = 0D;
            this.cancelarEditarDistribuidorButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarEditarDistribuidorButton.TabIndex = 43;
            this.cancelarEditarDistribuidorButton.Text = "Cancelar";
            this.cancelarEditarDistribuidorButton.UseVisualStyleBackColor = false;
            this.cancelarEditarDistribuidorButton.Click += new System.EventHandler(this.cancelarEditarDistribuidorButton_Click);
            // 
            // EditDistribuidorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(653, 521);
            this.ControlBox = false;
            this.Controls.Add(this.editarDireccionDistribuidorTextBox);
            this.Controls.Add(this.editarFechaIngresoDistribuidorDateTimePicker);
            this.Controls.Add(this.editarDireccionDistribuidorLabel);
            this.Controls.Add(this.editarFechaIngresoDistribuidorLabel);
            this.Controls.Add(this.editarNombreDistribuidorLabel);
            this.Controls.Add(this.editarNombreDistribuidorTextBox);
            this.Controls.Add(this.editarRutDistribuidorLabel);
            this.Controls.Add(this.editarRutDistribuidorTextBox);
            this.Controls.Add(this.msgEditarLabel);
            this.Controls.Add(this.editarGuardarDistribuidorButton);
            this.Controls.Add(this.cancelarEditarDistribuidorButton);
            this.Name = "EditDistribuidorForm";
            this.Text = "Editar Distribuidor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialSingleLineTextField editarDireccionDistribuidorTextBox;
        private System.Windows.Forms.DateTimePicker editarFechaIngresoDistribuidorDateTimePicker;
        private MaterialSkin.Controls.MaterialLabel editarDireccionDistribuidorLabel;
        private MaterialSkin.Controls.MaterialLabel editarFechaIngresoDistribuidorLabel;
        private MaterialSkin.Controls.MaterialLabel editarNombreDistribuidorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField editarNombreDistribuidorTextBox;
        private MaterialSkin.Controls.MaterialLabel editarRutDistribuidorLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField editarRutDistribuidorTextBox;
        private System.Windows.Forms.Label msgEditarLabel;
        private FontAwesome.Sharp.IconButton editarGuardarDistribuidorButton;
        private FontAwesome.Sharp.IconButton cancelarEditarDistribuidorButton;
    }
}