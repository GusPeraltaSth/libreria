﻿namespace Libreria.UI.Forms.Distribuidores
{
    partial class SearchDistribuidorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchDistribuidorListView = new System.Windows.Forms.ListView();
            this.hdrRutDistribuidor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hdrNombreDistribuidor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hdrFechaIngresoDistribuidor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.agregarDistribuidorButton = new FontAwesome.Sharp.IconButton();
            this.cancelarDistribuidorButton = new FontAwesome.Sharp.IconButton();
            this.eliminarDistribuidorButton = new FontAwesome.Sharp.IconButton();
            this.editarDistribuidorButton = new FontAwesome.Sharp.IconButton();
            this.SuspendLayout();
            // 
            // searchDistribuidorListView
            // 
            this.searchDistribuidorListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hdrRutDistribuidor,
            this.hdrNombreDistribuidor,
            this.hdrFechaIngresoDistribuidor});
            this.searchDistribuidorListView.HideSelection = false;
            this.searchDistribuidorListView.Location = new System.Drawing.Point(91, 98);
            this.searchDistribuidorListView.Name = "searchDistribuidorListView";
            this.searchDistribuidorListView.Size = new System.Drawing.Size(314, 251);
            this.searchDistribuidorListView.TabIndex = 0;
            this.searchDistribuidorListView.UseCompatibleStateImageBehavior = false;
            this.searchDistribuidorListView.View = System.Windows.Forms.View.Details;
            this.searchDistribuidorListView.SelectedIndexChanged += new System.EventHandler(this.searchDistribuidorListView_SelectedIndexChanged);
            // 
            // hdrRutDistribuidor
            // 
            this.hdrRutDistribuidor.Text = "RUT";
            this.hdrRutDistribuidor.Width = 80;
            // 
            // hdrNombreDistribuidor
            // 
            this.hdrNombreDistribuidor.Text = "Nombre";
            this.hdrNombreDistribuidor.Width = 150;
            // 
            // hdrFechaIngresoDistribuidor
            // 
            this.hdrFechaIngresoDistribuidor.Text = "Fecha Ingreso";
            this.hdrFechaIngresoDistribuidor.Width = 80;
            // 
            // agregarDistribuidorButton
            // 
            this.agregarDistribuidorButton.BackColor = System.Drawing.Color.White;
            this.agregarDistribuidorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarDistribuidorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarDistribuidorButton.ForeColor = System.Drawing.Color.White;
            this.agregarDistribuidorButton.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.agregarDistribuidorButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarDistribuidorButton.IconSize = 22;
            this.agregarDistribuidorButton.Location = new System.Drawing.Point(411, 98);
            this.agregarDistribuidorButton.Name = "agregarDistribuidorButton";
            this.agregarDistribuidorButton.Rotation = 0D;
            this.agregarDistribuidorButton.Size = new System.Drawing.Size(36, 36);
            this.agregarDistribuidorButton.TabIndex = 28;
            this.agregarDistribuidorButton.UseVisualStyleBackColor = false;
            this.agregarDistribuidorButton.Click += new System.EventHandler(this.agregarDistribuidorButton_Click);
            // 
            // cancelarDistribuidorButton
            // 
            this.cancelarDistribuidorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarDistribuidorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarDistribuidorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarDistribuidorButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarDistribuidorButton.ForeColor = System.Drawing.Color.White;
            this.cancelarDistribuidorButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarDistribuidorButton.IconColor = System.Drawing.Color.Black;
            this.cancelarDistribuidorButton.IconSize = 16;
            this.cancelarDistribuidorButton.Location = new System.Drawing.Point(411, 313);
            this.cancelarDistribuidorButton.Name = "cancelarDistribuidorButton";
            this.cancelarDistribuidorButton.Rotation = 0D;
            this.cancelarDistribuidorButton.Size = new System.Drawing.Size(100, 36);
            this.cancelarDistribuidorButton.TabIndex = 27;
            this.cancelarDistribuidorButton.Text = "Cancelar";
            this.cancelarDistribuidorButton.UseVisualStyleBackColor = false;
            this.cancelarDistribuidorButton.Click += new System.EventHandler(this.cancelarDistribuidorButton_Click);
            // 
            // eliminarDistribuidorButton
            // 
            this.eliminarDistribuidorButton.BackColor = System.Drawing.Color.White;
            this.eliminarDistribuidorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eliminarDistribuidorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eliminarDistribuidorButton.ForeColor = System.Drawing.Color.White;
            this.eliminarDistribuidorButton.IconChar = FontAwesome.Sharp.IconChar.Trash;
            this.eliminarDistribuidorButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.eliminarDistribuidorButton.IconSize = 22;
            this.eliminarDistribuidorButton.Location = new System.Drawing.Point(411, 182);
            this.eliminarDistribuidorButton.Name = "eliminarDistribuidorButton";
            this.eliminarDistribuidorButton.Rotation = 0D;
            this.eliminarDistribuidorButton.Size = new System.Drawing.Size(36, 36);
            this.eliminarDistribuidorButton.TabIndex = 26;
            this.eliminarDistribuidorButton.UseVisualStyleBackColor = false;
            this.eliminarDistribuidorButton.Click += new System.EventHandler(this.eliminarDistribuidorButton_Click);
            // 
            // editarDistribuidorButton
            // 
            this.editarDistribuidorButton.BackColor = System.Drawing.Color.White;
            this.editarDistribuidorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editarDistribuidorButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.editarDistribuidorButton.ForeColor = System.Drawing.Color.White;
            this.editarDistribuidorButton.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.editarDistribuidorButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.editarDistribuidorButton.IconSize = 22;
            this.editarDistribuidorButton.Location = new System.Drawing.Point(411, 140);
            this.editarDistribuidorButton.Name = "editarDistribuidorButton";
            this.editarDistribuidorButton.Rotation = 0D;
            this.editarDistribuidorButton.Size = new System.Drawing.Size(36, 36);
            this.editarDistribuidorButton.TabIndex = 25;
            this.editarDistribuidorButton.UseVisualStyleBackColor = false;
            this.editarDistribuidorButton.Click += new System.EventHandler(this.editarDistribuidorButton_Click);
            // 
            // SearchDistribuidorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 393);
            this.ControlBox = false;
            this.Controls.Add(this.agregarDistribuidorButton);
            this.Controls.Add(this.cancelarDistribuidorButton);
            this.Controls.Add(this.eliminarDistribuidorButton);
            this.Controls.Add(this.editarDistribuidorButton);
            this.Controls.Add(this.searchDistribuidorListView);
            this.Name = "SearchDistribuidorForm";
            this.Text = "Distribuidor";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView searchDistribuidorListView;
        private FontAwesome.Sharp.IconButton agregarDistribuidorButton;
        private FontAwesome.Sharp.IconButton cancelarDistribuidorButton;
        private FontAwesome.Sharp.IconButton eliminarDistribuidorButton;
        private FontAwesome.Sharp.IconButton editarDistribuidorButton;
        private System.Windows.Forms.ColumnHeader hdrRutDistribuidor;
        private System.Windows.Forms.ColumnHeader hdrNombreDistribuidor;
        private System.Windows.Forms.ColumnHeader hdrFechaIngresoDistribuidor;
    }
}