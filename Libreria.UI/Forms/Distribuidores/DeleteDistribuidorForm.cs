﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Distribuidores
{
    public partial class DeleteDistribuidorForm : MaterialForm
    {
        private string rut;
        private string nombreDistribuidor;
        public bool flag = true;

        #region Constructor
        public DeleteDistribuidorForm(DistribuidorModel model)
        {
            InitializeComponent();


            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
            nombreDistribuidor = model.dis_nombre_empresa;
            rut = model.dis_rut;

            LoadDataForm(model.dis_nombre_empresa);
        }
        #endregion

        #region Methods
        public void LoadDataForm(string nomDistribuidor)
        {
            eliminarLabel.Text = eliminarLabel.Text + " " + nomDistribuidor + "?";
        }
        #endregion

        #region Events
        private void cancelarButton_Click(object sender, System.EventArgs e)
        {
            this.Close();
            flag = false;
        }

        private void eliminarButton_Click(object sender, System.EventArgs e)
        {
            DistribuidorModel model = new DistribuidorModel();
            model.dis_nombre_empresa = nombreDistribuidor;
            model.dis_rut = rut;

            try
            {
                GlobalConfig.Connection.DeleteDistribuidor(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("El distribuidor se ha eliminado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}
