﻿namespace Libreria.UI.Forms.Direccion
{
    partial class SearchDireccionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.eleccionButton = new FontAwesome.Sharp.IconButton();
            this.mostrarDireccionListView = new System.Windows.Forms.ListView();
            this.hdrCalle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hdrNoCalle = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hdrUrbanizacion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hdrNoUrbanizacion = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hdrComuna = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.hdrId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // cancelarButton
            // 
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(506, 328);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(100, 36);
            this.cancelarButton.TabIndex = 11;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // eleccionButton
            // 
            this.eleccionButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.eleccionButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eleccionButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eleccionButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eleccionButton.ForeColor = System.Drawing.Color.White;
            this.eleccionButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.eleccionButton.IconColor = System.Drawing.Color.Black;
            this.eleccionButton.IconSize = 16;
            this.eleccionButton.Location = new System.Drawing.Point(506, 269);
            this.eleccionButton.Name = "eleccionButton";
            this.eleccionButton.Rotation = 0D;
            this.eleccionButton.Size = new System.Drawing.Size(100, 36);
            this.eleccionButton.TabIndex = 12;
            this.eleccionButton.Text = "Elegir";
            this.eleccionButton.UseVisualStyleBackColor = false;
            this.eleccionButton.Click += new System.EventHandler(this.eleccionButton_Click);
            // 
            // mostrarDireccionListView
            // 
            this.mostrarDireccionListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.hdrId,
            this.hdrCalle,
            this.hdrNoCalle,
            this.hdrUrbanizacion,
            this.hdrNoUrbanizacion,
            this.hdrComuna});
            this.mostrarDireccionListView.HideSelection = false;
            this.mostrarDireccionListView.Location = new System.Drawing.Point(29, 113);
            this.mostrarDireccionListView.Name = "mostrarDireccionListView";
            this.mostrarDireccionListView.Size = new System.Drawing.Size(471, 251);
            this.mostrarDireccionListView.TabIndex = 13;
            this.mostrarDireccionListView.UseCompatibleStateImageBehavior = false;
            this.mostrarDireccionListView.View = System.Windows.Forms.View.Details;
            this.mostrarDireccionListView.SelectedIndexChanged += new System.EventHandler(this.mostrarDireccionListView_SelectedIndexChanged);
            this.mostrarDireccionListView.DoubleClick += new System.EventHandler(this.mostrarDireccionListView_DoubleClick);
            // 
            // hdrCalle
            // 
            this.hdrCalle.Text = "Calle";
            this.hdrCalle.Width = 170;
            // 
            // hdrNoCalle
            // 
            this.hdrNoCalle.Text = "No.";
            this.hdrNoCalle.Width = 50;
            // 
            // hdrUrbanizacion
            // 
            this.hdrUrbanizacion.Text = "Depto/Casa";
            this.hdrUrbanizacion.Width = 80;
            // 
            // hdrNoUrbanizacion
            // 
            this.hdrNoUrbanizacion.Text = "No. Depto/Casa";
            this.hdrNoUrbanizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.hdrNoUrbanizacion.Width = 90;
            // 
            // hdrComuna
            // 
            this.hdrComuna.Text = "Comuna";
            this.hdrComuna.Width = 77;
            // 
            // hdrId
            // 
            this.hdrId.Text = "ID";
            this.hdrId.Width = 0;
            // 
            // SearchDireccionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 393);
            this.ControlBox = false;
            this.Controls.Add(this.mostrarDireccionListView);
            this.Controls.Add(this.eleccionButton);
            this.Controls.Add(this.cancelarButton);
            this.Name = "SearchDireccionForm";
            this.Text = "Buscar Direccion";
            this.ResumeLayout(false);

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton eleccionButton;
        private System.Windows.Forms.ListView mostrarDireccionListView;
        private System.Windows.Forms.ColumnHeader hdrCalle;
        private System.Windows.Forms.ColumnHeader hdrNoCalle;
        private System.Windows.Forms.ColumnHeader hdrUrbanizacion;
        private System.Windows.Forms.ColumnHeader hdrNoUrbanizacion;
        private System.Windows.Forms.ColumnHeader hdrComuna;
        private System.Windows.Forms.ColumnHeader hdrId;
    }
}