﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;

namespace Libreria.UI.Forms.Direccion
{
    public partial class CreateDireccionForm : MaterialForm
    {
        private List<PaisModel> listPais;
        private List<RegionModel> listRegion;
        private List<ProvinciaModel> listProvincia;
        private List<ComunaModel> listComuna;
        public int idDir { get; set; }

        #region Constructor
        public CreateDireccionForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            LoadInitializeForm();
        }
        #endregion

        #region Methods
        public void LoadInitializeForm()
        {
            listPais = GlobalConfig.Connection.GetPaises_All();

            creaPaisDireccionComboBox.DataSource = listPais;
            creaPaisDireccionComboBox.DisplayMember = "dsc_pais";
            creaPaisDireccionComboBox.ValueMember = "cdg_pais";

            msgCreaDireccionLabel.Visible = false;
            numeroDeptoCasaLabel.Text = "";

            creaRegionDireccionComboBox.Enabled = false;
            creaProvinciaDireccionComboBox.Enabled = false;
            creaComunaDireccionComboBox.Enabled = false;
            deptoCasaComboBox.Enabled = false;
            creaNoUrbanizacionDireccionTextBox.Enabled = false;

        }
        #endregion

        #region Events
        private void creaPaisDireccionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (creaPaisDireccionComboBox.SelectedValue.ToString() != null && creaPaisDireccionComboBox.SelectedValue.ToString() == "CL")
            {
                listRegion = GlobalConfig.Connection.GetRegiones_All_Params(creaPaisDireccionComboBox.SelectedValue.ToString());

                creaRegionDireccionComboBox.DisplayMember = "dsc_region";
                creaRegionDireccionComboBox.ValueMember = "id_region";

                creaRegionDireccionComboBox.DataSource = listRegion;
                creaRegionDireccionComboBox.Enabled = true;
            }
            else
            {
                creaRegionDireccionComboBox.Enabled = false;
                creaProvinciaDireccionComboBox.Enabled = false;
                creaComunaDireccionComboBox.Enabled = false;
            }
        }

        private void creaRegionDireccionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (creaRegionDireccionComboBox.SelectedValue.ToString() != "00" && creaRegionDireccionComboBox.SelectedValue.ToString() != "")
            {
                listProvincia = GlobalConfig.Connection.GetProvincias_All_Params(creaRegionDireccionComboBox.SelectedValue.ToString());

                creaProvinciaDireccionComboBox.DisplayMember = "dsc_provincia";
                creaProvinciaDireccionComboBox.ValueMember = "id_provincia";
                creaProvinciaDireccionComboBox.DataSource = listProvincia;

                creaProvinciaDireccionComboBox.Enabled = true;
            }
        }

        private void creaProvinciaDireccionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (creaProvinciaDireccionComboBox.SelectedValue.ToString() != "000" && creaProvinciaDireccionComboBox.SelectedValue.ToString() != null)
            {
                listComuna = GlobalConfig.Connection.GetComunas_All_Params(creaProvinciaDireccionComboBox.SelectedValue.ToString());

                creaComunaDireccionComboBox.DisplayMember = "dsc_comuna";
                creaComunaDireccionComboBox.ValueMember = "id_comuna";
                creaComunaDireccionComboBox.DataSource = listComuna;

                creaComunaDireccionComboBox.Enabled = true;
            }
        }

        private void creaComunaDireccionComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            creaCalleDireccionTextBox.Enabled = true;
            creaCalleDireccionTextBox.Focus();
            creaNoCalleDireccionTextBox.Enabled = true;
        }

        private void agregarButton_Click(object sender, EventArgs e)
        {
            DireccionModel model = new DireccionModel();
            model.cdg_pais = creaPaisDireccionComboBox.SelectedValue.ToString();
            model.id_region = creaRegionDireccionComboBox.SelectedValue.ToString();
            model.id_provincia = creaProvinciaDireccionComboBox.SelectedValue.ToString();
            model.id_comuna = creaComunaDireccionComboBox.SelectedValue.ToString();
            model.calle = creaCalleDireccionTextBox.Text;
            model.no_calle = creaNoCalleDireccionTextBox.Text;
            model.urbanizacion = deptoCasaComboBox.SelectedItem.ToString();
            model.no_urbanizacion = creaNoUrbanizacionDireccionTextBox.Text;


            GlobalConfig.Connection.InsertDireccion(model);
            idDir = model.id_direccion;

            if (model.id_direccion != 0)
            {
                msgCreaDireccionLabel.Text = "Dirección Agregada Exitosamente";
                msgCreaDireccionLabel.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                msgCreaDireccionLabel.Text = "Error al agregar, Favor vuelva a intentarlo nuevamente ...";
                msgCreaDireccionLabel.ForeColor = System.Drawing.Color.Red;
            }

            this.Close();
        }

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void deptoCasaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (deptoCasaComboBox.SelectedIndex != 0)
            {
                numeroDeptoCasaLabel.Text = deptoCasaComboBox.SelectedItem.ToString() + ":";

                if (creaNoUrbanizacionDireccionTextBox.Enabled == false)
                {
                    creaNoUrbanizacionDireccionTextBox.Enabled = true;
                }
            }
        }

        private void creaNoCalleDireccionTextBox_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (creaNoUrbanizacionDireccionTextBox.Text != null)
            {
                deptoCasaComboBox.Enabled = true;
                deptoCasaComboBox.SelectedIndex = 0;
            }
        }
        #endregion
    }
}
