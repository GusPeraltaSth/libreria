﻿namespace Libreria.UI.Forms.Direccion
{
    partial class CreateDireccionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msgCreaDireccionLabel = new System.Windows.Forms.Label();
            this.creaPaisDireccionComboBox = new System.Windows.Forms.ComboBox();
            this.creaPaisDireccionLabel = new MaterialSkin.Controls.MaterialLabel();
            this.creaComunaDireccionLabel = new MaterialSkin.Controls.MaterialLabel();
            this.creaProvinciaDireccionLabel = new MaterialSkin.Controls.MaterialLabel();
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.agregarButton = new FontAwesome.Sharp.IconButton();
            this.creaRegionDireccionLabel = new MaterialSkin.Controls.MaterialLabel();
            this.creaRegionDireccionComboBox = new System.Windows.Forms.ComboBox();
            this.creaComunaDireccionComboBox = new System.Windows.Forms.ComboBox();
            this.creaProvinciaDireccionComboBox = new System.Windows.Forms.ComboBox();
            this.creaNoCalleDireccionLabel = new MaterialSkin.Controls.MaterialLabel();
            this.creaNoCalleDireccionTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.creaNoUrbanizacionDireccionTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.creaCalleDireccionTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.numeroDeptoCasaLabel = new MaterialSkin.Controls.MaterialLabel();
            this.deptoCasaComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // msgCreaDireccionLabel
            // 
            this.msgCreaDireccionLabel.AutoSize = true;
            this.msgCreaDireccionLabel.BackColor = System.Drawing.Color.White;
            this.msgCreaDireccionLabel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.msgCreaDireccionLabel.Location = new System.Drawing.Point(115, 547);
            this.msgCreaDireccionLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.msgCreaDireccionLabel.Name = "msgCreaDireccionLabel";
            this.msgCreaDireccionLabel.Size = new System.Drawing.Size(57, 21);
            this.msgCreaDireccionLabel.TabIndex = 20;
            this.msgCreaDireccionLabel.Text = "label1";
            // 
            // creaPaisDireccionComboBox
            // 
            this.creaPaisDireccionComboBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creaPaisDireccionComboBox.FormattingEnabled = true;
            this.creaPaisDireccionComboBox.Location = new System.Drawing.Point(267, 90);
            this.creaPaisDireccionComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.creaPaisDireccionComboBox.Name = "creaPaisDireccionComboBox";
            this.creaPaisDireccionComboBox.Size = new System.Drawing.Size(452, 28);
            this.creaPaisDireccionComboBox.TabIndex = 40;
            this.creaPaisDireccionComboBox.SelectedIndexChanged += new System.EventHandler(this.creaPaisDireccionComboBox_SelectedIndexChanged);
            // 
            // creaPaisDireccionLabel
            // 
            this.creaPaisDireccionLabel.AutoSize = true;
            this.creaPaisDireccionLabel.BackColor = System.Drawing.Color.White;
            this.creaPaisDireccionLabel.Depth = 0;
            this.creaPaisDireccionLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.creaPaisDireccionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.creaPaisDireccionLabel.Location = new System.Drawing.Point(115, 93);
            this.creaPaisDireccionLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.creaPaisDireccionLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.creaPaisDireccionLabel.Name = "creaPaisDireccionLabel";
            this.creaPaisDireccionLabel.Size = new System.Drawing.Size(42, 19);
            this.creaPaisDireccionLabel.TabIndex = 12;
            this.creaPaisDireccionLabel.Text = "País:";
            // 
            // creaComunaDireccionLabel
            // 
            this.creaComunaDireccionLabel.AutoSize = true;
            this.creaComunaDireccionLabel.BackColor = System.Drawing.Color.White;
            this.creaComunaDireccionLabel.Depth = 0;
            this.creaComunaDireccionLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.creaComunaDireccionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.creaComunaDireccionLabel.Location = new System.Drawing.Point(115, 320);
            this.creaComunaDireccionLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.creaComunaDireccionLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.creaComunaDireccionLabel.Name = "creaComunaDireccionLabel";
            this.creaComunaDireccionLabel.Size = new System.Drawing.Size(69, 19);
            this.creaComunaDireccionLabel.TabIndex = 15;
            this.creaComunaDireccionLabel.Text = "Comuna:";
            // 
            // creaProvinciaDireccionLabel
            // 
            this.creaProvinciaDireccionLabel.AutoSize = true;
            this.creaProvinciaDireccionLabel.BackColor = System.Drawing.Color.White;
            this.creaProvinciaDireccionLabel.Depth = 0;
            this.creaProvinciaDireccionLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.creaProvinciaDireccionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.creaProvinciaDireccionLabel.Location = new System.Drawing.Point(115, 245);
            this.creaProvinciaDireccionLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.creaProvinciaDireccionLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.creaProvinciaDireccionLabel.Name = "creaProvinciaDireccionLabel";
            this.creaProvinciaDireccionLabel.Size = new System.Drawing.Size(75, 19);
            this.creaProvinciaDireccionLabel.TabIndex = 14;
            this.creaProvinciaDireccionLabel.Text = "Provincia:";
            // 
            // cancelarButton
            // 
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(458, 603);
            this.cancelarButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(261, 55);
            this.cancelarButton.TabIndex = 11;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // agregarButton
            // 
            this.agregarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.agregarButton.ForeColor = System.Drawing.Color.White;
            this.agregarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.agregarButton.IconColor = System.Drawing.Color.Black;
            this.agregarButton.IconSize = 16;
            this.agregarButton.Location = new System.Drawing.Point(119, 603);
            this.agregarButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.agregarButton.Name = "agregarButton";
            this.agregarButton.Rotation = 0D;
            this.agregarButton.Size = new System.Drawing.Size(261, 55);
            this.agregarButton.TabIndex = 10;
            this.agregarButton.Text = "Guardar";
            this.agregarButton.UseVisualStyleBackColor = false;
            this.agregarButton.Click += new System.EventHandler(this.agregarButton_Click);
            // 
            // creaRegionDireccionLabel
            // 
            this.creaRegionDireccionLabel.AutoSize = true;
            this.creaRegionDireccionLabel.BackColor = System.Drawing.Color.White;
            this.creaRegionDireccionLabel.Depth = 0;
            this.creaRegionDireccionLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.creaRegionDireccionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.creaRegionDireccionLabel.Location = new System.Drawing.Point(115, 168);
            this.creaRegionDireccionLabel.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.creaRegionDireccionLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.creaRegionDireccionLabel.Name = "creaRegionDireccionLabel";
            this.creaRegionDireccionLabel.Size = new System.Drawing.Size(59, 19);
            this.creaRegionDireccionLabel.TabIndex = 13;
            this.creaRegionDireccionLabel.Text = "Región:";
            // 
            // creaRegionDireccionComboBox
            // 
            this.creaRegionDireccionComboBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creaRegionDireccionComboBox.FormattingEnabled = true;
            this.creaRegionDireccionComboBox.Location = new System.Drawing.Point(267, 165);
            this.creaRegionDireccionComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.creaRegionDireccionComboBox.Name = "creaRegionDireccionComboBox";
            this.creaRegionDireccionComboBox.Size = new System.Drawing.Size(452, 28);
            this.creaRegionDireccionComboBox.TabIndex = 3;
            this.creaRegionDireccionComboBox.SelectedIndexChanged += new System.EventHandler(this.creaRegionDireccionComboBox_SelectedIndexChanged);
            // 
            // creaComunaDireccionComboBox
            // 
            this.creaComunaDireccionComboBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creaComunaDireccionComboBox.FormattingEnabled = true;
            this.creaComunaDireccionComboBox.Location = new System.Drawing.Point(267, 317);
            this.creaComunaDireccionComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.creaComunaDireccionComboBox.Name = "creaComunaDireccionComboBox";
            this.creaComunaDireccionComboBox.Size = new System.Drawing.Size(452, 28);
            this.creaComunaDireccionComboBox.TabIndex = 5;
            this.creaComunaDireccionComboBox.SelectedIndexChanged += new System.EventHandler(this.creaComunaDireccionComboBox_SelectedIndexChanged);
            // 
            // creaProvinciaDireccionComboBox
            // 
            this.creaProvinciaDireccionComboBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creaProvinciaDireccionComboBox.FormattingEnabled = true;
            this.creaProvinciaDireccionComboBox.Location = new System.Drawing.Point(267, 242);
            this.creaProvinciaDireccionComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.creaProvinciaDireccionComboBox.Name = "creaProvinciaDireccionComboBox";
            this.creaProvinciaDireccionComboBox.Size = new System.Drawing.Size(452, 28);
            this.creaProvinciaDireccionComboBox.TabIndex = 4;
            this.creaProvinciaDireccionComboBox.SelectedIndexChanged += new System.EventHandler(this.creaProvinciaDireccionComboBox_SelectedIndexChanged);
            // 
            // creaNoCalleDireccionLabel
            // 
            this.creaNoCalleDireccionLabel.AutoSize = true;
            this.creaNoCalleDireccionLabel.BackColor = System.Drawing.Color.White;
            this.creaNoCalleDireccionLabel.Depth = 0;
            this.creaNoCalleDireccionLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.creaNoCalleDireccionLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.creaNoCalleDireccionLabel.Location = new System.Drawing.Point(115, 430);
            this.creaNoCalleDireccionLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.creaNoCalleDireccionLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.creaNoCalleDireccionLabel.Name = "creaNoCalleDireccionLabel";
            this.creaNoCalleDireccionLabel.Size = new System.Drawing.Size(67, 19);
            this.creaNoCalleDireccionLabel.TabIndex = 17;
            this.creaNoCalleDireccionLabel.Text = "Número:";
            // 
            // creaNoCalleDireccionTextBox
            // 
            this.creaNoCalleDireccionTextBox.BackColor = System.Drawing.Color.White;
            this.creaNoCalleDireccionTextBox.Depth = 0;
            this.creaNoCalleDireccionTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creaNoCalleDireccionTextBox.Hint = "";
            this.creaNoCalleDireccionTextBox.Location = new System.Drawing.Point(267, 427);
            this.creaNoCalleDireccionTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.creaNoCalleDireccionTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.creaNoCalleDireccionTextBox.Name = "creaNoCalleDireccionTextBox";
            this.creaNoCalleDireccionTextBox.PasswordChar = '\0';
            this.creaNoCalleDireccionTextBox.SelectedText = "";
            this.creaNoCalleDireccionTextBox.SelectionLength = 0;
            this.creaNoCalleDireccionTextBox.SelectionStart = 0;
            this.creaNoCalleDireccionTextBox.Size = new System.Drawing.Size(452, 23);
            this.creaNoCalleDireccionTextBox.TabIndex = 7;
            this.creaNoCalleDireccionTextBox.UseSystemPasswordChar = false;
            this.creaNoCalleDireccionTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.creaNoCalleDireccionTextBox_KeyDown);
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.Color.White;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(115, 489);
            this.materialLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(93, 19);
            this.materialLabel3.TabIndex = 18;
            this.materialLabel3.Text = "Depto/Casa:";
            // 
            // creaNoUrbanizacionDireccionTextBox
            // 
            this.creaNoUrbanizacionDireccionTextBox.BackColor = System.Drawing.Color.White;
            this.creaNoUrbanizacionDireccionTextBox.Depth = 0;
            this.creaNoUrbanizacionDireccionTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creaNoUrbanizacionDireccionTextBox.Hint = "";
            this.creaNoUrbanizacionDireccionTextBox.Location = new System.Drawing.Point(585, 485);
            this.creaNoUrbanizacionDireccionTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.creaNoUrbanizacionDireccionTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.creaNoUrbanizacionDireccionTextBox.Name = "creaNoUrbanizacionDireccionTextBox";
            this.creaNoUrbanizacionDireccionTextBox.PasswordChar = '\0';
            this.creaNoUrbanizacionDireccionTextBox.SelectedText = "";
            this.creaNoUrbanizacionDireccionTextBox.SelectionLength = 0;
            this.creaNoUrbanizacionDireccionTextBox.SelectionStart = 0;
            this.creaNoUrbanizacionDireccionTextBox.Size = new System.Drawing.Size(134, 23);
            this.creaNoUrbanizacionDireccionTextBox.TabIndex = 9;
            this.creaNoUrbanizacionDireccionTextBox.UseSystemPasswordChar = false;
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.Color.White;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(115, 378);
            this.materialLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(47, 19);
            this.materialLabel4.TabIndex = 16;
            this.materialLabel4.Text = "Calle:";
            // 
            // creaCalleDireccionTextBox
            // 
            this.creaCalleDireccionTextBox.BackColor = System.Drawing.Color.White;
            this.creaCalleDireccionTextBox.Depth = 0;
            this.creaCalleDireccionTextBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creaCalleDireccionTextBox.Hint = "";
            this.creaCalleDireccionTextBox.Location = new System.Drawing.Point(267, 374);
            this.creaCalleDireccionTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.creaCalleDireccionTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.creaCalleDireccionTextBox.Name = "creaCalleDireccionTextBox";
            this.creaCalleDireccionTextBox.PasswordChar = '\0';
            this.creaCalleDireccionTextBox.SelectedText = "";
            this.creaCalleDireccionTextBox.SelectionLength = 0;
            this.creaCalleDireccionTextBox.SelectionStart = 0;
            this.creaCalleDireccionTextBox.Size = new System.Drawing.Size(452, 23);
            this.creaCalleDireccionTextBox.TabIndex = 6;
            this.creaCalleDireccionTextBox.UseSystemPasswordChar = false;
            // 
            // numeroDeptoCasaLabel
            // 
            this.numeroDeptoCasaLabel.AutoSize = true;
            this.numeroDeptoCasaLabel.BackColor = System.Drawing.Color.White;
            this.numeroDeptoCasaLabel.Depth = 0;
            this.numeroDeptoCasaLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.numeroDeptoCasaLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.numeroDeptoCasaLabel.Location = new System.Drawing.Point(493, 489);
            this.numeroDeptoCasaLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.numeroDeptoCasaLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.numeroDeptoCasaLabel.Name = "numeroDeptoCasaLabel";
            this.numeroDeptoCasaLabel.Size = new System.Drawing.Size(45, 19);
            this.numeroDeptoCasaLabel.TabIndex = 19;
            this.numeroDeptoCasaLabel.Text = "Label";
            // 
            // deptoCasaComboBox
            // 
            this.deptoCasaComboBox.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deptoCasaComboBox.FormattingEnabled = true;
            this.deptoCasaComboBox.ItemHeight = 20;
            this.deptoCasaComboBox.Items.AddRange(new object[] {
            "--SELECCIONE--",
            "DEPTO",
            "CASA"});
            this.deptoCasaComboBox.Location = new System.Drawing.Point(267, 480);
            this.deptoCasaComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deptoCasaComboBox.Name = "deptoCasaComboBox";
            this.deptoCasaComboBox.Size = new System.Drawing.Size(134, 28);
            this.deptoCasaComboBox.TabIndex = 8;
            this.deptoCasaComboBox.SelectedIndexChanged += new System.EventHandler(this.deptoCasaComboBox_SelectedIndexChanged);
            // 
            // CreateDireccionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 671);
            this.ControlBox = false;
            this.Controls.Add(this.deptoCasaComboBox);
            this.Controls.Add(this.numeroDeptoCasaLabel);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.creaCalleDireccionTextBox);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.creaNoUrbanizacionDireccionTextBox);
            this.Controls.Add(this.creaNoCalleDireccionLabel);
            this.Controls.Add(this.creaNoCalleDireccionTextBox);
            this.Controls.Add(this.creaProvinciaDireccionComboBox);
            this.Controls.Add(this.creaComunaDireccionComboBox);
            this.Controls.Add(this.creaRegionDireccionComboBox);
            this.Controls.Add(this.msgCreaDireccionLabel);
            this.Controls.Add(this.creaPaisDireccionComboBox);
            this.Controls.Add(this.creaPaisDireccionLabel);
            this.Controls.Add(this.creaComunaDireccionLabel);
            this.Controls.Add(this.creaProvinciaDireccionLabel);
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.agregarButton);
            this.Controls.Add(this.creaRegionDireccionLabel);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "CreateDireccionForm";
            this.Text = "Crear Dirección";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label msgCreaDireccionLabel;
        private System.Windows.Forms.ComboBox creaPaisDireccionComboBox;
        private MaterialSkin.Controls.MaterialLabel creaPaisDireccionLabel;
        private MaterialSkin.Controls.MaterialLabel creaComunaDireccionLabel;
        private MaterialSkin.Controls.MaterialLabel creaProvinciaDireccionLabel;
        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton agregarButton;
        private MaterialSkin.Controls.MaterialLabel creaRegionDireccionLabel;
        private System.Windows.Forms.ComboBox creaRegionDireccionComboBox;
        private System.Windows.Forms.ComboBox creaComunaDireccionComboBox;
        private System.Windows.Forms.ComboBox creaProvinciaDireccionComboBox;
        private MaterialSkin.Controls.MaterialLabel creaNoCalleDireccionLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField creaNoCalleDireccionTextBox;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialSingleLineTextField creaNoUrbanizacionDireccionTextBox;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialSingleLineTextField creaCalleDireccionTextBox;
        private MaterialSkin.Controls.MaterialLabel numeroDeptoCasaLabel;
        private System.Windows.Forms.ComboBox deptoCasaComboBox;
    }
}