﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Direccion
{
    public partial class SearchDireccionForm : MaterialForm
    {
        public int idDir;

        #region Constructor
        public SearchDireccionForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
            eleccionButton.Enabled = false;

            LoadDataForm();
        }
        #endregion

        #region Methods
        public void LoadDataForm()
        {
            mostrarDireccionListView.FullRowSelect = true;

            List<DireccionModel> listDireccion = GlobalConfig.Connection.GetDireccion_All();

            foreach (DireccionModel d in listDireccion)
            {

                ListViewItem item = new ListViewItem(d.id_direccion.ToString());
                item.SubItems.Add(d.calle);
                item.SubItems.Add(d.no_calle);
                item.SubItems.Add(d.urbanizacion);
                item.SubItems.Add(d.no_urbanizacion);
                item.SubItems.Add(d.dsc_comuna);

                mostrarDireccionListView.Items.Add(item);
            }
        }
        #endregion

        #region Events

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void mostrarDireccionListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (eleccionButton.Enabled == false)
            {
                eleccionButton.Enabled = true;
            }
        }

        private void eleccionButton_Click(object sender, EventArgs e)
        {
            ListViewItem item;
            item = mostrarDireccionListView.SelectedItems[0];
            int.TryParse(item.Text, out idDir);
            this.Close();
        }

        private void mostrarDireccionListView_DoubleClick(object sender, EventArgs e)
        {
            ListViewItem item;
            item = mostrarDireccionListView.SelectedItems[0];
            int.TryParse(item.Text, out idDir);
            this.Close();
        }
        #endregion
    }
}
