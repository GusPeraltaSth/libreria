﻿namespace Libreria.UI.Forms.Editorial
{
    partial class EditEditorialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.guardarButton = new FontAwesome.Sharp.IconButton();
            this.editarEditorialLabel = new MaterialSkin.Controls.MaterialLabel();
            this.editarEditorialTextBox = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.SuspendLayout();
            // 
            // cancelarButton
            // 
            this.cancelarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(380, 191);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarButton.TabIndex = 16;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // guardarButton
            // 
            this.guardarButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.guardarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.guardarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.guardarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.guardarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guardarButton.ForeColor = System.Drawing.Color.White;
            this.guardarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.guardarButton.IconColor = System.Drawing.Color.Black;
            this.guardarButton.IconSize = 16;
            this.guardarButton.Location = new System.Drawing.Point(127, 191);
            this.guardarButton.Name = "guardarButton";
            this.guardarButton.Rotation = 0D;
            this.guardarButton.Size = new System.Drawing.Size(174, 36);
            this.guardarButton.TabIndex = 15;
            this.guardarButton.Text = "Guardar";
            this.guardarButton.UseVisualStyleBackColor = false;
            this.guardarButton.Click += new System.EventHandler(this.guardarButton_Click);
            // 
            // editarEditorialLabel
            // 
            this.editarEditorialLabel.AutoSize = true;
            this.editarEditorialLabel.BackColor = System.Drawing.Color.White;
            this.editarEditorialLabel.Depth = 0;
            this.editarEditorialLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.editarEditorialLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.editarEditorialLabel.Location = new System.Drawing.Point(123, 107);
            this.editarEditorialLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.editarEditorialLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarEditorialLabel.Name = "editarEditorialLabel";
            this.editarEditorialLabel.Size = new System.Drawing.Size(69, 19);
            this.editarEditorialLabel.TabIndex = 14;
            this.editarEditorialLabel.Text = "Editorial:";
            // 
            // editarEditorialTextBox
            // 
            this.editarEditorialTextBox.BackColor = System.Drawing.Color.White;
            this.editarEditorialTextBox.Depth = 0;
            this.editarEditorialTextBox.Hint = "";
            this.editarEditorialTextBox.Location = new System.Drawing.Point(251, 107);
            this.editarEditorialTextBox.Margin = new System.Windows.Forms.Padding(5);
            this.editarEditorialTextBox.MouseState = MaterialSkin.MouseState.HOVER;
            this.editarEditorialTextBox.Name = "editarEditorialTextBox";
            this.editarEditorialTextBox.PasswordChar = '\0';
            this.editarEditorialTextBox.SelectedText = "";
            this.editarEditorialTextBox.SelectionLength = 0;
            this.editarEditorialTextBox.SelectionStart = 0;
            this.editarEditorialTextBox.Size = new System.Drawing.Size(303, 23);
            this.editarEditorialTextBox.TabIndex = 13;
            this.editarEditorialTextBox.UseSystemPasswordChar = false;
            // 
            // EditEditorialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 286);
            this.ControlBox = false;
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.guardarButton);
            this.Controls.Add(this.editarEditorialLabel);
            this.Controls.Add(this.editarEditorialTextBox);
            this.Name = "EditEditorialForm";
            this.Text = "Editar Editorial";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton guardarButton;
        private MaterialSkin.Controls.MaterialLabel editarEditorialLabel;
        private MaterialSkin.Controls.MaterialSingleLineTextField editarEditorialTextBox;
    }
}