﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Editorial
{
    public partial class SearchEditorialForm : MaterialForm
    {
        private List<EditorialModel> editoriales;

        #region Constructor
        public SearchEditorialForm()
        {
            InitializeComponent();

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            LoadDataForm();
        }
        #endregion

        #region Methods
        public void LoadDataForm()
        {
            editoriales = GlobalConfig.Connection.GetEditorial_All();

            buscarEditorialListBox.DataSource = null;

            buscarEditorialListBox.DisplayMember = "edi_nombre";
            buscarEditorialListBox.ValueMember = "id_editorial";
            buscarEditorialListBox.DataSource = editoriales;

            editarButton.Enabled = false;
            eliminarButton.Enabled = false;
            agregarButton.Enabled = true;
        }


        #endregion

        #region Events
        private void agregarButton_Click(object sender, EventArgs e)
        {
            CreateEditorialForm frmEditorialCreate = new CreateEditorialForm();
            frmEditorialCreate.StartPosition = FormStartPosition.CenterParent;
            frmEditorialCreate.Tag = this;
            frmEditorialCreate.ShowDialog(this);

            LoadDataForm();
        }

        private void editarButton_Click(object sender, EventArgs e)
        {
            EditorialModel editModel = new EditorialModel();

            editModel.id_editorial = int.Parse(buscarEditorialListBox.SelectedValue.ToString());
            editModel.edi_nombre = buscarEditorialListBox.GetItemText(buscarEditorialListBox.SelectedItem);

            EditEditorialForm frmEditCategoria = new EditEditorialForm(editModel);
            frmEditCategoria.StartPosition = FormStartPosition.CenterParent;
            frmEditCategoria.Tag = this;
            frmEditCategoria.ShowDialog(this);

            LoadDataForm();
        }

        private void eliminarButton_Click(object sender, EventArgs e)
        {
            EditorialModel model = new EditorialModel();
            model.id_editorial = int.Parse(buscarEditorialListBox.SelectedValue.ToString());
            model.edi_nombre = buscarEditorialListBox.GetItemText(buscarEditorialListBox.SelectedItem);

            DeleteEditorialForm frmDeleteEditorial = new DeleteEditorialForm(model);
            frmDeleteEditorial.StartPosition = FormStartPosition.CenterParent;
            frmDeleteEditorial.Tag = this;
            frmDeleteEditorial.ShowDialog(this);

            LoadDataForm();
        }

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buscarEditorialListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (editarButton.Enabled == false)
            {
                editarButton.Enabled = true;
            }
            if (eliminarButton.Enabled == false)
            {
                eliminarButton.Enabled = true;
            }
        }

        #endregion
    }
}
