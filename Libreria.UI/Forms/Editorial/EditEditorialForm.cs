﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Editorial
{
    public partial class EditEditorialForm : MaterialForm
    {
        private int id;

        #region Constructor
        public EditEditorialForm(EditorialModel model)
        {
            InitializeComponent();
            editarEditorialTextBox.Text = model.edi_nombre;
            id = model.id_editorial;

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            editarEditorialTextBox.Enabled = true;
        }
        #endregion

        #region Events
        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
            for (int i = 0; i < Application.OpenForms.Count; i++)
            {
                if (Application.OpenForms[i].Name == "SearchEditorialForm")
                {
                    Application.OpenForms[i].Show();
                }
            }
        }
        private void guardarButton_Click(object sender, EventArgs e)
        {
            EditorialModel model = new EditorialModel();
            model.edi_nombre = editarEditorialTextBox.Text;
            model.id_editorial = id;

            try
            {
                GlobalConfig.Connection.EditEditorial(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("La editorial se ha guardado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }
        #endregion
    }
}
