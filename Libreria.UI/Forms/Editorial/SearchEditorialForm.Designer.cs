﻿namespace Libreria.UI.Forms.Editorial
{
    partial class SearchEditorialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.agregarButton = new FontAwesome.Sharp.IconButton();
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.eliminarButton = new FontAwesome.Sharp.IconButton();
            this.editarButton = new FontAwesome.Sharp.IconButton();
            this.buscarEditorialListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // agregarButton
            // 
            this.agregarButton.BackColor = System.Drawing.Color.White;
            this.agregarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.agregarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.agregarButton.ForeColor = System.Drawing.Color.White;
            this.agregarButton.IconChar = FontAwesome.Sharp.IconChar.Plus;
            this.agregarButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.agregarButton.IconSize = 22;
            this.agregarButton.Location = new System.Drawing.Point(537, 111);
            this.agregarButton.Name = "agregarButton";
            this.agregarButton.Rotation = 0D;
            this.agregarButton.Size = new System.Drawing.Size(36, 36);
            this.agregarButton.TabIndex = 20;
            this.agregarButton.UseVisualStyleBackColor = false;
            this.agregarButton.Click += new System.EventHandler(this.agregarButton_Click);
            // 
            // cancelarButton
            // 
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(537, 334);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(100, 36);
            this.cancelarButton.TabIndex = 19;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // eliminarButton
            // 
            this.eliminarButton.BackColor = System.Drawing.Color.White;
            this.eliminarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eliminarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eliminarButton.ForeColor = System.Drawing.Color.White;
            this.eliminarButton.IconChar = FontAwesome.Sharp.IconChar.Trash;
            this.eliminarButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.eliminarButton.IconSize = 22;
            this.eliminarButton.Location = new System.Drawing.Point(537, 195);
            this.eliminarButton.Name = "eliminarButton";
            this.eliminarButton.Rotation = 0D;
            this.eliminarButton.Size = new System.Drawing.Size(36, 36);
            this.eliminarButton.TabIndex = 18;
            this.eliminarButton.UseVisualStyleBackColor = false;
            this.eliminarButton.Click += new System.EventHandler(this.eliminarButton_Click);
            // 
            // editarButton
            // 
            this.editarButton.BackColor = System.Drawing.Color.White;
            this.editarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.editarButton.ForeColor = System.Drawing.Color.White;
            this.editarButton.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.editarButton.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(173)))), ((int)(((byte)(78)))));
            this.editarButton.IconSize = 22;
            this.editarButton.Location = new System.Drawing.Point(537, 153);
            this.editarButton.Name = "editarButton";
            this.editarButton.Rotation = 0D;
            this.editarButton.Size = new System.Drawing.Size(36, 36);
            this.editarButton.TabIndex = 17;
            this.editarButton.UseVisualStyleBackColor = false;
            this.editarButton.Click += new System.EventHandler(this.editarButton_Click);
            // 
            // buscarEditorialListBox
            // 
            this.buscarEditorialListBox.DisplayMember = "dsc_idioma";
            this.buscarEditorialListBox.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buscarEditorialListBox.FormattingEnabled = true;
            this.buscarEditorialListBox.ItemHeight = 17;
            this.buscarEditorialListBox.Location = new System.Drawing.Point(248, 111);
            this.buscarEditorialListBox.Name = "buscarEditorialListBox";
            this.buscarEditorialListBox.Size = new System.Drawing.Size(283, 259);
            this.buscarEditorialListBox.TabIndex = 16;
            this.buscarEditorialListBox.SelectedIndexChanged += new System.EventHandler(this.buscarEditorialListBox_SelectedIndexChanged);
            // 
            // SearchEditorialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 450);
            this.ControlBox = false;
            this.Controls.Add(this.agregarButton);
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.eliminarButton);
            this.Controls.Add(this.editarButton);
            this.Controls.Add(this.buscarEditorialListBox);
            this.Name = "SearchEditorialForm";
            this.Text = "Editorial";
            this.ResumeLayout(false);

        }

        #endregion

        private FontAwesome.Sharp.IconButton agregarButton;
        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton eliminarButton;
        private FontAwesome.Sharp.IconButton editarButton;
        private System.Windows.Forms.ListBox buscarEditorialListBox;
    }
}