﻿namespace Libreria.UI.Forms.Editorial
{
    partial class DeleteEditorialForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cancelarButton = new FontAwesome.Sharp.IconButton();
            this.eliminarButton = new FontAwesome.Sharp.IconButton();
            this.eliminarEditorialLabel = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // cancelarButton
            // 
            this.cancelarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(83)))), ((int)(((byte)(79)))));
            this.cancelarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.cancelarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelarButton.ForeColor = System.Drawing.Color.White;
            this.cancelarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.cancelarButton.IconColor = System.Drawing.Color.Black;
            this.cancelarButton.IconSize = 16;
            this.cancelarButton.Location = new System.Drawing.Point(297, 213);
            this.cancelarButton.Name = "cancelarButton";
            this.cancelarButton.Rotation = 0D;
            this.cancelarButton.Size = new System.Drawing.Size(174, 36);
            this.cancelarButton.TabIndex = 18;
            this.cancelarButton.Text = "Cancelar";
            this.cancelarButton.UseVisualStyleBackColor = false;
            this.cancelarButton.Click += new System.EventHandler(this.cancelarButton_Click);
            // 
            // eliminarButton
            // 
            this.eliminarButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(184)))), ((int)(((byte)(92)))));
            this.eliminarButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eliminarButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.eliminarButton.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eliminarButton.ForeColor = System.Drawing.Color.White;
            this.eliminarButton.IconChar = FontAwesome.Sharp.IconChar.None;
            this.eliminarButton.IconColor = System.Drawing.Color.Black;
            this.eliminarButton.IconSize = 16;
            this.eliminarButton.Location = new System.Drawing.Point(94, 213);
            this.eliminarButton.Name = "eliminarButton";
            this.eliminarButton.Rotation = 0D;
            this.eliminarButton.Size = new System.Drawing.Size(174, 36);
            this.eliminarButton.TabIndex = 17;
            this.eliminarButton.Text = "Eliminar";
            this.eliminarButton.UseVisualStyleBackColor = false;
            this.eliminarButton.Click += new System.EventHandler(this.eliminarButton_Click);
            // 
            // eliminarEditorialLabel
            // 
            this.eliminarEditorialLabel.AutoSize = true;
            this.eliminarEditorialLabel.BackColor = System.Drawing.Color.White;
            this.eliminarEditorialLabel.Depth = 0;
            this.eliminarEditorialLabel.Font = new System.Drawing.Font("Roboto", 11F);
            this.eliminarEditorialLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.eliminarEditorialLabel.Location = new System.Drawing.Point(91, 114);
            this.eliminarEditorialLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.eliminarEditorialLabel.MouseState = MaterialSkin.MouseState.HOVER;
            this.eliminarEditorialLabel.Name = "eliminarEditorialLabel";
            this.eliminarEditorialLabel.Size = new System.Drawing.Size(229, 19);
            this.eliminarEditorialLabel.TabIndex = 16;
            this.eliminarEditorialLabel.Text = "¿Está seguro que quiere eliminar";
            // 
            // DeleteEditorialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 332);
            this.ControlBox = false;
            this.Controls.Add(this.cancelarButton);
            this.Controls.Add(this.eliminarButton);
            this.Controls.Add(this.eliminarEditorialLabel);
            this.Name = "DeleteEditorialForm";
            this.Text = "Eliminar Editorial";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private FontAwesome.Sharp.IconButton cancelarButton;
        private FontAwesome.Sharp.IconButton eliminarButton;
        private MaterialSkin.Controls.MaterialLabel eliminarEditorialLabel;
    }
}