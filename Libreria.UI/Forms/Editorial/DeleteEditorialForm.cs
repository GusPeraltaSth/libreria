﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Windows.Forms;

namespace Libreria.UI.Forms.Editorial
{
    public partial class DeleteEditorialForm : MaterialForm
    {
        private int id;
        private string nomEditorial;

        #region Constructor
        public DeleteEditorialForm(EditorialModel editorialModel)
        {
            InitializeComponent();

            id = editorialModel.id_editorial;
            nomEditorial = editorialModel.edi_nombre;

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);

            LoadDataForm(editorialModel.edi_nombre);
        }
        #endregion

        #region Methods
        public void LoadDataForm(string nomEditorial)
        {
            eliminarEditorialLabel.Text = eliminarEditorialLabel.Text + " " + nomEditorial + "?";
        }
        #endregion

        #region Events
        private void eliminarButton_Click(object sender, EventArgs e)
        {
            EditorialModel model = new EditorialModel();
            model.edi_nombre = nomEditorial;
            model.id_editorial = id;

            try
            {
                GlobalConfig.Connection.DeleteEditorial(model);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }

            MessageBox.Show("La editorial se ha eliminado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            this.Close();
        }

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
