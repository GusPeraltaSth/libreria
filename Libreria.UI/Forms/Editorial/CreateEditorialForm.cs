﻿using Libreria.DLL;
using Libreria.DLL.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;

namespace Libreria.UI.Forms.Editorial
{
    public partial class CreateEditorialForm : MaterialForm
    {
        #region Constructor
        public CreateEditorialForm()
        {
            InitializeComponent();
            msgCreaLabel.Text = "";

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue900, Primary.Blue900, Primary.Blue900, Accent.LightBlue200, TextShade.WHITE);
        }
        #endregion

        #region Events
        private void agregarButton_Click(object sender, EventArgs e)
        {
            EditorialModel model = new EditorialModel();
            model.edi_nombre = agregaTextBox.Text;

            GlobalConfig.Connection.InsertEditorial(model);
            if (model.id_editorial != 0)
            {
                msgCreaLabel.Text = "Editorial Agregada Exitosamente";
                msgCreaLabel.ForeColor = System.Drawing.Color.Green;

                agregaTextBox.Text = "";
                agregaTextBox.Focus();
            }
            else
            {
                msgCreaLabel.Text = "Error al Crear, Favor vuelva a intentarlo nuevamente ...";
                msgCreaLabel.ForeColor = System.Drawing.Color.Red;

                agregaTextBox.Text = "";
                this.ActiveControl = agregaTextBox;
            }
        }

        private void cancelarButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
