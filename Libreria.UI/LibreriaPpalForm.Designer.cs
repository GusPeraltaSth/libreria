﻿namespace Libreria.UI
{
    partial class LibreriaPpalForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.panelMantenedoresSubMenu = new System.Windows.Forms.Panel();
            this.metodoPagosButton = new System.Windows.Forms.Button();
            this.idiomasButton = new System.Windows.Forms.Button();
            this.estadoButton = new System.Windows.Forms.Button();
            this.editorialButton = new System.Windows.Forms.Button();
            this.distribuidoresButton = new System.Windows.Forms.Button();
            this.categoriaButton = new System.Windows.Forms.Button();
            this.autorButton = new System.Windows.Forms.Button();
            this.salirButton = new FontAwesome.Sharp.IconButton();
            this.mantenedoresButton = new FontAwesome.Sharp.IconButton();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.containerFormPanel = new System.Windows.Forms.Panel();
            this.comprasButton = new System.Windows.Forms.Button();
            this.facturasButton = new System.Windows.Forms.Button();
            this.librosButton = new System.Windows.Forms.Button();
            this.panelSideMenu.SuspendLayout();
            this.panelMantenedoresSubMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(30)))), ((int)(((byte)(68)))));
            this.panelSideMenu.Controls.Add(this.panelMantenedoresSubMenu);
            this.panelSideMenu.Controls.Add(this.salirButton);
            this.panelSideMenu.Controls.Add(this.mantenedoresButton);
            this.panelSideMenu.Controls.Add(this.panelLogo);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(220, 1025);
            this.panelSideMenu.TabIndex = 0;
            // 
            // panelMantenedoresSubMenu
            // 
            this.panelMantenedoresSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(30)))), ((int)(((byte)(120)))));
            this.panelMantenedoresSubMenu.Controls.Add(this.librosButton);
            this.panelMantenedoresSubMenu.Controls.Add(this.facturasButton);
            this.panelMantenedoresSubMenu.Controls.Add(this.comprasButton);
            this.panelMantenedoresSubMenu.Controls.Add(this.metodoPagosButton);
            this.panelMantenedoresSubMenu.Controls.Add(this.idiomasButton);
            this.panelMantenedoresSubMenu.Controls.Add(this.estadoButton);
            this.panelMantenedoresSubMenu.Controls.Add(this.editorialButton);
            this.panelMantenedoresSubMenu.Controls.Add(this.distribuidoresButton);
            this.panelMantenedoresSubMenu.Controls.Add(this.categoriaButton);
            this.panelMantenedoresSubMenu.Controls.Add(this.autorButton);
            this.panelMantenedoresSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelMantenedoresSubMenu.Location = new System.Drawing.Point(0, 185);
            this.panelMantenedoresSubMenu.Name = "panelMantenedoresSubMenu";
            this.panelMantenedoresSubMenu.Size = new System.Drawing.Size(220, 400);
            this.panelMantenedoresSubMenu.TabIndex = 6;
            // 
            // metodoPagosButton
            // 
            this.metodoPagosButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.metodoPagosButton.FlatAppearance.BorderSize = 0;
            this.metodoPagosButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.metodoPagosButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.metodoPagosButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.metodoPagosButton.Location = new System.Drawing.Point(0, 240);
            this.metodoPagosButton.Name = "metodoPagosButton";
            this.metodoPagosButton.Size = new System.Drawing.Size(220, 40);
            this.metodoPagosButton.TabIndex = 7;
            this.metodoPagosButton.Text = "Método de Pago";
            this.metodoPagosButton.UseVisualStyleBackColor = true;
            this.metodoPagosButton.Click += new System.EventHandler(this.metodoPagosButton_Click);
            // 
            // idiomasButton
            // 
            this.idiomasButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.idiomasButton.FlatAppearance.BorderSize = 0;
            this.idiomasButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.idiomasButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idiomasButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.idiomasButton.Location = new System.Drawing.Point(0, 200);
            this.idiomasButton.Name = "idiomasButton";
            this.idiomasButton.Size = new System.Drawing.Size(220, 40);
            this.idiomasButton.TabIndex = 6;
            this.idiomasButton.Text = "Idioma";
            this.idiomasButton.UseVisualStyleBackColor = true;
            this.idiomasButton.Click += new System.EventHandler(this.idiomasButton_Click_1);
            // 
            // estadoButton
            // 
            this.estadoButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.estadoButton.FlatAppearance.BorderSize = 0;
            this.estadoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.estadoButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.estadoButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.estadoButton.Location = new System.Drawing.Point(0, 160);
            this.estadoButton.Name = "estadoButton";
            this.estadoButton.Size = new System.Drawing.Size(220, 40);
            this.estadoButton.TabIndex = 5;
            this.estadoButton.Text = "Estado";
            this.estadoButton.UseVisualStyleBackColor = true;
            this.estadoButton.Click += new System.EventHandler(this.estadoButton_Click);
            // 
            // editorialButton
            // 
            this.editorialButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.editorialButton.FlatAppearance.BorderSize = 0;
            this.editorialButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editorialButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editorialButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.editorialButton.Location = new System.Drawing.Point(0, 120);
            this.editorialButton.Name = "editorialButton";
            this.editorialButton.Size = new System.Drawing.Size(220, 40);
            this.editorialButton.TabIndex = 4;
            this.editorialButton.Text = "Editorial";
            this.editorialButton.UseVisualStyleBackColor = true;
            this.editorialButton.Click += new System.EventHandler(this.editorialButton_Click);
            // 
            // distribuidoresButton
            // 
            this.distribuidoresButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.distribuidoresButton.FlatAppearance.BorderSize = 0;
            this.distribuidoresButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.distribuidoresButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.distribuidoresButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.distribuidoresButton.Location = new System.Drawing.Point(0, 80);
            this.distribuidoresButton.Name = "distribuidoresButton";
            this.distribuidoresButton.Size = new System.Drawing.Size(220, 40);
            this.distribuidoresButton.TabIndex = 3;
            this.distribuidoresButton.Text = "Distribuidor";
            this.distribuidoresButton.UseVisualStyleBackColor = true;
            this.distribuidoresButton.Click += new System.EventHandler(this.distribuidoresButton_Click);
            // 
            // categoriaButton
            // 
            this.categoriaButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.categoriaButton.FlatAppearance.BorderSize = 0;
            this.categoriaButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.categoriaButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.categoriaButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.categoriaButton.Location = new System.Drawing.Point(0, 40);
            this.categoriaButton.Name = "categoriaButton";
            this.categoriaButton.Size = new System.Drawing.Size(220, 40);
            this.categoriaButton.TabIndex = 1;
            this.categoriaButton.Text = "Categoría";
            this.categoriaButton.UseVisualStyleBackColor = true;
            this.categoriaButton.Click += new System.EventHandler(this.categoriaButton_Click);
            // 
            // autorButton
            // 
            this.autorButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.autorButton.FlatAppearance.BorderSize = 0;
            this.autorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.autorButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.autorButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.autorButton.Location = new System.Drawing.Point(0, 0);
            this.autorButton.Name = "autorButton";
            this.autorButton.Size = new System.Drawing.Size(220, 40);
            this.autorButton.TabIndex = 0;
            this.autorButton.Text = "Autor";
            this.autorButton.UseVisualStyleBackColor = true;
            this.autorButton.Click += new System.EventHandler(this.autorButton_Click);
            // 
            // salirButton
            // 
            this.salirButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(117)))), ((int)(((byte)(216)))));
            this.salirButton.FlatAppearance.BorderSize = 0;
            this.salirButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.salirButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.salirButton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salirButton.ForeColor = System.Drawing.Color.White;
            this.salirButton.IconChar = FontAwesome.Sharp.IconChar.SignOutAlt;
            this.salirButton.IconColor = System.Drawing.Color.White;
            this.salirButton.IconSize = 22;
            this.salirButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.salirButton.Location = new System.Drawing.Point(0, 981);
            this.salirButton.Name = "salirButton";
            this.salirButton.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.salirButton.Rotation = 0D;
            this.salirButton.Size = new System.Drawing.Size(220, 44);
            this.salirButton.TabIndex = 5;
            this.salirButton.Text = "Salir";
            this.salirButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.salirButton.UseVisualStyleBackColor = false;
            this.salirButton.Click += new System.EventHandler(this.salirButton_Click);
            // 
            // mantenedoresButton
            // 
            this.mantenedoresButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.mantenedoresButton.FlatAppearance.BorderSize = 0;
            this.mantenedoresButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mantenedoresButton.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.mantenedoresButton.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mantenedoresButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.mantenedoresButton.IconChar = FontAwesome.Sharp.IconChar.GripVertical;
            this.mantenedoresButton.IconColor = System.Drawing.Color.Gainsboro;
            this.mantenedoresButton.IconSize = 22;
            this.mantenedoresButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.mantenedoresButton.Location = new System.Drawing.Point(0, 140);
            this.mantenedoresButton.Name = "mantenedoresButton";
            this.mantenedoresButton.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.mantenedoresButton.Rotation = 0D;
            this.mantenedoresButton.Size = new System.Drawing.Size(220, 45);
            this.mantenedoresButton.TabIndex = 2;
            this.mantenedoresButton.Text = "Mantenedores";
            this.mantenedoresButton.UseVisualStyleBackColor = true;
            this.mantenedoresButton.Click += new System.EventHandler(this.mantenedoresButton_Click);
            // 
            // panelLogo
            // 
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(220, 140);
            this.panelLogo.TabIndex = 0;
            // 
            // panelTitle
            // 
            this.panelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(25)))), ((int)(((byte)(62)))));
            this.panelTitle.Location = new System.Drawing.Point(220, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(1700, 72);
            this.panelTitle.TabIndex = 1;
            // 
            // containerFormPanel
            // 
            this.containerFormPanel.Location = new System.Drawing.Point(220, 72);
            this.containerFormPanel.Name = "containerFormPanel";
            this.containerFormPanel.Size = new System.Drawing.Size(1700, 953);
            this.containerFormPanel.TabIndex = 10;
            // 
            // comprasButton
            // 
            this.comprasButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.comprasButton.FlatAppearance.BorderSize = 0;
            this.comprasButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comprasButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comprasButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.comprasButton.Location = new System.Drawing.Point(0, 280);
            this.comprasButton.Name = "comprasButton";
            this.comprasButton.Size = new System.Drawing.Size(220, 40);
            this.comprasButton.TabIndex = 8;
            this.comprasButton.Text = "Compras";
            this.comprasButton.UseVisualStyleBackColor = true;
            this.comprasButton.Click += new System.EventHandler(this.comprasButton_Click);
            // 
            // facturasButton
            // 
            this.facturasButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.facturasButton.FlatAppearance.BorderSize = 0;
            this.facturasButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.facturasButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.facturasButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.facturasButton.Location = new System.Drawing.Point(0, 320);
            this.facturasButton.Name = "facturasButton";
            this.facturasButton.Size = new System.Drawing.Size(220, 40);
            this.facturasButton.TabIndex = 9;
            this.facturasButton.Text = "Facturas";
            this.facturasButton.UseVisualStyleBackColor = true;
            this.facturasButton.Click += new System.EventHandler(this.facturasButton_Click);
            // 
            // librosButton
            // 
            this.librosButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.librosButton.FlatAppearance.BorderSize = 0;
            this.librosButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.librosButton.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.librosButton.ForeColor = System.Drawing.Color.Gainsboro;
            this.librosButton.Location = new System.Drawing.Point(0, 360);
            this.librosButton.Name = "librosButton";
            this.librosButton.Size = new System.Drawing.Size(220, 40);
            this.librosButton.TabIndex = 10;
            this.librosButton.Text = "Libros";
            this.librosButton.UseVisualStyleBackColor = true;
            this.librosButton.Click += new System.EventHandler(this.librosButton_Click);
            // 
            // LibreriaPpalForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1920, 1025);
            this.Controls.Add(this.containerFormPanel);
            this.Controls.Add(this.panelTitle);
            this.Controls.Add(this.panelSideMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LibreriaPpalForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panelSideMenu.ResumeLayout(false);
            this.panelMantenedoresSubMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Panel panelLogo;
        private FontAwesome.Sharp.IconButton mantenedoresButton;
        private FontAwesome.Sharp.IconButton salirButton;
        private System.Windows.Forms.Panel panelTitle;        
        private System.Windows.Forms.Panel containerFormPanel;
        private System.Windows.Forms.Panel panelMantenedoresSubMenu;
        private System.Windows.Forms.Button metodoPagosButton;
        private System.Windows.Forms.Button idiomasButton;
        private System.Windows.Forms.Button estadoButton;
        private System.Windows.Forms.Button editorialButton;
        private System.Windows.Forms.Button distribuidoresButton;
        private System.Windows.Forms.Button categoriaButton;
        private System.Windows.Forms.Button autorButton;
        private System.Windows.Forms.Button librosButton;
        private System.Windows.Forms.Button facturasButton;
        private System.Windows.Forms.Button comprasButton;
    }
}

