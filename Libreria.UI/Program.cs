﻿using Libreria.DLL;
using System;
using System.Windows.Forms;

namespace Libreria.UI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            GlobalConfig.InitializeConnections(DatabaseType.Sql);

            Application.Run(new LibreriaPpalForm());
        }
    }
}
