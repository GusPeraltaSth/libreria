﻿using Libreria.DLL.Models;
using System.Collections.Generic;

namespace Libreria.DLL.Interfaces
{
    public interface IDataConnection
    {
        #region List Get_All
        /// <summary>
        /// Interfaz que lista todos los idiomas de la Base de datos
        /// </summary>
        /// <returns></returns>
        List<IdiomaModel> GetIdioma_All();

        /// <summary>
        /// Interfaz que lista todas las categorias de la Base de datos
        /// </summary>
        /// <returns></returns>
        List<CategoriaModel> GetCategoria_All();

        /// <summary>
        /// Interfaz que lista todas las editorial de la Base de datos
        /// </summary>
        /// <returns></returns>
        List<EditorialModel> GetEditorial_All();

        List<EstadoModel> GetEstado_All();
        List<MetodoPagoModel> GetMetodoPago_All();

        /// <summary>
        /// Interfaz que lista todos los autores de la Base de datos
        /// </summary>
        /// <returns></returns>
        List<AutorModel> GetAutores_All();
        /// <summary>
        /// Interfaz que lista todos los distribuidores de la Base de datos
        /// </summary>
        /// <returns></returns>
        List<DistribuidorModel> GetDistribuidor_All();
        List<DireccionModel> GetDireccion_All();

        /// <summary>
        /// Interfaz que lista todos los paises de la Base de datos
        /// </summary>
        /// <returns></returns>
        List<PaisModel> GetPaises_All();
        List<RegionModel> GetRegiones_All();
        List<ProvinciaModel> GetProvincias_All();
        List<ComunaModel> GetComunas_All();
        #endregion

        #region Insert
        /// <summary>
        /// Interfaz que inserta el idioma a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        IdiomaModel InsertIdioma(IdiomaModel model);
        EditorialModel InsertEditorial(EditorialModel model);
        EstadoModel InsertEstado(EstadoModel model);
        CategoriaModel InsertCategoria(CategoriaModel model);
        MetodoPagoModel InsertMetodoPago(MetodoPagoModel model);
        AutorModel InsertAutor(AutorModel model);
        DireccionModel InsertDireccion(DireccionModel model);
        DistribuidorModel InsertDistribuidor(DistribuidorModel model, DireccionModel modelDir);
        #endregion

        #region Delete
        /// <summary>
        /// Interfaz que elimina el idioma a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        void DeleteIdioma(IdiomaModel model);
        void DeleteCategoria(CategoriaModel model);
        void DeleteEditorial(EditorialModel model);
        void DeleteEstado(EstadoModel model);
        void DeleteMetodoPago(MetodoPagoModel model);
        void DeleteAutor(AutorModel model);

        void DeleteDistribuidor(DistribuidorModel model);
        #endregion

        #region Update
        /// <summary>
        /// Interfaz que actualiza el idioma a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<IdiomaModel> EditIdioma(IdiomaModel model);

        /// <summary>
        /// Interfaz que actualiza el Categoría a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<CategoriaModel> EditCategoria(CategoriaModel model);

        /// <summary>
        /// Interfaz que actualiza el Editorial a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<EditorialModel> EditEditorial(EditorialModel model);

        /// <summary>
        /// Interfaz que actualiza el Metodo de Pago a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<MetodoPagoModel> EditMetodoPago(MetodoPagoModel model);

        /// <summary>
        /// Interfaz que actualiza el Estado a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<EstadoModel> EditEstado(EstadoModel model);

        /// <summary>
        /// Interfaz que actualiza el autor a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<AutorModel> EditAutor(AutorModel model);

        /// <summary>
        /// Interfaz que actualiza el Distribuidor a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<DistribuidorModel> EditDistribuidor(DistribuidorModel model);
        #endregion

        #region Search
        /// <summary>
        /// Interfaz que busca un idioma en la Base de datos
        /// </summary>
        /// <param name="buscaIdioma"></param>
        /// <returns></returns>
        List<IdiomaModel> SearchIdioma(string buscaIdioma);

        /// <summary>
        /// Interfaz que busca la Categoria a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<CategoriaModel> SearchCategoria(string buscaCategoria);

        /// <summary>
        /// Interfaz que busca la Editorial a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<EditorialModel> SearchEditorial(string buscaEditorial);

        /// <summary>
        /// Interfaz que busca el Estado a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<EstadoModel> SearchEstado(string buscaEstado);

        /// <summary>
        /// Interfaz que busca el Metodo de Pago a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<MetodoPagoModel> SearchMetodoPago(string buscaMetodoPago);

        /// <summary>
        /// Interfaz que busca el Autor a la Base de datos
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<AutorModel> SearchAutor(int idAutor, string nombre, string aPaterno, string aMaterno, string cdgPais);

        /// <summary>
        /// Interfaz que retorna todas las comunas con un filtro
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<ComunaModel> GetComunas_All_Params(string idProvincia);

        /// <summary>
        /// Interfaz que retorna todas las provincia con un filtro
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<ProvinciaModel> GetProvincias_All_Params(string idRegion);

        /// <summary>
        /// Interfaz que retorna todas las regiones con un filtro
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<RegionModel> GetRegiones_All_Params(string cdgPais);

        /// <summary>
        /// Interfaz que retorna todas las direcciones con un filtro
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        List<DireccionModel> GetDirecciones_All_Params(int idDireccion);
        #endregion
    }
}
