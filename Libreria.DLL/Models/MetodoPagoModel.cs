﻿namespace Libreria.DLL.Models
{
    public class MetodoPagoModel
    {
        public int id_metodoPago { get; set; }
        public string dsc_metodoPago { get; set; }
    }
}
