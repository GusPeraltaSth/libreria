﻿namespace Libreria.DLL.Models
{
    public class AutorModel
    {
        public int id_autores { get; set; }
        public string aut_apellido_paterno { get; set; }
        public string aut_apellido_materno { get; set; }
        public string aut_nombre { get; set; }
        public string cdg_pais { get; set; }
        public string fullName
        {
            get
            {
                return aut_nombre + " " + aut_apellido_paterno + " " + aut_apellido_materno;
            }
        }
    }
}
