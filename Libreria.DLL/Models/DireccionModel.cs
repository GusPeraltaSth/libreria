﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Libreria.DLL.Models
{
    public class DireccionModel
    {
        #region Own Attributes
        [Key]
        public int id_direccion { get; set; }

        [MaxLength(100)]
        public string urbanizacion { get; set; }

        [MaxLength(10)]
        public string no_urbanizacion { get; set; }

        [MaxLength(100)]
        public string calle { get; set; }

        [MaxLength(10)]
        public string no_calle { get; set; }
        #endregion

        #region FK Attributes

        [MaxLength(2)]
        [DefaultValue("CL")]
        public string cdg_pais { get; set; }
        public string dsc_pais { get; set; }

        [MaxLength(3)]
        [DefaultValue("--SELECCIONE--")]
        public string id_region { get; set; }
        public string dsc_region { get; set; }

        [MaxLength(4)]
        [DefaultValue("--SELECCIONE--")]
        public string id_provincia { get; set; }
        public string dsc_provincia { get; set; }

        [MaxLength(5)]
        [DefaultValue("--SELECCIONE--")]
        public string id_comuna { get; set; }
        public string dsc_comuna { get; set; }

        #endregion

        #region FK Navigation Property
        public virtual PaisModel Pais { get; set; }
        public virtual RegionModel Region { get; set; }
        public virtual ProvinciaModel Provincia { get; set; }
        public virtual ComunaModel Comuna { get; set; }
        #endregion

    }
}
