﻿namespace Libreria.DLL.Models
{
    public class CategoriaModel
    {
        public int id_categoria { get; set; }
        public string dsc_categoria { get; set; }
    }
}
