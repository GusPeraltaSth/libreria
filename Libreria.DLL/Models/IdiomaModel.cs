﻿namespace Libreria.DLL.Models
{
    /// <summary>
    /// Propiedades del Model, deben ser iguales a los campos de la tabla.
    /// </summary>
    public class IdiomaModel
    {
        public int id_idioma { get; set; }
        public string dsc_idioma { get; set; }
    }
}
