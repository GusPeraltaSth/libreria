﻿using System.ComponentModel.DataAnnotations;

namespace Libreria.DLL.Models
{
    public class ProvinciaModel
    {
        [MaxLength(2)]
        public string cdg_pais { get; set; }

        [MaxLength(3)]
        public string id_region { get; set; }

        [MaxLength(4)]
        public string id_provincia { get; set; }

        [MaxLength(50)]
        public string dsc_provincia { get; set; }
    }
}
