﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Libreria.DLL.Models
{
    public class DistribuidorModel
    {
        //TODO - Generar propiedades privadas de DistribuidorModel

        [MaxLength(10)]
        public string dis_rut { get; set; }
        [MaxLength(50)]
        public string dis_nombre_empresa { get; set; }
        [DataType(DataType.Date)]
        public DateTime dis_fecha_ingreso { get; set; }
        public List<DistribuidorDireccionModel> dis_direccion { get; set; }
    }
}
