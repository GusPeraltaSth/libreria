﻿using System.ComponentModel.DataAnnotations;

namespace Libreria.DLL.Models
{
    public class DistribuidorDireccionModel
    {
        [MaxLength(10)]
        public string dis_rut { get; set; }
        public int id_direccion { get; set; }
    }
}
