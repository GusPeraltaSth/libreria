﻿using System.ComponentModel.DataAnnotations;

namespace Libreria.DLL.Models
{
    public class RegionModel
    {
        [MaxLength(2)]
        public string cdg_pais { get; set; }

        [MaxLength(3)]
        public string id_region { get; set; }

        [MaxLength(50)]
        public string dsc_region { get; set; }
    }
}
