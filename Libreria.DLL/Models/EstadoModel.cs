﻿namespace Libreria.DLL.Models
{
    public class EstadoModel
    {
        //Generar propiedades privadas de EstadoModel
        public int id_estado { get; set; }
        public string dsc_estado { get; set; }
    }
}
