﻿using System.ComponentModel.DataAnnotations;

namespace Libreria.DLL.Models
{
    public class PaisModel
    {
        //TODO - Generar Atributos de PaisModel
        [Key]
        public string cdg_pais { get; set; }
        public string dsc_pais { get; set; }
    }
}
