﻿using Dapper;
using Libreria.DLL.Interfaces;
using Libreria.DLL.Models;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;

namespace Libreria.DLL.DataAccess
{
    public class SqlConnector : IDataConnection
    {
        private const string db = "Libreria";

        #region Methods Insert-Create
        /// <summary>
        /// Insert a la tabla Idioma, el parametro es el modelo IdiomaModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna el modelo, para hacer uso del id de la inserción</returns>
        public IdiomaModel InsertIdioma(IdiomaModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@dscIdioma", model.dsc_idioma);
                p.Add("@idOut", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                cx.Execute("dbo.spIdiomas_Insert", p, commandType: CommandType.StoredProcedure);

                model.id_idioma = p.Get<int>("@idOut");

                return model;
            }
        }
        /// <summary>
        /// Insert a la tabla Cateogoría, el parametro es el modelo CategoriaModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna el modelo, para hacer uso del id de la inserción</returns>
        public CategoriaModel InsertCategoria(CategoriaModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@dscCategoria", model.dsc_categoria);
                p.Add("@idOut", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                cx.Execute("dbo.spCategorias_Insert", p, commandType: CommandType.StoredProcedure);

                model.id_categoria = p.Get<int>("@idOut");

                return model;
            }
        }
        /// <summary>
        /// Insert a la tabla Estado, el parametro es el modelo CategoriaModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna el modelo, para hacer uso del id de la inserción</returns>
        public EstadoModel InsertEstado(EstadoModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@dscEstado", model.dsc_estado);
                p.Add("@idOut", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                cx.Execute("dbo.spEstados_Insert", p, commandType: CommandType.StoredProcedure);

                model.id_estado = p.Get<int>("@idOut");

                return model;
            }
        }

        /// <summary>
        /// Insert a la tabla Autores, el parametro es el modelo AutorModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna el modelo, para hacer uso del id de la inserción</returns>
        public AutorModel InsertAutor(AutorModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@nombre", model.aut_nombre);
                p.Add("@aPaterno", model.aut_apellido_paterno);
                p.Add("@aMaterno", model.aut_apellido_materno);
                p.Add("@cdgPais", model.cdg_pais);
                p.Add("@idOut", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                cx.Execute("dbo.spAutores_Insert", p, commandType: CommandType.StoredProcedure);

                model.id_autores = p.Get<int>("@idOut");

                return model;
            }
        }
        /// <summary>
        /// Insert a la tabla Editorial, el parametro es el modelo EditorialModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna el modelo, para hacer uso del id de la inserción</returns>
        public EditorialModel InsertEditorial(EditorialModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@nomEditorial", model.edi_nombre);
                p.Add("@idOut", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                cx.Execute("dbo.spEditorial_Insert", p, commandType: CommandType.StoredProcedure);

                model.id_editorial = p.Get<int>("@idOut");

                return model;
            }
        }
        /// <summary>
        /// Insert a la tabla Metodo de Pago, el parametro es el modelo MetodoPagoModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna el modelo, para hacer uso del id de la inserción</returns>
        public MetodoPagoModel InsertMetodoPago(MetodoPagoModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@dscMetodoPago", model.dsc_metodoPago);
                p.Add("@idOut", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                cx.Execute("dbo.spMetodosDePago_Insert", p, commandType: CommandType.StoredProcedure);

                model.id_metodoPago = p.Get<int>("@idOut");

                return model;
            }
        }
        /// <summary>
        /// Insert a la tabla Dirección, el parametro es el modelo DireccionModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna el modelo, para hacer uso del id de la inserción</returns>
        public DireccionModel InsertDireccion(DireccionModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();

                p.Add("@cdgPais", model.cdg_pais);
                p.Add("@idRegion", model.id_region);
                p.Add("@idProvincia", model.id_provincia);
                p.Add("@idComuna", model.id_comuna);
                p.Add("@calle", model.calle);
                p.Add("@noCalle", model.no_calle);
                p.Add("@urbanización", model.urbanizacion);
                p.Add("@noUrbanizacion", model.no_urbanizacion);
                p.Add("@idOut", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);

                cx.Execute("dbo.spDireccion_Insert", p, commandType: CommandType.StoredProcedure);

                model.id_direccion = p.Get<int>("@idOut");
            }

            return model;
        }
        /// <summary>
        /// Insert a la tabla Distribuidor, el parametro es el modelo DistribuidorModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Retorna el modelo, para hacer uso del id de la inserción</returns>
        public DistribuidorModel InsertDistribuidor(DistribuidorModel model, DireccionModel modelDir)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();

                p.Add("@idDireccion", modelDir.id_direccion);
                p.Add("@rutDistribuidor", model.dis_rut);
                p.Add("@nombreDistribuidor", model.dis_nombre_empresa);
                p.Add("@fechaIngresoDistribuidor", model.dis_fecha_ingreso);
                p.Add("@idOut", "", dbType: DbType.String, direction: ParameterDirection.Output);

                cx.Execute("dbo.spDistribuidores_Insert", p, commandType: CommandType.StoredProcedure);

                model.dis_rut = p.Get<string>("@idOut");
            }

            return model;
        }
        #endregion

        #region Methods Delete
        /// <summary>
        /// Elimina un registro de la tabla Idioma, parametro es el modelo IdiomaModel
        /// </summary>
        /// <param name="model"></param>
        public void DeleteIdioma(IdiomaModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@id", model.id_idioma);

                cx.Execute("dbo.spIdiomas_Delete", p, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla Categoria, parametro es el modelo CategoriaModel
        /// </summary>
        /// <param name="model"></param>
        public void DeleteCategoria(CategoriaModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@id", model.id_categoria);

                cx.Execute("dbo.spCategorias_Delete", p, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla Autor, parametro es el modelo AutorModel
        /// </summary>
        /// <param name="model"></param>
        public void DeleteAutor(AutorModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@id", model.id_autores);

                cx.Execute("dbo.spAutores_Delete", p, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla Editorial, parametro es el modelo EditorialModel
        /// </summary>
        /// <param name="model"></param>
        public void DeleteEditorial(EditorialModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@id", model.id_editorial);

                cx.Execute("dbo.spEditorial_Delete", p, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla Estado, parametro es el modelo EstadoModel
        /// </summary>
        /// <param name="model"></param>
        public void DeleteEstado(EstadoModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@id", model.id_estado);

                cx.Execute("dbo.spEstados_Delete", p, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla Metodo de Pago, parametro es el modelo MetodoPagoModel
        /// </summary>
        /// <param name="model"></param>
        public void DeleteMetodoPago(MetodoPagoModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@id", model.id_metodoPago);

                cx.Execute("dbo.spMetodosDePago_Delete", p, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Elimina un registro de la tabla Distribuidor, parametro es el modelo DistribuidorModel
        /// </summary>
        /// <param name="model"></param>
        public void DeleteDistribuidor(DistribuidorModel model)
        {
            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                var p = new DynamicParameters();
                p.Add("@rutDistribuidor", model.dis_rut);

                cx.Execute("dbo.spDistribuidores_Delete", p, commandType: CommandType.StoredProcedure);
            }
        }

        #endregion

        #region Methods Update
        /// <summary>
        /// Actualiza un registro de la tabla Idioma, parametro es el modelo IdiomaModel
        /// </summary>
        /// <param name="model"></param>
        public List<IdiomaModel> EditIdioma(IdiomaModel model)
        {
            List<IdiomaModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<IdiomaModel>("dbo.spIdiomas_Edit", new { varString = model.dsc_idioma, id = model.id_idioma }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Actualiza un registro de la tabla Categoría, parametro es el modelo CategoriaModel
        /// </summary>
        /// <param name="model"></param>
        public List<CategoriaModel> EditCategoria(CategoriaModel model)
        {
            List<CategoriaModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<CategoriaModel>("dbo.spCategorias_Edit", new { dscCategoria = model.dsc_categoria, id = model.id_categoria }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Actualiza un registro de la tabla Editorial, parametro es el modelo EditorialModel
        /// </summary>
        /// <param name="model"></param>
        public List<EditorialModel> EditEditorial(EditorialModel model)
        {
            List<EditorialModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<EditorialModel>("dbo.spEditorial_Edit", new { nomEditorial = model.edi_nombre, id = model.id_editorial }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Actualiza un registro de la tabla Estado, parametro es el modelo EstadoModel
        /// </summary>
        /// <param name="model"></param>
        public List<EstadoModel> EditEstado(EstadoModel model)
        {
            List<EstadoModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<EstadoModel>("dbo.spEstados_Edit", new { dscEstado = model.dsc_estado, id = model.id_estado }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Actualiza un registro de la tabla Metodo de Pago, parametro es el modelo MetodoModel
        /// </summary>
        /// <param name="model"></param>
        public List<MetodoPagoModel> EditMetodoPago(MetodoPagoModel model)
        {
            List<MetodoPagoModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<MetodoPagoModel>("dbo.spMetodosDePago_Edit", new { dscMetodoDePago = model.dsc_metodoPago, id = model.id_metodoPago }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Actualiza un registro de la tabla Autor, parametro es el modelo AutorModel
        /// </summary>
        /// <param name="model"></param>
        public List<AutorModel> EditAutor(AutorModel model)
        {
            List<AutorModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<AutorModel>("dbo.spAutores_Edit", new { nombre = model.aut_nombre, aPaterno = model.aut_apellido_paterno, aMaterno = model.aut_apellido_materno, cdgPais = model.cdg_pais, id = model.id_autores }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Actualiza un registro de la tabla Distribuidor, parametro es el modelo DistribuidorModel
        /// </summary>
        /// <param name="model"></param>
        public List<DistribuidorModel> EditDistribuidor(DistribuidorModel model)
        {
            List<DistribuidorModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<DistribuidorModel>("dbo.spDistribuidores_Edit", new { rutDis = model.dis_rut, nombreDis = model.dis_nombre_empresa, fechaIngreso = model.dis_fecha_ingreso }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        #endregion

        #region Methods Get-ListAll

        /// <summary>
        /// Metodo que carga todos los idiomas
        /// </summary>
        /// <returns></returns>
        public List<IdiomaModel> GetIdioma_All()
        {
            List<IdiomaModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<IdiomaModel>("dbo.spIdiomas_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todas loas categorías
        /// </summary>
        /// <returns></returns>
        public List<CategoriaModel> GetCategoria_All()
        {
            List<CategoriaModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<CategoriaModel>("dbo.spCategorias_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todas las editoriales
        /// </summary>
        /// <returns></returns>
        public List<EditorialModel> GetEditorial_All()
        {
            List<EditorialModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<EditorialModel>("dbo.spEditorial_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todo los metodos de pago
        /// </summary>
        /// <returns></returns>
        public List<MetodoPagoModel> GetMetodoPago_All()
        {
            List<MetodoPagoModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<MetodoPagoModel>("dbo.spMetodosDePago_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todo los estados
        /// </summary>
        /// <returns></returns>
        public List<EstadoModel> GetEstado_All()
        {
            List<EstadoModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<EstadoModel>("dbo.spEstados_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todo los autores
        /// </summary>
        /// <returns></returns>
        public List<AutorModel> GetAutores_All()
        {
            List<AutorModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<AutorModel>("dbo.spAutores_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todos los paises
        /// </summary>
        /// <returns></returns>
        public List<PaisModel> GetPaises_All()
        {
            List<PaisModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<PaisModel>("dbo.spPaises_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todas las regiones
        /// </summary>
        /// <returns></returns>
        public List<RegionModel> GetRegiones_All()
        {
            List<RegionModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<RegionModel>("dbo.spRegion_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todas Provincial
        /// </summary>
        /// <returns></returns>
        public List<ProvinciaModel> GetProvincias_All()
        {
            List<ProvinciaModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<ProvinciaModel>("dbo.spProvincia_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todas las comunas
        /// </summary>
        /// <returns></returns>
        public List<ComunaModel> GetComunas_All()
        {
            List<ComunaModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<ComunaModel>("dbo.spComuna_Get_All").ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo que carga todos los distribuidores
        /// </summary>
        /// <returns></returns>
        public List<DistribuidorModel> GetDistribuidor_All()
        {
            List<DistribuidorModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<DistribuidorModel>("dbo.spDistribuidores_Get_All").ToList();
            }

            return output;
        }

        public List<DireccionModel> GetDireccion_All()
        {
            List<DireccionModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<DireccionModel>("dbo.spDireccion_Get_All").ToList();
            }

            return output;
        }
        #endregion

        #region Metodos Search
        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="buscaIdioma"></param>
        /// <returns></returns>
        public List<IdiomaModel> SearchIdioma(string buscaIdioma)
        {
            List<IdiomaModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<IdiomaModel>("dbo.spIdiomas_Search", new { varString = buscaIdioma }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="buscaCategoria"></param>
        /// <returns></returns>
        public List<CategoriaModel> SearchCategoria(string buscaCategoria)
        {
            List<CategoriaModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<CategoriaModel>("dbo.spCategorias_Search", new { dscChar = buscaCategoria }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="buscaEditorial"></param>
        /// <returns></returns>
        public List<EditorialModel> SearchEditorial(string buscaEditorial)
        {
            List<EditorialModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<EditorialModel>("dbo.spEditorial_Search", new { dscChar = buscaEditorial }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="buscaEstado"></param>
        /// <returns></returns>
        public List<EstadoModel> SearchEstado(string buscaEstado)
        {
            List<EstadoModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<EstadoModel>("dbo.spEstados_Search", new { dscChar = buscaEstado }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="buscaMetodo"></param>
        /// <returns></returns>
        public List<MetodoPagoModel> SearchMetodoPago(string buscaMetodoPago)
        {
            List<MetodoPagoModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<MetodoPagoModel>("dbo.spMetodosDePago_Search", new { dscChar = buscaMetodoPago }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="idAutor"></param>
        /// <param name="nombre"></param>
        /// <param name="aPaterno"></param>
        /// <param name="aMaterno"></param>
        /// <param name="cdgPais"></param>
        /// <returns></returns>
        public List<AutorModel> SearchAutor([Optional] int idAutor, [Optional] string nombre, [Optional] string aPaterno, [Optional] string aMaterno, [Optional] string cdgPais)
        {
            List<AutorModel> output;

            if (cdgPais == "--")
            {
                cdgPais = null;
            }

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<AutorModel>("dbo.spAutores_Search", new { idAutor = idAutor, nombre = nombre, aPaterno = aPaterno, aMaterno = aMaterno, cdgPais = cdgPais }, commandType: CommandType.StoredProcedure).ToList();
            }
            return output;
        }

        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="cdgPais"></param>
        /// <returns></returns>
        public List<RegionModel> GetRegiones_All_Params(string cdgPais)
        {
            List<RegionModel> output = null;

            if (cdgPais != "--")
            {

                using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
                {
                    output = cx.Query<RegionModel>("dbo.spRegion_Get_All_Params", new { cdgPais = cdgPais }, commandType: CommandType.StoredProcedure).ToList();
                }
            }

            return output;
        }

        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="idRegion"></param>
        /// <returns></returns>
        public List<ProvinciaModel> GetProvincias_All_Params(string idRegion)
        {
            List<ProvinciaModel> output = null;

            if (idRegion != "00")
            {
                using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
                {
                    output = cx.Query<ProvinciaModel>("dbo.spProvincia_Get_All_Params", new { idRegion = idRegion }, commandType: CommandType.StoredProcedure).ToList();
                }
            }

            return output;
        }

        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="idProvincia"></param>
        /// <returns></returns>
        public List<ComunaModel> GetComunas_All_Params(string idProvincia)
        {
            List<ComunaModel> output = null;

            if (idProvincia != "000")
            {
                using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
                {
                    output = cx.Query<ComunaModel>("dbo.spComuna_Get_All_Params", new { idProvincia = idProvincia }, commandType: CommandType.StoredProcedure).ToList();
                }
            }
            return output;
        }

        /// <summary>
        /// Metodo de Búsqueda. Select con parametro, trae más de un registro con filtro.
        /// </summary>
        /// <param name="buscaDirecciones"></param>
        /// <returns></returns>
        public List<DireccionModel> GetDirecciones_All_Params(int idDireccion)
        {
            List<DireccionModel> output;

            using (IDbConnection cx = new System.Data.SqlClient.SqlConnection(GlobalConfig.CnnString(db)))
            {
                output = cx.Query<DireccionModel>("dbo.spDireccion_Get_All_Params", new { idDireccion = idDireccion }, commandType: CommandType.StoredProcedure).ToList();
            }

            return output;
        }
        #endregion
    }
}
